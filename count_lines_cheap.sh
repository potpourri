#!/bin/bash

# this is supposed to count lines, cheaply though

export TOTAL=0
for src_file in $(find src > t ; find include >> t ; cat t ; rm t)
  do

  if [[ ! $src_file  =~ "~$" ]] && [[ $(file $src_file) =~ "ASCII C++" ]]
    then
    TEMP=$(wc -l "$src_file" | sed -e "s/\([0-9]*\).*/\1/g")
    export TOTAL=$(bc <<< "$TOTAL + $TEMP")
  fi
done

echo "Total number of lines of code :" $TOTAL
