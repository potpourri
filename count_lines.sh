#!/bin/bash

# this is supposed to count lines

export CC=gcc

if [ ! -e "ncsl" ] 
  then
  $CC ncsl.c -o ncsl
fi

export TOTAL=0
for src_file in $(find src > t ; find include >> t ; cat t ; rm t)
  do

  if [[ ! $src_file  =~ "~$" ]] && [[ $(file $src_file) =~ "ASCII C++" ]]
    then
    export TOTAL=$(bc <<< "$TOTAL + $(./ncsl -t -b $src_file)")
  fi
done

echo "Total number of lines of code :" $TOTAL
