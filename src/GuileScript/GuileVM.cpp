// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The Guile VM wrapper

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <queue>

#include <ctime>

#include "GuileVM.h"

using namespace fragrant;

GuileVM::GuileVM()
{
  count++;
  idx = count;
  pointers[idx] = this;
}

GuileVM::~GuileVM()
{
  count--;
  pointers.erase(pointers.find(idx));
}

bool GuileVM::initVM(std::vector<fragrant::Variant*> params)
{
  running = true;
  lock = false;

  std::vector<fragrant::Variant*> paramsd;

  this_ptr = new fragrant::Variant();
  *this_ptr = this;
  paramsd.push_back(this_ptr);

  guile_thread = fragrant::Thread::makeThread(&GuileVM::run_thread, paramsd);
}

void GuileVM::closeVM()
{
  running = false;
  delete guile_thread;
  delete this_ptr;
}

void GuileVM::execString(std::string data)
{
  SVM_Command command;
  command.type = SCT_DATA;
  command.name = data;

  while (lock) 1;
  lock = true;
  vm_commands_queue.push(command);
  lock = false;

  return;
}

void GuileVM::execFile(std::string filename)
{
  SVM_Command command;
  command.type = SCT_FILE;
  command.name = filename;

  while (lock) 1;
  lock = true;
  vm_commands_queue.push(command);
  lock = false;

  return;
}

void GuileVM::wrapFunction(std::string name, func thefunc)
{
  SVM_Command command;
  command.type = SCT_REGISTER_FUNCTION;
  command.name = name;
  command.function = thefunc;

  while (lock) 1;
  lock = true;
  vm_commands_queue.push(command);
  lock = false;

  return;
}

void GuileVM::wrapClass(ScriptedClass* dclass)
{
  SVM_Command command;
  command.type = SCT_REGISTER_CLASS_FUNC;
  command.dclass = dclass;

  while (lock) 1;
  lock = true;
  vm_commands_queue.push(command);
  lock = false;

  return;
}

int GuileVM::count = 0;
std::map<int, GuileVM*> GuileVM::pointers;

std::string GuileVM::makeWrapper(std::string name, int idx)
{
  std::string results;

  std::stringstream stream; // gwar, conversion
  std::string e;
  stream << idx;
  stream >> e;

  int cur;
  for (cur = 0; cur < (sizeof(GUILEVM_CODE)/sizeof(std::string)); cur++)
    {
      if (GUILEVM_CODE[cur] == FUNC)
        results += name;
      if (GUILEVM_CODE[cur] == NUM)
        results += e;

      if (GUILEVM_CODE[cur] != FUNC && GUILEVM_CODE[cur] != NUM)
        results += GUILEVM_CODE[cur];
    }

  return results;
}

void* GuileVM::run_guile(void* data)
{
  SCM callback_function = scm_c_define_gsubr(CALLBACK.c_str(),
    1, 0, 0, (SCM (*)()) GuileVM::callback);

  timespec sleepd;
  sleepd.tv_sec = 0;
  sleepd.tv_nsec = GUILEVM_CYCLE_LENGTH;

  GuileVM* vm = (GuileVM*)data;

  while (vm->running)
    {
      if (!vm->vm_commands_queue.size())
        nanosleep(&sleepd, 0);

      else
        {
          while (vm->lock) 1;  // yeah, just wait 'til it's free
                               // cheap hack, sue me
          vm->lock = true;
          SVM_Command command = vm->vm_commands_queue.front();
          vm->vm_commands_queue.pop();
          vm->lock = false;
          std::string data = "";
          bool error = false;

          switch (command.type)
            {
              case (SCT_FILE):
                {
                  std::fstream file;
                  file.open(command.name.c_str());

                  error = true;

                  if (!file.is_open())
                    std::cerr << "GuileVM::run_thread(): Error opening file: "
                              << command.name << std::endl;
                  else
                    {
                      std::string temp;
                      while (!file.eof())
                        {
                          getline(file, temp);
                          data += temp + '\n';
                        }

                      error = false;
                    }

                  break;
                }

              case (SCT_DATA):
                {
                  data = command.name;
                  break;
                }

              case (SCT_REGISTER_FUNCTION):
                {
                  data = makeWrapper(command.name, vm->idx);
                  vm->function_pointers[command.name] = command.function;
                  break;
                }

              case (SCT_REGISTER_CLASS_FUNC):
                {
                  scm_t_bits tag = scm_make_smob_type(command.name.c_str(), 
                    sizeof(GuileVMClass));
                  scm_set_smob_free(tag, &free_smob);
                  scm_set_smob_print(tag, &print_smob);

                  vm->smob_types[command.name] = tag;
                  vm->function_pointers[MAKE +
                    command.dclass->getClassName()] = 
                    command.dclass->getConstructor();

                  data = makeWrapper(MAKE + command.dclass->getClassName(),
                    vm->idx);

                  std::vector<std::string> funcs =
                    command.dclass->getClassFunctions();
                  std::vector<std::string>::iterator iter;

                  for (iter = funcs.begin(); iter != funcs.end(); iter++)
                    data += makeWrapper(*iter + "_" + 
                      command.dclass->getClassName(), vm->idx);
                }
            }

          if (!error)
            scm_c_eval_string(data.c_str());
        }
    }

  return 0;
}

void GuileVM::run_thread(std::vector<fragrant::Variant*> params)
{
  scm_with_guile(&GuileVM::run_guile, (void*)params.at(0)->get<GuileVM*>());
  return;
}

std::vector<fragrant::Variant*> GuileVM::makeParamList(SCM root)
{
  std::vector<fragrant::Variant*> results;

  int cur;
  for (cur = 0; cur < scm_to_int(scm_length(root)); cur++)
    {
      SCM cur_param = scm_list_ref(root, scm_from_signed_integer(cur));
      fragrant::Variant* param = new fragrant::Variant();

      if (scm_is_true(scm_boolean_p(cur_param)))
        *param = scm_to_bool(cur_param);

      if (scm_is_true(scm_integer_p(cur_param)))
        *param = scm_to_int(cur_param);

      if (scm_is_true(scm_rational_p(cur_param)) &&
          !scm_is_true(scm_integer_p(cur_param)))
        *param = scm_to_double(cur_param);

      if (scm_is_true(scm_string_p(cur_param)))
        {
          char* cs_param = scm_to_locale_string(cur_param);
          std::string cpps_param = std::string(cs_param);
          free(cs_param);

          *param = cpps_param;
        }

      if (scm_is_true(scm_list_p(cur_param)))
        *param = makeParamList(cur_param);

      results.push_back(param);
    }

  return results;
}

void GuileVM::deleteParamList(std::vector<fragrant::Variant*> param_list)
{
  int cur;
  for (cur = 0; cur < param_list.size(); cur++)
    if (param_list.at(cur)->verifyType<std::vector<fragrant::Variant*> >())
      deleteParamList(param_list.at(cur)
        ->get<std::vector<fragrant::Variant*> >());
    else
      delete param_list.at(cur);
  
  return;
}

void GuileVM::deleteParam(fragrant::Variant* param)
{
  if (param->verifyType<std::vector<fragrant::Variant*> >())
    deleteParamList(param->get<std::vector<fragrant::Variant*> >());
  else
    delete param;
  return;
}

SCM GuileVM::fromParam(fragrant::Variant* param)
{
  SCM results = SCM_UNDEFINED;

  if (param->verifyType<std::string>())
    results = scm_from_locale_string(param->get<std::string>().c_str());

  if (param->verifyType<float>())
    results = scm_from_double(static_cast<double>(param->get<float>()));

  if (param->verifyType<bool>())
    results = scm_from_bool(static_cast<int>(param->get<bool>()));

  if (param->verifyType<int>())
    results = scm_from_int(param->get<int>());

  if (param->verifyType<std::vector<fragrant::Variant*> >())
    {
      std::vector<fragrant::Variant*> variants =
        param->get<std::vector<fragrant::Variant*> >();

      results = scm_list_1(fromParam(variants.at(0)));

      int cur;
      for (cur = 1; cur < variants.size(); cur++)
        results = scm_append(scm_list_2(results,
          scm_list_1(fromParam(variants.at(cur)))));
    }

  return results;
}

size_t GuileVM::free_smob(SCM smob)
{
  GuileVMClass* die_klass = (GuileVMClass*) SCM_SMOB_DATA(smob);
  std::string name = die_klass->wrapped_class->getClassName();

  std::cout << "debug heheheh" << std::endl;

  die_klass->wrapped_class->destroy();
  scm_gc_free(die_klass, sizeof(GuileVMClass), name.c_str());

  return 0;
}

int GuileVM::print_smob(SCM smob, SCM port, scm_print_state* pstate)
{
  GuileVMClass* die_klass = (GuileVMClass*) SCM_SMOB_DATA(smob);

  scm_puts((std::string("#<") +
            std::string(die_klass->wrapped_class->getClassName() +
            std::string(">") )).c_str(), port);

  return 1;
}

GVM_CallbackMode GuileVM::parseCallbackParams(SCM list)
{
  GVM_CallbackMode result = GCM_Error;
  int list_length = scm_to_int(scm_length(list));

  if (list_length > 3 || list_length < 3)
    return result;

  if (!scm_is_true(scm_string_p(scm_list_ref(list, 
       scm_from_signed_integer(0)))))
    return result;

  if (!scm_is_true(scm_integer_p(scm_list_ref(list, 
       scm_from_signed_integer(1)))))
    return GCM_Error;

  if (!scm_is_true(scm_list_p(scm_list_ref(list, 
       scm_from_signed_integer(2)))))
    return GCM_Error;

  SCM param_list = scm_list_ref(scm_list_ref(list, 
    scm_from_signed_integer(2)), scm_from_signed_integer(0));
  int param_length = scm_to_int(scm_length(param_list));

  std::map<std::string, scm_t_bits>::iterator iter;

  if (param_length)
    for (iter = smob_types.begin(); iter != smob_types.end(); iter++)
      if (SCM_SMOB_PREDICATE(iter->second,
          scm_list_ref(param_list, scm_from_signed_integer(0))))
        return GCM_Class;

  return GCM_Function;
}

SCM GuileVM::callback(SCM a)
{
  if (!scm_is_true(scm_list_p(a)))
    {
      std::cerr << "GuileVM::callback(): Wrong params" << std::endl;
      return SCM_UNDEFINED;
    }

  SCM gname = scm_list_ref(a, scm_from_signed_integer(0));
  char* cname = scm_to_locale_string(gname);
  std::string funcname = std::string(cname);
  free(cname);

  SCM gref = scm_list_ref(a, scm_from_signed_integer(1));
  int cref = scm_to_int(gref);
  GuileVM* gvm_ref = GuileVM::pointers[cref];

  GVM_CallbackMode mode = gvm_ref->parseCallbackParams(a);

  if (mode == GCM_Error)
    {
      std::cerr << "GuileVM::callback(): Wrong params" << std::endl;
      return SCM_UNDEFINED;
    }

  fragrant::Variant* result;

  SCM params = scm_list_ref(scm_list_ref(a,
    scm_from_signed_integer(2)), scm_from_signed_integer(0));

  if (mode == GCM_Function)
    {
      std::vector<fragrant::Variant*> param_list = makeParamList(params);

      if (gvm_ref->function_pointers.find(funcname) ==
          gvm_ref->function_pointers.end())
        {
          std::cerr << "GuileVM::callback(): No such function: "
                    << funcname << std::endl;
          deleteParamList(param_list);
          return SCM_UNDEFINED;
        }

      func the_func = gvm_ref->function_pointers[funcname];

      result = the_func(param_list);
      deleteParamList(param_list);
    }

  if (mode == GCM_Class)
    {
      GuileVMClass* class_ptr = (GuileVMClass*)SCM_SMOB_DATA(
        scm_list_ref(params, scm_from_signed_integer(0)));

      std::string real_funcname = funcname.substr(0,
        funcname.size() - (class_ptr->wrapped_class->
        getClassName().size() + 1));

      std::vector<fragrant::Variant*> param_list =
        makeParamList(scm_cdr(params));

      result = class_ptr->wrapped_class->
        callFunction(real_funcname, param_list, gvm_ref);
    }

  else 

  1; // heh, for some reason it doesn't compile without this
     // an excellent reason preprocessor macros blow
  SCM return_scm;

  if (result->verifyType<ScriptedClass*>())
    {
      ScriptedClass* returned_class = result->get<ScriptedClass*>();
      std::string name = returned_class->getClassName();
      GuileVMClass* gvm_class = (GuileVMClass*)
        scm_gc_malloc(sizeof(GuileVMClass), name.c_str());
      gvm_class->wrapped_class = returned_class;

      SCM d;

      SCM_NEWSMOB(return_scm, gvm_ref->smob_types[name], gvm_class);
    }

  else
    return_scm = fromParam(result);

  deleteParam(result);

  return return_scm;
}

// fin
