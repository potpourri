// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The Guile wrapper header

#ifndef __GUILEVM_H
#define __GUILEVM_H

#include <map>
#include <string>
#include <queue>

#include <libguile.h>

#include "ScriptVM.h"
#include "Thread.h"

namespace fragrant
{
  enum SVM_Command_Type
    {
      SCT_FILE,
      SCT_DATA,
      SCT_REGISTER_FUNCTION,
      SCT_REGISTER_CLASS_FUNC
    };

  struct SVM_Command
    {
      SVM_Command_Type type;
      std::string name;
      ScriptedClass* dclass; // applicable for the SCT_REGISTER_CLASS_FUNC
      func function; // applicable for the SCT_REGISTER_FUNCTION enum value
    };

  const long int GUILEVM_CYCLE_LENGTH = 50;

  const std::string MAKE = "make_";
  const std::string CALLBACK = "callback";
  const std::string FUNC = "FUNC";
  const std::string NUM = "NUM";
  const std::string GUILEVM_CODE[] =
    {
      "(define ", FUNC, " (lambda args (", CALLBACK, " (list \"", FUNC, 
      "\" ", NUM, " (list args)))  ))"
    };

  enum GVM_CallbackMode
    {
      GCM_Function,
      GCM_Class,
      GCM_Error
    };

  struct GuileVMClass
    {
      ScriptedClass* wrapped_class;
      bool ours;
    };

  class GuileVM : public ScriptVM
    {
      public:
        GuileVM();
        ~GuileVM();

        bool initVM(std::vector<fragrant::Variant*> params);
        void closeVM();

        void execString(std::string data);
        void execFile(std::string filename);

        void wrapFunction(std::string name, func thefunc);
        void wrapClass(ScriptedClass* dclass);

      private:
        fragrant::Thread* guile_thread;
        fragrant::Variant* this_ptr;
        bool running;
        bool lock;
        std::queue<SVM_Command> vm_commands_queue;

        int idx;
        static int count;
        static std::map<int, GuileVM*> pointers;

        std::map<std::string, func> function_pointers;
        std::map<std::string, scm_t_bits> smob_types;

        static std::string makeWrapper(std::string name, int idx);

        static void* run_guile(void* data);
        static void run_thread(std::vector<fragrant::Variant*> params);

        // c++->guile and guile->c++ conversion stuff

        static std::vector<fragrant::Variant*> makeParamList(SCM root);
        static void deleteParamList(std::vector<fragrant::Variant*> param_list);

        static void deleteParam(fragrant::Variant* param);
        static SCM fromParam(fragrant::Variant* param);

        // stuff

        static size_t free_smob(SCM smob);
        static int print_smob(SCM smob, SCM port, scm_print_state* pstate);

        GVM_CallbackMode parseCallbackParams(SCM list);

        // the guile to c++ callback

        static SCM callback(SCM a);
  };

}

#endif
