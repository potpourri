// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The plugin stuff

#include <string>
#include <iostream>

#include "Plugin.h"
#include "GuileVM.h"

const std::string PLUGIN_NAME = "GuileVM Plugin";
const std::string PLUGIN_AUTHOR = "Brian Caine";
const std::string PLUGIN_VERSION = "1.0";

using namespace fragrant;

PluginInfo queryPlugin()
{
  PluginInfo info;
  info.name = PLUGIN_NAME;
  info.authors.push_back(PLUGIN_AUTHOR);
  info.version = PLUGIN_VERSION;

  return info;
}

PluginData loadPlugin()
{
  PluginData results;
  results.name = PLUGIN_NAME;
  results.type = PT_Script;
  results.payload = new fragrant::Variant();

  ScriptVM* guile_vm = dynamic_cast<ScriptVM*>(new GuileVM());
  *results.payload = guile_vm;

  return results;
}
