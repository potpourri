// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This is my oop wrapper around libzip
// this is just for reading, just so you don't get the wrong idea

#ifndef __ZIP_H
#define __ZIP_H

#include <zip.h>

#include <vector>
#include <string>

#include <cstdlib>
#include <time.h>

#include "../../include/plugins/media/Archive.h"
#include "../../include/core/common.h"

namespace fragrant
{
  const std::string temp = "temp";

  const int wrap = 40;
  const int seed = time(0);

  struct ZipWrap
    {
      zip* zip_data;
      int refcount;
    };

  class Zip : public Archive
    {
      public:
        Zip(std::string data);
        Zip(ZipWrap* zip_datad);
        ~Zip();

        void setArchiveFuncs(std::map<std::string, archive_function> funcs);

        std::string getFile(std::string source);
        std::vector<std::string> getListing(std::string folder);
        Directory* getDirectory(std::vector<std::string> path);

      private:
        std::string fetchData(std::string filename);

        std::string temp_filename;
        ZipWrap* zip_data;

        std::string prefix;

        std::map<std::string, archive_function> functions;
    };
}

#endif
