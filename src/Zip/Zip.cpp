// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The sauce to my zip class

#include "Zip.h"

#include <iostream>
#include <fstream>
#include <stdexcept>

#include <cstdlib>

#include "../../include/core/common.h"
#include "../../include/core/Magic.h"

using namespace fragrant;

Zip::Zip(std::string data)
{
  srand(seed);
  temp_filename = temp + toString(rand() % 40);

  std::fstream temp_crap(temp_filename.c_str(),
    std::ios_base::out|std::ofstream::binary); 
  temp_crap.write(data.c_str(), data.size());
  temp_crap.close();

  zip_data = new ZipWrap();

  zip_data->zip_data = zip_open(temp_filename.c_str(), 0, 0);
  zip_data->refcount = 1;

  prefix = "";

  if (!zip_data->zip_data)
    throw std::runtime_error("Zip::Zip(): Error opening zipfile");
}

Zip::Zip(ZipWrap* zip_datad) : zip_data(zip_datad)
{
  zip_data->refcount++;
}

Zip::~Zip()
{
  zip_data->refcount--;

  if (zip_data->zip_data && !zip_data->refcount)
    {
      zip_close(zip_data->zip_data);
      delete zip_data;

      if (unlink(temp_filename.c_str()) == -1)
        std::cerr << "Zip::~Zip(): Error removing temporary zipfile, \""
                  << temp_filename << "\"" << std::endl;
    }
}

void Zip::setArchiveFuncs(std::map<std::string, archive_function> funcs)
{
  functions = funcs;
}

std::string Zip::getFile(std::string source)
{
  std::vector<std::string> portions = splitPath(simplifyPath(source));

  if (portions.size() == 1)
    return fetchData(source);

  Directory* last_directory = getDirectory(
    std::vector<std::string>(portions.begin(), portions.end() - 1));

  std::string result =
    last_directory->getFile(portions.at(portions.size() - 1));

  delete last_directory;

  return result;
}

std::vector<std::string> Zip::getListing(std::string folder)
{
  throw std::runtime_error("Zip::getListing(): Not implemented yet");
}

Directory* Zip::getDirectory(std::vector<std::string> path)
{
  if (!path.size())
    {
      Zip* nzip = new Zip(zip_data);
      nzip->temp_filename = temp_filename;
      nzip->prefix = prefix;
      nzip->functions = functions;

      return nzip;
    }

  std::string data;

  try
    {
      data = fetchData(path.at(0));
    }
  catch (std::runtime_error& d){}

  Magic mime_magic = Magic(true);
  std::string mime_type = mime_magic.data(data);

  Directory* result = 0;

  std::vector<std::string> rest =
    std::vector<std::string>(path.begin() + 1, path.end());

  if (functions.find(mime_type) != functions.end())
    {
      archive_function function = functions[mime_type];
      Archive* arc = function(data);
      arc->setArchiveFuncs(functions);
      result = arc->getDirectory(rest);
      delete arc;
    }
  else
    {
      Zip* ndir = new Zip(zip_data);
      ndir->setArchiveFuncs(functions); // ndir->setArrchiveFuncs(functions);
      ndir->temp_filename = temp_filename;
      ndir->prefix = prefix + path_separator + path.at(0);
      result = ndir->getDirectory(rest);
      delete ndir;
    }

  return result;
}

std::string Zip::fetchData(std::string filename)
{
  if (zip_name_locate(zip_data->zip_data, filename.c_str(), 0) == -1)
    throw std::runtime_error("Zip::fetchData(): Error locating file, \""
      + filename + "\"");

  struct zip_stat info;
  zip_stat(zip_data->zip_data, filename.c_str(), 0, &info);

  char data[info.size];

  zip_file* file = zip_fopen(zip_data->zip_data, filename.c_str(), 0);
  if (!zip_fread(file, data, info.size))
    {
      zip_fclose(file);
      throw std::runtime_error("Zip::fetchData(): error reading file, \""
        + filename + "\"");
    }
  zip_fclose(file);

  return std::string(data, info.size);
}
