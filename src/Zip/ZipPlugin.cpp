// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// again, machinery

#include "../../include/core/Plugin.h"
#include "../../include/plugins/media/Media.h"
#include "ZipPlugin.h"

using namespace fragrant;

Archive* makeZip(std::string data)
{
  return new Zip(data);
}

PluginData queryPlugin()
{
  PluginData data;

  data.name = "Zip Plugin";
  data.type = PT_Media;
  data.version = "0.1";
  data.authors.push_back("Brian Caine");

  return data;
}

fragrant::Variant* loadPluginPayload()
{
  fragrant::Variant* payload = new fragrant::Variant();

  Media media_details;
//  media_details.mime_type = "application/x-zip";
  media_details.mime_type = "application/zip";
  media_details.func = makeZip;

  *payload = media_details;

  return payload;
}
