// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// moar sauce

#include "SDLSurface.h"
#include "../../include/core/common.h"
#include "../../include/core/Actor.h"

#include <SDL/SDL_rotozoom.h>
#include <stdexcept>
#include <iostream>

using namespace fragrant;

SDLSurface::SDLSurface(SDL_Surface* nsurf) : surf(nsurf), mine(true) {}

SDLSurface::~SDLSurface()
{
  if (mine)
    SDL_FreeSurface(surf);
}

void SDLSurface::draw(Target* target, GraphicsRect* destrect,
  GraphicsPair* srcrect, float angle)
{
  SDLSurface* target_surf = dynamic_cast<SDLSurface*>(target);

  if (!target_surf)
    throw std::runtime_error("SDLSurface::draw(): target is not "
      "a SDLSurface");

  SDL_Surface* dest_surf = target_surf->surf;
  SDL_Surface* final_surf = (angle == 0.0) ? surf :
    rotozoomSurface(surf, angle, 1, SMOOTH);

  bool free_me = (final_surf != surf);

  SDL_Rect destrect_sdl = {0, 0, 0, 0};
  SDL_Rect srcrect_sdl = {0, 0, final_surf->w, final_surf->h};

  Pair<float> offset = getOffset(makePair(surf->w, surf->h), angle);

  if (destrect)
    {
      destrect_sdl.x = destrect->xy.x + offset.x;
      destrect_sdl.y = destrect->xy.y + offset.y;
    }

  SDL_BlitSurface(final_surf, &srcrect_sdl, dest_surf, &destrect_sdl);

  if (srcrect)
    {
      srcrect->x = srcrect_sdl.x;
      srcrect->y = srcrect_sdl.y;
    }

  if (free_me)
    SDL_FreeSurface(final_surf);

  return;
}

ScriptedClass* SDLSurface::getScriptObject()
{
  return 0;
}

void SDLSurface::destroy()
{
  delete this;
}
