// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A drawable thing

#ifndef __SDLSURFACE_H
#define __SDLSURFACE_H

#include <SDL/SDL.h>

#include "../../include/plugins/graphics/Graphics.h"

namespace fragrant
{
  const int SMOOTH = 0;

  class SDLSurface : public Target, public Drawable
    {
      public:
        SDLSurface(SDL_Surface* nsurf = 0);
        virtual ~SDLSurface();

        void draw(Target* target, GraphicsRect* destrect,
          GraphicsPair* srcrect, float angle);
        ScriptedClass* getScriptObject();
        void destroy();

        SDL_Surface* surf;

      protected:

        bool mine;
    };
}

#endif
