// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A Display implementation

#ifndef __SDLDISPLAY_H
#define __SDLDISPLAY_H

#include "SDLSurface.h"

namespace fragrant
{
  class SDLDisplay : public SDLSurface, public Display
    {
      public:
        SDLDisplay();
        ~SDLDisplay();

        bool init(GraphicsPair size, int bpp);
        bool resize(GraphicsPair size, int bpp);
        void close();

        void flip();

      private:
    };
}

#endif
