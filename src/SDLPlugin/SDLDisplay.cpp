// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The sauce

#include "SDLDisplay.h"
#include "../SDL/SDLManager.h"

#include <stdexcept>

using namespace fragrant;

SDLDisplay::SDLDisplay()
{
  this->surf = 0;
  this->mine = false;
}

SDLDisplay::~SDLDisplay() {}

bool SDLDisplay::init(GraphicsPair size, int bpp)
{
  surf = SDL_SetVideoMode(size.x, size.y, bpp, 0);
  SDL_EnableKeyRepeat(
    SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL);

  return static_cast<bool>(surf);
}

bool SDLDisplay::resize(GraphicsPair size, int bpp)
{
  throw std::runtime_error("SDLDisplay::resize(): I just don't feel"
    " like implementing this");
}

void SDLDisplay::close()
{
  SDLManager manager;
  manager.decrement(fragrant::SS_Video);

  delete this;

  return;
}

void SDLDisplay::flip()
{
  SDL_Flip(surf);

  return;
}
