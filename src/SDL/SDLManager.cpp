// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The sauce

#include "SDLManager.h"

using namespace fragrant;

#include <SDL/SDL.h>

SDLManager::SDLManager()
{}

SDLManager::~SDLManager()
{}

void SDLManager::increment(SDLSubsystem system)
{
  int flag = 0;

  if (tally.timer || tally.audio || tally.video)
    tally.initted = true; // debug : yeah, this shoudn't be necessary

  switch (system)
    {
      case (SS_Timer):
        {
          tally.timer++;
          if (tally.timer == 1)
            flag = SDL_INIT_TIMER;

          break;
        }

      case (SS_Audio):
        {
          tally.audio++;
          if (tally.audio == 1)
            flag = SDL_INIT_AUDIO;

          break;
        }

      case (SS_Video):
        {
          tally.video++;
          if (tally.timer == 1)
            flag = SDL_INIT_VIDEO;

          break;
        }
    }

  if (!flag)
    return;

  if (tally.initted)
    {
      SDL_InitSubSystem(flag);
    }

  else
    {
      // debug

      SDL_Init(flag);

      // debug

      tally.initted = true;

      // debug
    }

  return;
}

void SDLManager::decrement(SDLSubsystem system)
{
  int flag = 0;

  switch (system)
    {
      case (SS_Timer):
        {
          tally.timer--;
          if (tally.timer == 0)
            flag = SDL_INIT_TIMER;

          break;
        }

      case (SS_Audio):
        {
          tally.audio--;
          if (tally.audio == 0)
            flag = SDL_INIT_AUDIO;

          break;
        }

      case (SS_Video):
        {
          tally.video--;
          if (tally.timer == 0)
            flag = SDL_INIT_VIDEO;

          break;
        }
    }

  if (!flag)
    return;

  if (tally.timer || tally.audio || tally.video)
    SDL_QuitSubSystem(flag);

  else
    SDL_Quit();

  return;
}

_SDLTally SDLManager::tally = {0, 0, 0, 0};
