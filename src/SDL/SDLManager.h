// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The SDLManager class handles the subsystems of SDL

#ifndef __SDLMANAGER_H
#define __SDLMANAGER_H

namespace fragrant
{
  struct _SDLTally
    {
      int timer;
      int audio;
      int video;

      bool initted;
    };

  enum SDLSubsystem
    {
      SS_Timer,
      SS_Audio,
      SS_Video
    };

  class SDLManager
    {
      public:
        SDLManager();
        ~SDLManager();

        void increment(SDLSubsystem system);
        void decrement(SDLSubsystem system);

      private:
        static _SDLTally tally;
    };
}

#endif
