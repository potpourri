// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A fragrant::InputManager implemented with sdl

#ifndef __SDLINPUT_H
#define __SDLINPUT_H

#include <SDL/SDL.h>

#include "../../include/plugins/input/InputManager.h"

namespace fragrant
{
  class SDLInput : public InputManager
    {
      public:
        SDLInput();
        ~SDLInput();

        void init(EventPipeline* pipeline);
        void close();

        void appendEvent(Event nevent);
        void pump();

      private:
        static Key getKey(SDLKey key);

        EventPipeline* pipeline;
        std::map<int, std::string> event_pairs;
        std::vector<Event> events;
    };
}

#endif
