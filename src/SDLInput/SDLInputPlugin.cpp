// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// This is the plugin source for the SDLInput plugin

#include "SDLInput.h"
#include "../../include/core/Plugin.h"

fragrant::Variant* loadPluginPayload()
{
  fragrant::SDLInput* input = new fragrant::SDLInput;
  fragrant::Variant* variant_result = new fragrant::Variant();

  *variant_result = dynamic_cast<fragrant::InputManager*>(input);

  return variant_result;
}

fragrant::PluginData queryPlugin()
{
  fragrant::PluginData results;

  results.name = "SDLInput";
  results.type = fragrant::PT_Input;
  results.version = "1.0";
  results.authors.push_back(std::string("Brian Caine"));

  return results;
}
