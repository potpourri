// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The sauce to the afforementioned SDLInput class

#include "SDLInput.h"
#include "../SDL/SDLManager.h"

#include <stdexcept>
#include <iostream>

#include <SDL/SDL.h>

using namespace fragrant;

SDLInput::SDLInput()
{
  pipeline = 0;

  SDLManager manager;
  manager.increment(SS_Video);
}

SDLInput::~SDLInput()
{
  SDLManager manager;
  manager.decrement(SS_Video);
}

void SDLInput::init(EventPipeline* pipeline)
{
  SDL_EnableKeyRepeat(10, 30);
  this->pipeline = pipeline;
}

void SDLInput::close()
{
  delete this;
}

void SDLInput::appendEvent(Event nevent)
{
  events.push_back(nevent);
  return;
}

void SDLInput::pump()
{
  SDL_Event event;
  Event result;

  while (SDL_PollEvent(&event))
    {
      switch (event.type)
        {
          case (SDL_ACTIVEEVENT):
            {
              result.name = EventStringArray[ET_WMEvent];

              Variant* gain = new Variant();
              *gain = static_cast<int>(event.active.gain);

              int nstate;
              int oldstate = event.active.state;

              nstate = (oldstate & SDL_APPMOUSEFOCUS) |
                       (oldstate & SDL_APPINPUTFOCUS) |
                       (oldstate & SDL_APPACTIVE);

              Variant* state = new Variant();
              *state = static_cast<int>(nstate);

              result.data["gain"] = gain;
              result.data["state"] = state;

              break;
            }

          case (SDL_KEYDOWN):
          case (SDL_KEYUP):
            {
              result.name = EventStringArray[ET_Key];

              Variant* pressed = new Variant();
              *pressed = (event.key.state == SDL_PRESSED);

              Variant* dchar = new Variant();
              *dchar = static_cast<char>(event.key.keysym.unicode);

              Variant* key = new Variant();
              *key = getKey(event.key.keysym.sym);

              result.data["pressed"] = pressed;
              result.data["char"] = dchar;
              result.data["key"] = key;

              break;
            }

          case (SDL_MOUSEMOTION):
            {
              result.name = EventStringArray[ET_MouseMotion];

              int ostate = event.motion.state;

              Variant* state = new Variant();
              *state = static_cast<int>(
                ((SDL_BUTTON(1) & ostate) ? MB_LEFT : 0) |
                ((SDL_BUTTON(2) & ostate) ? MB_MIDDLE : 0) | 
                ((SDL_BUTTON(3) & ostate) ? MB_RIGHT : 0));

              Variant* x = new Variant();
              *x = static_cast<int>(event.motion.x);

              Variant* y = new Variant();
              *y = static_cast<int>(event.motion.y);

              Variant* xrel = new Variant();
              *xrel = static_cast<int>(event.motion.xrel);

              Variant* yrel = new Variant();
              *yrel = static_cast<int>(event.motion.yrel);

              result.data["state"] = state;

              result.data["x"] = x;
              result.data["y"] = y;

              result.data["xrel"] = xrel;
              result.data["yrel"] = yrel;

              break;
            }

          case (SDL_MOUSEBUTTONDOWN):
          case (SDL_MOUSEBUTTONUP):
            {
              result.name = EventStringArray[ET_MouseButton];

              Variant* button = new Variant();
              switch (event.button.button)
                {
                  case(SDL_BUTTON_LEFT):
                    {
                      *button = MB_LEFT;
                      break;
                    }

                  case(SDL_BUTTON_MIDDLE):
                    {
                      *button = MB_MIDDLE;
                      break;
                    }
                  case(SDL_BUTTON_RIGHT):
                    {
                      *button = MB_RIGHT;
                      break;
                    }
                  case(SDL_BUTTON_WHEELUP):
                    {
                      *button = MB_WHEEL_UP;
                      break;
                    }
                  case(SDL_BUTTON_WHEELDOWN):
                    {
                      *button = MB_WHEEL_DOWN;
                      break;
                    }
                }

              Variant* pressed = new Variant();
              *pressed = event.button.state == SDL_PRESSED;

              Variant* x = new Variant();
              *x = static_cast<int>(event.button.x);

              Variant* y = new Variant();
              *y = static_cast<int>(event.button.y);

              result.data["button"] = button;
              result.data["pressed"] = pressed;

              result.data["x"] = x;
              result.data["y"] = y;

              break;
            }

          case (SDL_JOYAXISMOTION):
          case (SDL_JOYBALLMOTION):
          case (SDL_JOYHATMOTION):
          case (SDL_JOYBUTTONDOWN):
          case (SDL_JOYBUTTONUP):
            {
              std::cout << "SDLInput::pump(): Joystick events not supported"
                        << std::endl;
              break;
            }

          case (SDL_QUIT):
            {
              result.name = EventStringArray[ET_Quit];
              break;
            }
        }

      pipeline->raiseEvent(result);
      result = Event();
    }

  int cur;
  for (cur = 0; cur < events.size(); cur++)
    pipeline->raiseEvent(events.at(cur));

  events.clear();

  return;
}

Key SDLInput::getKey(SDLKey key)
{
  switch (key)
    {
      case(SDLK_FIRST): return K_FIRST;
      case(SDLK_BACKSPACE): return K_BACKSPACE;
      case(SDLK_TAB): return K_TAB;
      case(SDLK_CLEAR): return K_CLEAR;
      case(SDLK_RETURN): return K_RETURN;
      case(SDLK_PAUSE): return K_PAUSE;
      case(SDLK_ESCAPE): return K_ESCAPE;
      case(SDLK_SPACE): return K_SPACE;
      case(SDLK_EXCLAIM): return K_EXCLAIM;
      case(SDLK_QUOTEDBL): return K_QUOTEDBL;
      case(SDLK_HASH): return K_HASH;
      case(SDLK_DOLLAR): return K_DOLLAR;
      case(SDLK_AMPERSAND): return K_AMPERSAND;
      case(SDLK_QUOTE): return K_QUOTE;
      case(SDLK_LEFTPAREN): return K_LEFTPAREN;
      case(SDLK_RIGHTPAREN): return K_RIGHTPAREN;
      case(SDLK_ASTERISK): return K_ASTERISK;
      case(SDLK_PLUS): return K_PLUS;
      case(SDLK_COMMA): return K_COMMA;
      case(SDLK_MINUS): return K_MINUS;
      case(SDLK_PERIOD): return K_PERIOD;
      case(SDLK_SLASH): return K_SLASH;
      case(SDLK_0): return K_0;
      case(SDLK_1): return K_1;
      case(SDLK_2): return K_2;
      case(SDLK_3): return K_3;
      case(SDLK_4): return K_4;
      case(SDLK_5): return K_5;
      case(SDLK_6): return K_6;
      case(SDLK_7): return K_7;
      case(SDLK_8): return K_8;
      case(SDLK_9): return K_9;
      case(SDLK_COLON): return K_COLON;
      case(SDLK_SEMICOLON): return K_SEMICOLON;
      case(SDLK_LESS): return K_LESS;
      case(SDLK_EQUALS): return K_EQUALS;
      case(SDLK_GREATER): return K_GREATER;
      case(SDLK_QUESTION): return K_QUESTION;
      case(SDLK_AT): return K_AT;
      case(SDLK_LEFTBRACKET): return K_LEFTBRACKET;
      case(SDLK_BACKSLASH): return K_BACKSLASH;
      case(SDLK_RIGHTBRACKET): return K_RIGHTBRACKET;
      case(SDLK_CARET): return K_CARET;
      case(SDLK_UNDERSCORE): return K_UNDERSCORE;
      case(SDLK_BACKQUOTE): return K_BACKQUOTE;
      case(SDLK_a): return K_a;
      case(SDLK_b): return K_b;
      case(SDLK_c): return K_c;
      case(SDLK_d): return K_d;
      case(SDLK_e): return K_e;
      case(SDLK_f): return K_f;
      case(SDLK_g): return K_g;
      case(SDLK_h): return K_h;
      case(SDLK_i): return K_i;
      case(SDLK_j): return K_j;
      case(SDLK_k): return K_k;
      case(SDLK_l): return K_l;
      case(SDLK_m): return K_m;
      case(SDLK_n): return K_n;
      case(SDLK_o): return K_o;
      case(SDLK_p): return K_p;
      case(SDLK_q): return K_q;
      case(SDLK_r): return K_r;
      case(SDLK_s): return K_s;
      case(SDLK_t): return K_t;
      case(SDLK_u): return K_u;
      case(SDLK_v): return K_v;
      case(SDLK_w): return K_w;
      case(SDLK_x): return K_x;
      case(SDLK_y): return K_y;
      case(SDLK_z): return K_z;
      case(SDLK_DELETE): return K_DELETE;
      case(SDLK_WORLD_0): return K_WORLD_0;
      case(SDLK_WORLD_1): return K_WORLD_1;
      case(SDLK_WORLD_2): return K_WORLD_2;
      case(SDLK_WORLD_3): return K_WORLD_3;
      case(SDLK_WORLD_4): return K_WORLD_4;
      case(SDLK_WORLD_5): return K_WORLD_5;
      case(SDLK_WORLD_6): return K_WORLD_6;
      case(SDLK_WORLD_7): return K_WORLD_7;
      case(SDLK_WORLD_8): return K_WORLD_8;
      case(SDLK_WORLD_9): return K_WORLD_9;
      case(SDLK_WORLD_10): return K_WORLD_10;
      case(SDLK_WORLD_11): return K_WORLD_11;
      case(SDLK_WORLD_12): return K_WORLD_12;
      case(SDLK_WORLD_13): return K_WORLD_13;
      case(SDLK_WORLD_14): return K_WORLD_14;
      case(SDLK_WORLD_15): return K_WORLD_15;
      case(SDLK_WORLD_16): return K_WORLD_16;
      case(SDLK_WORLD_17): return K_WORLD_17;
      case(SDLK_WORLD_18): return K_WORLD_18;
      case(SDLK_WORLD_19): return K_WORLD_19;
      case(SDLK_WORLD_20): return K_WORLD_20;
      case(SDLK_WORLD_21): return K_WORLD_21;
      case(SDLK_WORLD_22): return K_WORLD_22;
      case(SDLK_WORLD_23): return K_WORLD_23;
      case(SDLK_WORLD_24): return K_WORLD_24;
      case(SDLK_WORLD_25): return K_WORLD_25;
      case(SDLK_WORLD_26): return K_WORLD_26;
      case(SDLK_WORLD_27): return K_WORLD_27;
      case(SDLK_WORLD_28): return K_WORLD_28;
      case(SDLK_WORLD_29): return K_WORLD_29;
      case(SDLK_WORLD_30): return K_WORLD_30;
      case(SDLK_WORLD_31): return K_WORLD_31;
      case(SDLK_WORLD_32): return K_WORLD_32;
      case(SDLK_WORLD_33): return K_WORLD_33;
      case(SDLK_WORLD_34): return K_WORLD_34;
      case(SDLK_WORLD_35): return K_WORLD_35;
      case(SDLK_WORLD_36): return K_WORLD_36;
      case(SDLK_WORLD_37): return K_WORLD_37;
      case(SDLK_WORLD_38): return K_WORLD_38;
      case(SDLK_WORLD_39): return K_WORLD_39;
      case(SDLK_WORLD_40): return K_WORLD_40;
      case(SDLK_WORLD_41): return K_WORLD_41;
      case(SDLK_WORLD_42): return K_WORLD_42;
      case(SDLK_WORLD_43): return K_WORLD_43;
      case(SDLK_WORLD_44): return K_WORLD_44;
      case(SDLK_WORLD_45): return K_WORLD_45;
      case(SDLK_WORLD_46): return K_WORLD_46;
      case(SDLK_WORLD_47): return K_WORLD_47;
      case(SDLK_WORLD_48): return K_WORLD_48;
      case(SDLK_WORLD_49): return K_WORLD_49;
      case(SDLK_WORLD_50): return K_WORLD_50;
      case(SDLK_WORLD_51): return K_WORLD_51;
      case(SDLK_WORLD_52): return K_WORLD_52;
      case(SDLK_WORLD_53): return K_WORLD_53;
      case(SDLK_WORLD_54): return K_WORLD_54;
      case(SDLK_WORLD_55): return K_WORLD_55;
      case(SDLK_WORLD_56): return K_WORLD_56;
      case(SDLK_WORLD_57): return K_WORLD_57;
      case(SDLK_WORLD_58): return K_WORLD_58;
      case(SDLK_WORLD_59): return K_WORLD_59;
      case(SDLK_WORLD_60): return K_WORLD_60;
      case(SDLK_WORLD_61): return K_WORLD_61;
      case(SDLK_WORLD_62): return K_WORLD_62;
      case(SDLK_WORLD_63): return K_WORLD_63;
      case(SDLK_WORLD_64): return K_WORLD_64;
      case(SDLK_WORLD_65): return K_WORLD_65;
      case(SDLK_WORLD_66): return K_WORLD_66;
      case(SDLK_WORLD_67): return K_WORLD_67;
      case(SDLK_WORLD_68): return K_WORLD_68;
      case(SDLK_WORLD_69): return K_WORLD_69;
      case(SDLK_WORLD_70): return K_WORLD_70;
      case(SDLK_WORLD_71): return K_WORLD_71;
      case(SDLK_WORLD_72): return K_WORLD_72;
      case(SDLK_WORLD_73): return K_WORLD_73;
      case(SDLK_WORLD_74): return K_WORLD_74;
      case(SDLK_WORLD_75): return K_WORLD_75;
      case(SDLK_WORLD_76): return K_WORLD_76;
      case(SDLK_WORLD_77): return K_WORLD_77;
      case(SDLK_WORLD_78): return K_WORLD_78;
      case(SDLK_WORLD_79): return K_WORLD_79;
      case(SDLK_WORLD_80): return K_WORLD_80;
      case(SDLK_WORLD_81): return K_WORLD_81;
      case(SDLK_WORLD_82): return K_WORLD_82;
      case(SDLK_WORLD_83): return K_WORLD_83;
      case(SDLK_WORLD_84): return K_WORLD_84;
      case(SDLK_WORLD_85): return K_WORLD_85;
      case(SDLK_WORLD_86): return K_WORLD_86;
      case(SDLK_WORLD_87): return K_WORLD_87;
      case(SDLK_WORLD_88): return K_WORLD_88;
      case(SDLK_WORLD_89): return K_WORLD_89;
      case(SDLK_WORLD_90): return K_WORLD_90;
      case(SDLK_WORLD_91): return K_WORLD_91;
      case(SDLK_WORLD_92): return K_WORLD_92;
      case(SDLK_WORLD_93): return K_WORLD_93;
      case(SDLK_WORLD_94): return K_WORLD_94;
      case(SDLK_WORLD_95): return K_WORLD_95;
      case(SDLK_KP0): return K_KP0;
      case(SDLK_KP1): return K_KP1;
      case(SDLK_KP2): return K_KP2;
      case(SDLK_KP3): return K_KP3;
      case(SDLK_KP4): return K_KP4;
      case(SDLK_KP5): return K_KP5;
      case(SDLK_KP6): return K_KP6;
      case(SDLK_KP7): return K_KP7;
      case(SDLK_KP8): return K_KP8;
      case(SDLK_KP9): return K_KP9;
      case(SDLK_KP_PERIOD): return K_KP_PERIOD;
      case(SDLK_KP_DIVIDE): return K_KP_DIVIDE;
      case(SDLK_KP_MULTIPLY): return K_KP_MULTIPLY;
      case(SDLK_KP_MINUS): return K_KP_MINUS;
      case(SDLK_KP_PLUS): return K_KP_PLUS;
      case(SDLK_KP_ENTER): return K_KP_ENTER;
      case(SDLK_KP_EQUALS): return K_KP_EQUALS;
      case(SDLK_UP): return K_UP;
      case(SDLK_DOWN): return K_DOWN;
      case(SDLK_RIGHT): return K_RIGHT;
      case(SDLK_LEFT): return K_LEFT;
      case(SDLK_INSERT): return K_INSERT;
      case(SDLK_HOME): return K_HOME;
      case(SDLK_END): return K_END;
      case(SDLK_PAGEUP): return K_PAGEUP;
      case(SDLK_PAGEDOWN): return K_PAGEDOWN;
      case(SDLK_F1): return K_F1;
      case(SDLK_F2): return K_F2;
      case(SDLK_F3): return K_F3;
      case(SDLK_F4): return K_F4;
      case(SDLK_F5): return K_F5;
      case(SDLK_F6): return K_F6;
      case(SDLK_F7): return K_F7;
      case(SDLK_F8): return K_F8;
      case(SDLK_F9): return K_F9;
      case(SDLK_F10): return K_F10;
      case(SDLK_F11): return K_F11;
      case(SDLK_F12): return K_F12;
      case(SDLK_F13): return K_F13;
      case(SDLK_F14): return K_F14;
      case(SDLK_F15): return K_F15;
      case(SDLK_NUMLOCK): return K_NUMLOCK;
      case(SDLK_CAPSLOCK): return K_CAPSLOCK;
      case(SDLK_SCROLLOCK): return K_SCROLLOCK;
      case(SDLK_RSHIFT): return K_RSHIFT;
      case(SDLK_LSHIFT): return K_LSHIFT;
      case(SDLK_RCTRL): return K_RCTRL;
      case(SDLK_LCTRL): return K_LCTRL;
      case(SDLK_RALT): return K_RALT;
      case(SDLK_LALT): return K_LALT;
      case(SDLK_RMETA): return K_RMETA;
      case(SDLK_LMETA): return K_LMETA;
      case(SDLK_LSUPER): return K_LSUPER;
      case(SDLK_RSUPER): return K_RSUPER;
      case(SDLK_MODE): return K_MODE;
      case(SDLK_COMPOSE): return K_COMPOSE;
      case(SDLK_HELP): return K_HELP;
      case(SDLK_PRINT): return K_PRINT;
      case(SDLK_SYSREQ): return K_SYSREQ;
      case(SDLK_BREAK): return K_BREAK;
      case(SDLK_MENU): return K_MENU;
      case(SDLK_POWER): return K_POWER;
      case(SDLK_EURO): return K_EURO;
      case(SDLK_UNDO): return K_UNDO;
    }
}
