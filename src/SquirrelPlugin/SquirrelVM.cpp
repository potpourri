// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// sauce

#include "SquirrelVM.h"
#include <pcrecpp.h>
#include <stdexcept>

using namespace fragrant;

Variant* fragrant::getVariantFromSquirrel(HSQUIRRELVM svm, int pos)
{
  Variant* results = new Variant;

  switch (sq_gettype(svm, pos))
    {
      case (OT_NULL):
        {
          *results = 0;
          break;
        }

      case (OT_FLOAT):
        {
          float temp;
          sq_getfloat(svm, pos, &temp);
          *results = temp;

          break;
        }

      case (OT_BOOL):
        {
          SQBool temp;
          sq_getbool(svm, pos, &temp);
          *results = static_cast<bool>(temp);

          break;
        }

      case (OT_INTEGER):
        {
          SQInteger temp;
          sq_getinteger(svm, pos, &temp);
          *results = static_cast<int>(temp);

          break;
        }

      case (OT_STRING):
        {
          int size = static_cast<int>(sq_getsize(svm, pos));
          SQChar* temp[size];
          sq_getstring(svm, pos, (const SQChar**)&temp);

          *results = std::string((char*)*temp);

          break;
        }

      case (OT_ARRAY):
        {
          int size = sq_getsize(svm, pos);
          int cur_item;

          std::vector<Variant*> moar_results;

          for (cur_item = 0; cur_item < size; cur_item++)
            {
              sq_pushinteger(svm, cur_item);
              sq_get(svm, pos);
              moar_results.push_back(fragrant::getVariantFromSquirrel(svm,
                sq_gettop(svm)));
              sq_pop(svm, 1);
            }

          *results = moar_results;

          break;
        }

      default:
        {}  // anything else isn't supported yet
    }

  return results;
}

void fragrant::deleteVariants(std::vector<Variant*> variants)
{
  int cur;
  for (cur = 0; cur < variants.size(); cur++)
    if (variants.at(cur) &&
        variants.at(cur)->verifyType<std::vector<Variant*> >())
      {
        fragrant::deleteVariants(variants.at(cur)->
          get<std::vector<Variant*> >());
        delete variants.at(cur);
      }
    else
      delete variants.at(cur);

  return;
}

SquirrelVM::SquirrelVM() : squirrel_vm(0)
{
  squirrel_vms.push_back(this);
  index = squirrel_vms.size() - 1;
}

SquirrelVM::~SquirrelVM()
{
  squirrel_vms.erase(squirrel_vms.begin() + index);
}

bool SquirrelVM::initVM()
{
  squirrel_vm = sq_open(VM_SIZE);
  sqstd_seterrorhandlers(squirrel_vm);
  sq_pushroottable(squirrel_vm);
  sq_setprintfunc(squirrel_vm, &print_func);
  sqstd_register_mathlib(squirrel_vm);

  return true;
}

void SquirrelVM::closeVM()
{
  if (squirrel_vm)
    sq_close(squirrel_vm);

  delete this;

  return;
}

void SquirrelVM::execString(std::string data)
{
  cur_index = index;

  sq_compilebuffer(squirrel_vm, data.c_str(), data.size(),
    BUFFER_NAME.c_str(), true);
  sq_pushroottable(squirrel_vm);
  sq_call(squirrel_vm, 1, 0, true);

  return;
}

void SquirrelVM::wrapFunction(std::string funcname, func function)
{
  functions[funcname] = function;

  sq_pushroottable(squirrel_vm);
  sq_pushstring(squirrel_vm, funcname.c_str(), funcname.size());
  sq_newclosure(squirrel_vm, &centralFunction, 0);
  sq_setnativeclosurename(squirrel_vm, 4, funcname.c_str());
  sq_createslot(squirrel_vm, 1);
  sq_pop(squirrel_vm, 1);

  return;
}

void SquirrelVM::wrapClass(ScriptedClass* dclass)
{
  bool exists = false;
  int max = -1;

  std::map<int, ScriptedClass*>::iterator iter;
  for (iter = class_pointers.begin(); iter != class_pointers.end(); iter++)
    {
      if (iter->second->getClassName() == dclass->getClassName())
        exists = true;

      max = (iter->first > max) ? iter->first : max;
    }

  max++;

  if (!exists)
    {
      pcrecpp::RE name_re("\\$NAME");
      pcrecpp::RE func_re("\\$FUNC");
      pcrecpp::RE funcname_re("\\$FUNCNAME");

      std::string function_string;

      int cur;
      std::vector<std::string> funcs = dclass->getClassFunctions();
      for (cur = 0; cur < funcs.size(); cur++)
        {
          std::string cur_func = funcs.at(cur);

          std::string temp = CLASS_FUNC_STRING;
          funcname_re.GlobalReplace(cur_func, &temp);
          function_string += temp;

          cur_func = CLASS_MARKER + cur_func;

          // wrap individual functions
          sq_pushroottable(squirrel_vm);
          sq_pushstring(squirrel_vm, cur_func.c_str(), cur_func.size());
          sq_newclosure(squirrel_vm, &centralFunction, 0);
          sq_setnativeclosurename(squirrel_vm, -1, cur_func.c_str());
          sq_createslot(squirrel_vm, -3);
          sq_pop(squirrel_vm, 1);
        }

      std::string results = CLASS_STRING;

      name_re.GlobalReplace(dclass->getClassName(), &results);
      func_re.GlobalReplace(function_string, &results);

      this->execString(results);
    }

  class_pointers[max] = dclass;

  return;
}

int SquirrelVM::cur_index = -1;
std::vector<SquirrelVM*> SquirrelVM::squirrel_vms;

int SquirrelVM::callback(HSQUIRRELVM svm)
{
  SQStackInfos stack_info;
  sq_stackinfos(svm, 0, &stack_info);
  std::string funcname = std::string(stack_info.funcname);

  bool is_class_function =
    (funcname.size() > CLASS_MARKER.size() &&
     funcname.substr(0, CLASS_MARKER.size()) == CLASS_MARKER) ?
    (true) : (false);

  if (is_class_function)
    funcname = funcname.substr(CLASS_MARKER.size());

  std::vector<Variant*> params;
  int num_args = sq_gettop(svm);

  int cur;
  for (cur = 2; cur <= num_args; cur++)
    params.push_back(fragrant::getVariantFromSquirrel(svm, cur));

  Variant* results = 0;

  if (is_class_function)
    {
      std::vector<Variant*> nparams =
        params.at(1)->get<std::vector<Variant*> >();
      results = class_pointers[params.at(0)->get<int>()]->
        callFunction(funcname,nparams , this);
    }

  else
    results = functions[funcname](params);

  if (results)
    {
      putVariantIntoSquirrel(results, svm);
      params.push_back(results);
    }

  deleteVariants(params);

  return results ? 1 : 0;
}

void SquirrelVM::putVariantIntoSquirrel(Variant* data, HSQUIRRELVM svm)
{
  if (data->verifyType<int>())
    {
      sq_pushinteger(svm, data->get<int>());
      return;
    }

  if (data->verifyType<std::vector<Variant*> >())
    {
      std::vector<Variant*> array_list = data->get<std::vector<Variant*> >();

      sq_newarray(svm, 0);

      int cur;
      for (cur = 0; cur < array_list.size(); cur++)
        {
          putVariantIntoSquirrel(array_list.at(cur), svm);
          sq_arrayappend(svm, -2);
        }

      return;
    }

  if (data->verifyType<bool>())
    {
      sq_pushbool(svm, data->get<bool>());
      return;
    }

  if (data->verifyType<float>())
    {
      sq_pushfloat(svm, data->get<float>());
      return;
    }

  if (data->verifyType<std::string>())
    {
      sq_pushstring(svm, static_cast<SQChar*>((SQChar*)
        data->get<std::string>().c_str()), data->get<std::string>().size());
      return;
    }

  if (data->verifyType<ScriptedClass*>())
    {
      wrapClass(data->get<ScriptedClass*>());

      std::map<int, ScriptedClass*>::iterator iter;
      int idx;
      for (iter = class_pointers.begin(); iter != class_pointers.end(); iter++)
        if (iter->second == data->get<ScriptedClass*>())
          idx = iter->first;

      sq_pushroottable(svm);
      sq_pushstring(svm, (data->get<ScriptedClass*>()->
        getClassName() + CLASS_CONSTRUCTOR).c_str(), -1);
      sq_get(svm, -2);
      sq_pushroottable(svm);
      sq_pushinteger(svm, idx);
      sq_call(svm, 2, true, false);
      sq_remove(svm, -2);
      sq_remove(svm, -2);

      return;
    }

  sq_pushnull(svm);

  return;
}

SQInteger SquirrelVM::centralFunction(HSQUIRRELVM squirrel_vm)
{
  return squirrel_vms[cur_index]->callback(squirrel_vm);
}

void SquirrelVM::print_func(HSQUIRRELVM v, const SQChar* s, ...)
{
  va_list arglist; 
  va_start(arglist, s);
  vprintf(s, arglist);
  va_end(arglist); 

  return;
}
