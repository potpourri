// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// A ScriptVM implementation using Squirrel

#ifndef __SQUIRRELVM_H
#define __SQUIRRELVM_H

#include "../../include/plugins/script/ScriptVM.h"

#include <vector>
#include <map>
#include <cstdarg>

#include <squirrel/squirrel.h>
#include <squirrel/sqstdio.h>
#include <squirrel/sqstdaux.h>
#include <squirrel/sqstdmath.h>

namespace fragrant
{
  const int VM_SIZE = 1024;
  const std::string CLASS_MARKER = "c_";
  const std::string BUFFER_NAME = "this is unhelpful";
  const std::string CLASS_CONSTRUCTOR = "_make";
  const std::string CLASS_STRING = std::string(
    "class $NAME\n"
    "{\n"
    "  constructor(i)\n"
    "    {\n"
    "      idx = i;\n"
    "    }\n"
    "$FUNC"
    "  idx = -1;\n"
    "};\n") + std::string(
    "function $NAME") + CLASS_CONSTRUCTOR + std::string(
    "(i)\n"
    "{\n"
    "  local n = $NAME(i);\n"
    "  return n;\n"
    "}\n\n");
  const std::string CLASS_FUNC_STRING =
    "  function $FUNCNAME(...)\n"
    "    {\n"
    "      local arry = [];\n"
    "      for (local i = 0; i < vargc; i++)\n"
    "        arry.push(vargv[i]);\n"
    "      local results = c_$FUNCNAME(idx, arry);\n"
    "      return results;\n"
    "    }\n\n";

  Variant* getVariantFromSquirrel(HSQUIRRELVM svm, int pos);
  void deleteVariants(std::vector<Variant*> variants);

  class SquirrelVM : public ScriptVM
    {
      public:
        SquirrelVM();
        ~SquirrelVM();

        // script vm stuff

         bool initVM();
         void closeVM();

         void execString(std::string data);

         void wrapFunction(std::string name, func function);
         void wrapClass(ScriptedClass* dclass);

      private:
        HSQUIRRELVM squirrel_vm;

        std::map<std::string, func> functions;
        std::map<int, ScriptedClass*> class_pointers;

        int index;
        static int cur_index;
        static std::vector<SquirrelVM*> squirrel_vms;

        int callback(HSQUIRRELVM svm);

        void putVariantIntoSquirrel(Variant* data, HSQUIRRELVM svm);

        static SQInteger centralFunction(HSQUIRRELVM squirrel_vm);
        static void print_func(HSQUIRRELVM v, const SQChar* s, ...);
    };
}

#endif
