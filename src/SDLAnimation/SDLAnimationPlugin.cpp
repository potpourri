// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The sauce to the plugin machinery

#include "SDLAnimationPlugin.h"
#include "parse_animation.h"

#include "SDL/SDL_image.h"
#include "SDL/SDL.h"

#include "../SDL/SDLManager.h"

#include <stdexcept>
#include <algorithm>

using namespace fragrant;
using namespace XMLParser;
using namespace parse_animation;

bool isvalidXMLAnimationData(XMLAnimationData data)
{
  int cur;

  std::vector<std::string> section_names;

  for (cur = 0; cur < data.sections.size(); cur++)
    {
      if (data.sources.find(data.sections.at(cur).source) ==
          data.sources.end())
        return false;

      section_names.push_back(data.sections.at(cur).name);
    }

  for (cur = 0; cur < data.sequence.size(); cur++)
    if (std::find(section_names.begin(), section_names.end(),
                  data.sequence.at(cur).source) == section_names.end())
      return false;

  return true;
}

XMLAnimationData pruneData(XMLAnimationData& data)
{
  XMLAnimationData ndata;
  int cur;

  for (cur = 0; cur < data.sequence.size(); cur++)
    {
      XMLAnimationFrame cur_frame = data.sequence.at(cur);

      XMLAnimationSection cur_section;

      int ncur;
      for (ncur = 0; ncur < data.sections.size(); ncur++)
        if (data.sections.at(ncur).name == cur_frame.source)
          cur_section = data.sections.at(ncur);

      bool found = false;
      for (ncur = 0; ncur < ndata.sections.size(); ncur++)
        if (cur_section.name == ndata.sections.at(ncur).name)
          found = true;

      if (!found)
        ndata.sections.push_back(cur_section);
      ndata.sources[cur_section.source] =
        data.sources[cur_section.source];
      ndata.sequence.push_back(cur_frame);
    }

  return ndata;
}

void insert_animation_data(XMLAnimationData& data,
  XMLAnimationData& ndata)
{
  data.sections.insert(data.sections.end(),
    ndata.sections.begin(), ndata.sections.end());
  data.sequence.insert(data.sequence.end(),
    ndata.sequence.begin(), ndata.sequence.end());
  data.sources.insert(ndata.sources.begin(), ndata.sources.end());

  return;
}

Drawable* make_animation(fragrant::MediaLoader& loader,
  std::string filename, std::map<std::string,
  fragrant::Variant*> args = std::map<std::string, fragrant::Variant*>())
{
  std::string buffer;

  try
    {
      buffer = loader.loadMedia(filename);
    }

  catch(std::runtime_error& e)
    {
      std::cerr << "make_animation(): Error loading data\n\t"
                << e.what() << std::endl;
      return 0;
    }

  XMLAnimationData xml_data;

  try
    {
      xml_data = parse_animationFromBuffer(buffer);
    }

  catch(std::runtime_error& e)
    {
      std::cerr << "make_animation(): Error parsing xml\n\t"
                << e.what() << std::endl;
      return 0;
    }

  // no recursive includes

  std::vector<std::string>::iterator niter;
  for (niter = xml_data.includes.begin();
       niter != xml_data.includes.end(); niter++)
    {
      std::string nfile;

      std::vector<std::string> split_path = splitPath(filename);
      std::string base = joinPath(
        std::vector<std::string>(split_path.begin(),
                                 split_path.end() - 1));

      if (isURL(*niter) || !isRelative(*niter))
        nfile = *niter;

      else
        nfile = simplifyPath(base + path_separator + *niter);

      std::string nbuffer;

      try
        {
          nbuffer = loader.loadMedia(nfile);
        }

      catch(std::runtime_error& e)
        {
          std::cerr << "make_animation(): Error loading include file: "
                    << nfile << std::endl;
          continue;
        }

      XMLAnimationData nxml_data;

      try
        {
          nxml_data = parse_animationFromBuffer(nbuffer);
        }

      catch(std::runtime_error& e)
        {
          std::cerr << "make_animation(): Error parsing include file: "
                    << e.what() << std::endl;
          continue;
        }

      insert_animation_data(xml_data, nxml_data);
    }

  if (!isvalidXMLAnimationData(xml_data))
    {
      std::cerr << "make_animation(): invalid animation data"
                << std::endl;
      return 0;
    }

  SDLAnimationData data;
  data.input_manager = loader.getGame()->getInputManager();

  if (args.find("id") != args.end() &&
      args["id"]->verifyType<std::string>())
    data.id = args["id"]->get<std::string>();

  else
    data.id = "";

  XMLAnimationData clean_data = pruneData(xml_data);
  std::vector<std::string> image_sources;

  std::map<std::string, std::string>::iterator iter;
  for (iter = clean_data.sources.begin();
       iter != clean_data.sources.end(); iter++)
    {
      std::string buffer;
      std::vector<std::string> split_path = splitPath(filename);
      std::string base = joinPath(
        std::vector<std::string>(split_path.begin(),
                                 split_path.end() - 1));
      std::string filename =
        (isURL(iter->second) || !isRelative(iter->second)) ?
        (iter->second) :
        (simplifyPath(base + path_separator + iter->second));

      image_sources.push_back(iter->first);

      try
        {
          buffer = loader.loadMedia(filename);
        }

      catch(std::runtime_error& e)
        {
          std::cerr << "make_animation(): failed to load file:\n\t"
                    << filename << std::endl;

          SDLAnimation d = SDLAnimation(data); // baleet data
          return 0;
        }

      SDL_RWops* const_mem_sauce = SDL_RWFromConstMem(
        (const void*)buffer.c_str(), buffer.size());
      SDL_Surface* surf = IMG_Load_RW(const_mem_sauce, 1);
      data.root_surfaces.push_back(surf);
    }

  int cur;
  std::vector<std::string> section_list;
  for (cur = 0; cur < clean_data.sections.size(); cur++)
    {
      XMLAnimationSection cur_section = clean_data.sections.at(cur);

      int pos = 0;
      while (pos < image_sources.size())
        {
          if (image_sources.at(pos) == cur_section.source)
            break;

          pos++;
        }

      SDL_Surface* sauce = data.root_surfaces.at(pos);

      char* pixels = static_cast<char*>(sauce->pixels) +
        (sauce->pitch * cur_section.pos.y) +
        (sauce->format->BytesPerPixel * cur_section.pos.x);
      SDL_Surface* nsurf = SDL_CreateRGBSurfaceFrom(pixels,
        cur_section.size.x, cur_section.size.y, sauce->format->BitsPerPixel, 
        sauce->pitch, sauce->format->Rmask, sauce->format->Gmask, 
        sauce->format->Bmask, sauce->format->Amask);

      SDLAnimationFrame frame;
      frame.image = nsurf;
      frame.offset = cur_section.offset;

      data.images.push_back(frame);
      section_list.push_back(cur_section.name);
    }

  for (cur = 0; cur < clean_data.sequence.size(); cur++)
    {
      XMLAnimationFrame cur_frame = clean_data.sequence.at(cur);

      int num;
      for (num = 0; num < section_list.size(); num++)
        if (section_list.at(num) == cur_frame.source)
          break;

      Pair<int> nframe;
      nframe.x = num;
      nframe.y = cur_frame.duration;

      data.frames.push_back(nframe);
    }

  return dynamic_cast<Drawable*>(new SDLAnimation(data));
}

void destroy_image(Drawable* item)
{
  delete dynamic_cast<SDLAnimation*>(item);

  return;
}

PluginData queryPlugin()
{
  PluginData data;

  data.name = "SDL Graphics Animation Plugin";
  data.type = PT_Graphics;
  data.version = "1.0";
  data.authors.push_back("Brian Caine");
  data.requirements.push_back("libSDLPlugin.so");

  return data;
}

Variant* loadPluginPayload()
{
  Variant* payload = new Variant;

  Graphics results;
  results.makeDisplay = 0;
  results.makeImage = make_animation;
  results.deleteImage = destroy_image;

  *payload = results;

  return payload;
}
