// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// Parses the animation xml file.

#ifndef __PARSE_ANIMATION_H
#define __PARSE_ANIMATION_H

#include <libxml++/libxml++.h>

#include "../../include/core/XmlExtra.h"
#include "../../include/plugins/graphics/Graphics.h"

#include <string>
#include <map>
#include <vector>

namespace XMLParser
{
  namespace parse_animation
    {
      struct XMLAnimationSection
        {
          std::string name;
          std::string source;

          GraphicsPair pos;
          GraphicsPair size;
          GraphicsPair offset;
        };

      struct XMLAnimationFrame
        {
          std::string source;
          int duration;
        };

      struct XMLAnimationData
        {
          std::map<std::string, std::string> sources;
          // sources[name] = filename;

          std::vector<XMLAnimationSection> sections;
          std::vector<std::string> includes;
          std::vector<XMLAnimationFrame> sequence;
        };

      XMLAnimationData parse_animationFromBuffer(std::string buffer);
      XMLAnimationData parse_animation(xmlpp::Node* node);

      std::map<std::string, std::string> parse_sources(xmlpp::Node* node);
      std::vector<XMLAnimationSection> parse_sections(xmlpp::Node* node);
      std::vector<XMLAnimationFrame> parse_sequence(xmlpp::Node* node);
    }
}

#endif
