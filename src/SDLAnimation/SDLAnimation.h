// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A drawable thing

#ifndef __SDLANIMATION_H
#define __SDLANIMATION_H

#include <SDL/SDL.h>

#include "../../include/plugins/graphics/Graphics.h"
#include "../../include/plugins/input/InputManager.h"
#include "../../include/core/Game.h"
#include "../SDLPlugin/SDLSurface.h"

namespace fragrant
{
  const std::string ANIMATION_LOOP_EVENT = "SDLAnimation_Loop";

  struct SDLAnimationFrame
    {
      SDL_Surface* image;
      Pair<int> offset;
    };

  struct SDLAnimationData
    {
      std::vector<SDLAnimationFrame> images;
      std::vector<Pair<int> > frames;  // Pair.x = frame; .y = duration

      std::vector<SDL_Surface*> root_surfaces;
      InputManager* input_manager;
      std::string id;
    };

  class SDLAnimation : public Drawable, public ScriptedClass
    {
      public:
        SDLAnimation(SDLAnimationData ndata);
        ~SDLAnimation();

        void draw(Target* target, GraphicsRect* destrect,
          GraphicsPair* srcrect, float angle);
        ScriptedClass* getScriptObject();
        void destroy();

        // script stuff

        func getConstructor();

        std::string getClassName();
        std::vector<std::string> getClassFunctions();

        Variant* callFunction(std::string funcname,
          std::vector<Variant*> params, ScriptVM* script_vm);

      protected:
        SDLAnimationData data;

        int cur_frame;
        int time;
        int ticks_left;
        bool paused;
    };
}

#endif
