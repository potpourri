// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// uhh, sauce?

#include "SDLAnimation.h"
#include "../SDLPlugin/SDLSurface.h"

#include "../../include/core/common.h"
#include "../../include/core/Actor.h"

#include <SDL/SDL_rotozoom.h>

using namespace fragrant;

SDLAnimation::SDLAnimation(SDLAnimationData ndata)
{
  data = ndata;
  cur_frame = -1;
  time = -1;
  paused = false;
}

SDLAnimation::~SDLAnimation()
{
  this->destroy();
}

GraphicsPair makePair(int x, int y)
{
  GraphicsPair results;
  results.x = x;
  results.y = y;

  return results;
}

GraphicsPair rotate_point(GraphicsPair src, float angle)
{
  src.y = -src.y;

  float radius = sqrt(pow(src.x, 2) + pow(src.y, 2));
  float nangle = (radius > 0) ? fromRadians(acos(src.x / radius)) : 0;

  if (src.x == 0 && src.y == 0)
    return src;

  if (angle == 0)
    return makePair(src.x, -src.y);

  while (angle < 0)
    angle += 360;
  angle = static_cast<int>(angle) % 360;

  angle += nangle;

  GraphicsPair results = makePair(
    static_cast<int>(radius * cos(toRadians(angle))),
    -static_cast<int>(radius * sin(toRadians(angle))));

  return results;
}

void SDLAnimation::draw(Target* target, GraphicsRect* destrect,
                        GraphicsPair* srcrect, float angle)
{
  if (cur_frame == -1 && time == -1)
    {
      cur_frame = 0;
      time = SDL_GetTicks();
      ticks_left = 0;
    }

  else
    {
      int ntime = SDL_GetTicks();

      if (!paused)
        ticks_left -= (ntime - time);

      time = ntime;

      while (ticks_left <= 0)
        {
          cur_frame++;

          if (cur_frame >= data.frames.size())
            {
              Event event;
              event.name = ANIMATION_LOOP_EVENT;

              if (data.id != "")
                {
                  event.data["id"] = new Variant;
                  *(event.data["id"]) = data.id;
                }

              data.input_manager->appendEvent(event);
            }

          cur_frame %= data.frames.size();

          ticks_left = data.frames.at(cur_frame).y + ticks_left;
        }
    }

  SDLSurface* target_surf = dynamic_cast<SDLSurface*>(target);

  // sometime later make this more efficient

  SDLAnimationFrame current_frame_data = data.images.at(cur_frame);
  SDL_Surface* rotated_image = (angle == 0.0) ?
    (current_frame_data.image) :
    (rotozoomSurface(current_frame_data.image, angle, 1, SMOOTH));
  SDL_Surface* dest_surf = target_surf->surf;
  bool free_me = rotated_image != current_frame_data.image;

  SDL_Rect destrect_sdl = {0, 0, 0, 0};
  SDL_Rect srcrect_sdl = {0, 0, rotated_image->w, rotated_image->h};

  Pair<float> offset = getOffset(
    makePair(current_frame_data.image->w,current_frame_data.image->h), angle);
  GraphicsPair sub_offset = rotate_point(
    ::makePair(static_cast<int>(current_frame_data.offset.x),
               static_cast<int>(current_frame_data.offset.y)), angle);

  if (destrect)
    {
      destrect_sdl.x = destrect->xy.x + offset.x + sub_offset.x;
      destrect_sdl.y = destrect->xy.y + offset.y + sub_offset.y;
    }

  SDL_BlitSurface(rotated_image, &srcrect_sdl, dest_surf, &destrect_sdl);

  if (srcrect)
    {
      srcrect->x = srcrect_sdl.x;
      srcrect->y = srcrect_sdl.y;
    }

  if (free_me)
    SDL_FreeSurface(rotated_image);

  return;
}

ScriptedClass* SDLAnimation::getScriptObject()
{
  return dynamic_cast<ScriptedClass*>(this);
}

void SDLAnimation::destroy()
{
  int cur;
  for (cur = 0; cur < data.images.size(); cur++)
    SDL_FreeSurface(data.images.at(cur).image);

  for (cur = 0; cur < data.root_surfaces.size(); cur++)
    SDL_FreeSurface(data.root_surfaces.at(cur));

  return;
}

func SDLAnimation::getConstructor()
{
  return 0;
}

std::string SDLAnimation::getClassName()
{
  return "Animation";
}

std::vector<std::string> SDLAnimation::getClassFunctions()
{
  std::vector<std::string> results;

  results.push_back("pause");
  results.push_back("unpause");

  results.push_back("isPaused");

  results.push_back("setFrame");
  results.push_back("getFrame");

  return results;
}

Variant* SDLAnimation::callFunction(std::string funcname,
  std::vector<Variant*> params, ScriptVM* script_vm)
{
  Variant* results = new Variant;

  if (funcname == "pause")
    paused = true;

  if (funcname == "unpause")
    paused = false;

  if (funcname == "isPaused")
    *results = paused;

  if (funcname == "setFrame")
    {
      if (params.size() < 1 ||
          (!params.at(0)->verifyType<float>() &&
           !params.at(0)->verifyType<int>()))
        {
          std::cout
            << "SDLAnimation::callFunction(funcname=\"setFrame\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->verifyType<float>() ?
                static_cast<int>(params.at(0)->get<float>()) :
                params.at(0)->get<int>();

      if (idx >= data.frames.size() || idx < 0)
        {
          std::cout
            << "SDLAnimation::callFunction(funcname=\"setFrame\"):"
               " invalid index"
            << std::endl;
          return results;
        }

      cur_frame = idx;
      ticks_left = data.frames.at(cur_frame).y;
    }

  if (funcname == "getFrame")
    *results = cur_frame;

  return results;
}
