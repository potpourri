// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.
      

// NOTES:

// Parses the animation xml

#include "parse_animation.h"
#include "animation.h"

#include <stdexcept>

using namespace XMLParser;
using namespace parse_animation;

XMLAnimationData XMLParser::parse_animation::
  parse_animationFromBuffer(std::string buffer)
{
  XMLAnimationData results;

  try
    {
      xmlpp::DomParser parser;
      xmlpp::Node* base;

      parser.set_substitute_entities();

      parser.parse_memory(buffer);

      if (parser)
        base = parser.get_document()->get_root_node();
      else
        throw std::runtime_error("some weird error");

      if (!validXML(animation_str, "animation", &parser))
        throw std::runtime_error("invalid");

      results = XMLParser::parse_animation::parse_animation(base);
    }

  catch (std::runtime_error& e)
    {
      throw std::runtime_error(
        std::string("XMLParser::parse_animation::"
          "parse_animationFromBuffer(): xmlpp error: ") + e.what());
    }

  return results;
}

XMLAnimationData XMLParser::parse_animation::
  parse_animation(xmlpp::Node* node)
{
  XMLAnimationData results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {
      xmlpp::Node* cur_node = *iter;

      if (cur_node->get_name() == "import")
        results.includes.push_back(getContents(cur_node));

      if (cur_node->get_name() == "sources")
        results.sources = parse_sources(cur_node);

      if (cur_node->get_name() == "sections")
        results.sections = parse_sections(cur_node);

      if (cur_node->get_name() == "sequence")
        results.sequence = parse_sequence(cur_node);
    }

  return results;
}

std::map<std::string, std::string> XMLParser::parse_animation::
  parse_sources(xmlpp::Node* node)
{
  std::map<std::string, std::string> results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    if ((*iter)->get_name() == "image")
      {
        xmlpp::Node* sub_node = *((*iter)->get_children().begin());

        std::string filename;
        std::string name;

        while (sub_node->get_name() != "filename")
          sub_node = sub_node->get_next_sibling();

        filename = getContents(sub_node);

        while (sub_node->get_name() != "name")
          sub_node = sub_node->get_next_sibling();

        name = getContents(sub_node);

        results[name] = filename;
      }

  return results;
}

GraphicsPair makeGfxPair(int x, int y)
{
  GraphicsPair results;

  results.x = x;
  results.y = y;

  return results;
}

std::vector<XMLAnimationSection> XMLParser::parse_animation::
  parse_sections(xmlpp::Node* node)
{
  std::vector<XMLAnimationSection> results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    if ((*iter)->get_name() == "cut")
      {
        xmlpp::Node* sub_node = *((*iter)->get_children().begin());

        std::string name;
        std::string source;
        int x, y, width, height, offset_x, offset_y;

        while (sub_node->get_name() != "name")
          sub_node = sub_node->get_next_sibling();

        name = getContents(sub_node);

        while (sub_node->get_name() != "source")
          sub_node = sub_node->get_next_sibling();

        source = getContents(sub_node);

        while (sub_node->get_name() != "x")
          sub_node = sub_node->get_next_sibling();

        x = fromString<int>(getContents(sub_node));

        while (sub_node->get_name() != "y")
          sub_node = sub_node->get_next_sibling();

        y = fromString<int>(getContents(sub_node));

        while (sub_node->get_name() != "width")
          sub_node = sub_node->get_next_sibling();

        width = fromString<int>(getContents(sub_node));

        while (sub_node->get_name() != "height")
          sub_node = sub_node->get_next_sibling();

        height = fromString<int>(getContents(sub_node));

        while (sub_node->get_name() != "offset_x")
          sub_node = sub_node->get_next_sibling();

        offset_x = fromString<int>(getContents(sub_node));

        while (sub_node->get_name() != "offset_y")
          sub_node = sub_node->get_next_sibling();

        offset_y = fromString<int>(getContents(sub_node));

        XMLAnimationSection tresults;

        tresults.name = name;
        tresults.source = source;

        tresults.pos = makeGfxPair(x, y);
        tresults.size = makeGfxPair(width, height);
        tresults.offset = makeGfxPair(offset_x, offset_y);

        results.push_back(tresults);
      }

  return results;
}

std::vector<XMLAnimationFrame> XMLParser::parse_animation::
  parse_sequence(xmlpp::Node* node)
{
  std::vector<XMLAnimationFrame> results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    if ((*iter)->get_name() == "frame")
      {
        xmlpp::Node* sub_node = *((*iter)->get_children().begin());

        std::string name;
        int duration;

        while (sub_node->get_name() != "name")
          sub_node = sub_node->get_next_sibling();

        name = getContents(sub_node);

        while (sub_node->get_name() != "duration")
          sub_node = sub_node->get_next_sibling();

        duration = fromString<int>(getContents(sub_node));

        XMLAnimationFrame tresults;
        tresults.source = name;
        tresults.duration = duration;

        results.push_back(tresults);
      }

  return results;
}
