// Copyright 2008 Brian Caine

// This file is part of Potpourri.
// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// Poorly implemented xml parsing

#include "../../include/Global.h"

#include "../../include/core/parselevel.h"
#include "../../dtd/level.h"
#include "../../include/core/XmlExtra.h"

#include <stdexcept>

using namespace XMLParser;
using namespace parselevel;

std::string XMLParser::parselevel::getPresentationName(xmlpp::Node* node)
{
  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;

  for (iter = list.begin(); iter != list.end(); iter++)
    if ((*iter)->get_name() == "name")
      return getContents(*iter);

  throw std::runtime_error("What the fuck? How did this validate?");
}

xmlpp::Node* XMLParser::parselevel::getPresentationData(
  xmlpp::Node* node)
{
  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;

  for (iter = list.begin(); iter != list.end(); iter++)
    if ((*iter)->get_name() == "data")
      return *iter;

  throw std::runtime_error("What the fuck? How did this validate?");
}

fragrant::LevelActor XMLParser::parselevel::getActor(
  xmlpp::Node* node, std::string presentation)
{
  fragrant::LevelActor results;

  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;

  results.zindex = 0.0;
  results.angle = 0.0;

  for (iter = list.begin(); iter != list.end(); iter++)
    {
      if ((*iter)->get_name() == "name")
        results.name = getContents(*iter);

      if ((*iter)->get_name() == "source")
        results.source = getContents(*iter);

      if ((*iter)->get_name() == "position")
        results.position = getPair(*iter, presentation);

      if ((*iter)->get_name() == "zindex")
        results.zindex = fromString<float>(getContents(*iter));

      if ((*iter)->get_name() == "angle")
        results.angle = fromString<float>(getContents(*iter));
    }

  return results;
}

fragrant::LevelGraphicParam parseparam(xmlpp::Node* node)
{
  fragrant::LevelGraphicParam results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {
      xmlpp::Node* cur_node = *iter;

      if (cur_node->get_name() == "name")
        results.name = getContents(cur_node);

      if (cur_node->get_name() == "type")
        results.type = getContents(cur_node);

      if (cur_node->get_name() == "value")
        results.value = getContents(cur_node);
    }

  return results;
}

fragrant::LevelGraphic XMLParser::parselevel::getGraphic(
  xmlpp::Node* node, std::string presentation)
{
  fragrant::LevelGraphic results;

  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;

  results.zindex = 0.0;
  results.angle = 0.0;

  for (iter = list.begin(); iter != list.end(); iter++)
    {
      if ((*iter)->get_name() == "source")
        results.source = getContents(*iter);

      if ((*iter)->get_name() == "position")
        results.position = getPair(*iter, presentation);

      if ((*iter)->get_name() == "zindex")
        results.zindex = fromString<float>(getContents(*iter));

      if ((*iter)->get_name() == "angle")
        results.angle = fromString<float>(getContents(*iter));

      if ((*iter)->get_name() == "param")
        results.params.push_back(parseparam(*iter));
    }

  return results;
}

fragrant::LevelPair XMLParser::parselevel::getPair(
  xmlpp::Node* node, std::string presentation)
{
  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;

  for (iter = list.begin(); iter != list.end(); iter++)
    {
      if ((*iter)->get_name() == "presentation" &&
          getPresentationName(*iter) == presentation)
        {
          Pair<float> temp_pair = parsePair(
            getContents(getPresentationData(*iter)));

          fragrant::LevelPair paar;
          paar.x = static_cast<int>(temp_pair.x);
          paar.y = static_cast<int>(temp_pair.y);

          return paar;
        }
    }

  fragrant::LevelPair paar;
  Pair<float> temp_pair = parsePair(getContents(node));

  paar.x = static_cast<int>(temp_pair.x);
  paar.y = static_cast<int>(temp_pair.y);

  return paar;
}

std::vector<std::string> XMLParser::parselevel::getPresentationNames(
  xmlpp::Node* node)
{
  std::vector<std::string> results;

  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;

  for (iter = list.begin(); iter != list.end(); iter++)
    {
      if (node->get_name() != "presentation")
        {
          std::vector<std::string> temp_list = getPresentationNames(*iter);

          int cur;
          for (cur = 0; cur < temp_list.size(); cur++)
            results.push_back(temp_list.at(cur));
        }

      else
        if ((*iter)->get_name() == "name")
          results.push_back(getContents(*iter));
    }

  return results;
}

fragrant::LevelSlice XMLParser::parselevel::getLevelSlice(
  xmlpp::Node* node, std::string presentation)
{
  fragrant::LevelSlice results;

  results.startup = false;
  results.presentation = presentation;

  xmlpp::Node::NodeList list = node->get_children();
  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {
      if ((*iter)->get_name() == "name")
        results.name = getContents(*iter);

      if ((*iter)->get_name() == "startup")
        results.startup = true;

      if ((*iter)->get_name() == "viewport")
        results.viewport = getPair(*iter, presentation);

      if ((*iter)->get_name() == "actor")
        results.actors.push_back(getActor(*iter, presentation));

      if ((*iter)->get_name() == "presentation")
        if (getPresentationName(*iter) == presentation)
          {
            fragrant::LevelSlice temp_slice =
              getLevelSlice(getPresentationData(*iter), presentation);

            int cur;

            for (cur = 0; cur < temp_slice.actors.size(); cur++)
              results.actors.push_back(temp_slice.actors.at(cur));

            for (cur = 0; cur < temp_slice.static_graphics.size(); cur++)
              results.static_graphics.push_back(
                temp_slice.static_graphics.at(cur));
          }

      if ((*iter)->get_name() == "static_graphic")
        results.static_graphics.push_back(getGraphic(*iter, presentation));
    }

  return results;
}

fragrant::LevelData XMLParser::parselevel::parselevel(xmlpp::Node* node)
{
  fragrant::LevelData results;

  std::vector<std::string> presentation_list = getPresentationNames(node);
  std::vector<fragrant::LevelSlice> slices;

  results.presentations = presentation_list;

  if (!presentation_list.size())
    slices.push_back(getLevelSlice(node, ""));

  int cur;
  for (cur = 0; cur < presentation_list.size(); cur++)
    slices.push_back(getLevelSlice(node, presentation_list.at(cur)));

  if (!results.presentations.size())
    results.presentations.push_back("");

  results.name = slices.at(0).name;

  for (cur = 0; cur < slices.size(); cur++)
    {
      fragrant::LevelSlice cur_slice = slices.at(cur);

      results.slices[cur_slice.presentation] = cur_slice;
    }

  return results;
}

fragrant::LevelData XMLParser::parselevel::parselevelFromBuffer(
  std::string buffer)
{
  fragrant::LevelData results;
  xmlpp::Node* base = 0;

  try
    {
      xmlpp::DomParser parser;
      parser.set_substitute_entities();

      parser.parse_memory(buffer);

      if (parser)
        base = parser.get_document()->get_root_node();
      else
        throw std::runtime_error("some weird error");

      if (!validXML(level_str, "level", &parser))
        throw std::runtime_error("invalid");

      results = XMLParser::parselevel::parselevel(base);;
    }

  catch (const std::exception& e)
    {
      throw std::runtime_error(std::string("XMLParser::parselevel::") +
        "parselevelFrombuffer(): xmlpp error" + e.what());
    }

  return results;
}
