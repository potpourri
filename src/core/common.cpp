// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This is the sauce to my shitty functions

#include "../../include/core/common.h"
#include "../../include/core/Actor.h"

#include <pcrecpp.h>

#include <stdexcept>
#include <iostream>
#include <map>
#include <queue>
#include <cmath>
#include <sys/time.h>

std::string joinPath(std::vector<std::string> path)
{
  std::string result;

  if (!path.size())
    return result;

  std::vector<std::string> sub_path = splitPath(path.at(0));

  if (path.at(0).size() && path.at(0).at(0) == path_separator)
    result += path_separator;

  int cur;
  if (!result.size())
    for (cur = 0; cur < sub_path.size(); cur++)
      result += sub_path.at(cur) + path_separator;
  else
    {
      result += sub_path.at(0).substr(1);
      for (cur = 1; cur < sub_path.size(); cur++)
        result += sub_path.at(cur) + path_separator;
    }

  int mcur;
  for (mcur = 1; mcur < path.size(); mcur++)
    {
      sub_path = splitPath(path.at(mcur));
      for (cur = 0; cur < sub_path.size(); cur++)
        result += sub_path.at(cur) + path_separator;
    }

  return result;
}

std::vector<std::string> splitPath(std::string path)
{
  std::vector<std::string> results;

  if (path.size() && path.at(0) == path_separator)
    results.push_back(std::string("") + path_separator);

  int last_pos = path.find_first_not_of(path_separator, 0);
  int cur_pos = path.find_first_of(path_separator, last_pos);

  while (last_pos != std::string::npos || cur_pos != std::string::npos)
    {
      results.push_back(path.substr(last_pos, cur_pos - last_pos));

      last_pos = path.find_first_not_of(path_separator, cur_pos);
      cur_pos = path.find_first_of(path_separator, last_pos);
    }

  return results;
}

inline bool isParentsOnly(std::vector<std::string> input)
{
  int cur;
  for (cur = 0; cur < input.size(); cur++)
    if (input.at(cur) != parent_dir)
      return false;
  return true;
}

std::string simplifyPath(std::string orig)
{
  std::vector<std::string> split_path = splitPath(joinPath(splitPath(orig)));

  std::vector<std::string> result;

  bool relative = isRelative(orig);

  int parent_count = 0;

  int cur;
  for (cur = 0; cur < split_path.size(); cur++)
    {
      if (split_path.at(cur) != parent_dir &&
          split_path.at(cur) != current_dir)
        {
          parent_count = 0;
          result.push_back(split_path.at(cur));
        }
      else if (split_path.at(cur) == parent_dir)
        {
          if (!result.size() || isParentsOnly(result))
            result.push_back(parent_dir);
          else
            {
              if (parent_count + 1)
                result.pop_back();
              parent_count++;
            }
        }
    }

  if (!relative)
    {
      std::vector<std::string>::iterator iter;
      for (iter = result.begin(); iter != result.end(); iter++)
        if (*iter != parent_dir)
          {
            break;
          }

      result = std::vector<std::string>(iter, result.end());
      result.insert(result.begin(), std::string("") + path_separator);
    }

  return joinPath(result);
}

DirectoryNesting compareDirectories(std::string patha, std::string pathb)
{
  DirectoryNesting result;

  if (!patha.size() || !pathb.size())
    return DN_Unrelated;

  std::vector<std::string> splita = splitPath(simplifyPath(patha));
  std::vector<std::string> splitb = splitPath(simplifyPath(pathb));

  int cur;
  for (cur = 0; cur < splita.size(); cur++)
    {
      if (cur >= splitb.size())
        return DN_FormerInLatter;

      if (splita.at(cur) != splitb.at(cur))
        return DN_Unrelated;
    }

  if (splitb.size() == splita.size())
    return DN_Identical;

  return DN_LatterInFormer;
}

std::vector<std::string> pathDifference(std::string patha, std::string pathb)
{
  std::vector<std::string> results;

  DirectoryNesting nest_value = compareDirectories(patha, pathb);

  if (nest_value == DN_Identical)
    return results;

  // hack
  if (!patha.size())
    return splitPath(simplifyPath(pathb));

  if (nest_value == DN_Unrelated)
    throw std::runtime_error("pathDifference(): Paths are unrelated");

  std::vector<std::string> first;
  std::vector<std::string> second;

  if (nest_value == DN_LatterInFormer)
    {
      first = splitPath(simplifyPath(patha));
      second = splitPath(simplifyPath(pathb));
    }
  else
    {
      first = splitPath(simplifyPath(pathb));
      second = splitPath(simplifyPath(patha));
    }

  results = std::vector<std::string>(
    second.begin() + first.size(), second.end());

  return results;
}

bool isRelative(std::string path)
{
  if (path.size() && path.at(0) == path_separator)
    return false;
  return true;
}

ParsedPath parsePath(std::string path)
{
  ParsedPath results;

  pcrecpp::RE url_split(url_regexp);

  if (!url_split.FullMatch(path, &results.protocol,
                           &results.host, &results.path))
    throw std::runtime_error(
      "parsePath(): invalid url syntax:\n\t" + path);

  int cur;
  for (cur = 0;
       cur < (sizeof(hostless_protocols)/sizeof(std::string));
       cur++)
    if (results.protocol == std::string(hostless_protocols[cur]) &&
        results.host.size())
      {
        results.path = results.host + path_separator + results.path;
        results.host = "";
      }

  results.raw_path = path;
  results.split_path = splitPath(results.path);

  return results;
}

bool isURL(std::string path)
{
  try
    {
      ParsedPath parsed_path = parsePath(path);
      return true;
    }

  catch (...) {}

  return false;
}

std::vector<std::string> splitString(std::string path, std::string delim)
{
  std::vector<std::string> results;

  int last_pos = path.find_first_not_of(delim, 0);
  int cur_pos = path.find_first_of(delim, last_pos);

  while (last_pos != std::string::npos || cur_pos != std::string::npos)
    {
      results.push_back(path.substr(last_pos, cur_pos - last_pos));

      last_pos = path.find_first_not_of(delim, cur_pos);
      cur_pos = path.find_first_of(delim, last_pos);
    }

  return results;
}

float toRadians(float input)
{
  return input * (PI / 180);
}

float fromRadians(float input)
{
  return input * (180 / PI);
}

#include <SDL/SDL.h>
// debug

int getticks()
{
  return SDL_GetTicks(); // debug
}

void dosleep(int count)
{
  timespec amount;
  timespec dont_care;
  amount.tv_sec = count/1000;
  amount.tv_nsec = (count%1000) * 1000000; // i think this is right

  nanosleep(&amount, &dont_care);

  return;
}

Pair<float> getOffset(Pair<float> cur_size, float angle)
{
  if (angle != abs(angle))
    angle += 360;

  angle = static_cast<int>(angle) % 360;

  float cur_radius = sqrt(pow(cur_size.x/2, 2) + pow(cur_size.y/2, 2));
  Pair<float> ul_corner;
  Pair<float> ur_corner;

  float ul_corner_angle = fromRadians(acos((-cur_size.x/2) / cur_radius));
  float ur_corner_angle = fromRadians(acos((cur_size.x/2) / cur_radius));

  ul_corner = fragrant::makePair(
    cur_radius * cos(toRadians(ul_corner_angle + angle)),
    cur_radius * sin(toRadians(ul_corner_angle + angle)));

  ur_corner = fragrant::makePair(
    cur_radius * cos(toRadians(ur_corner_angle + angle)),
    cur_radius * sin(toRadians(ur_corner_angle + angle)));

  Pair<float> nsize;

  if ((static_cast<int>(angle) % 180) < 90)
    nsize = fragrant::makePair(abs(ul_corner.x) * 2, abs(ur_corner.y) * 2);

  if ((static_cast<int>(angle) % 180) >= 90)
    nsize = fragrant::makePair(abs(ur_corner.x) * 2, abs(ul_corner.y) * 2);

  Pair<float> diff;
  diff.x = -(ul_corner.x + (nsize.x/2));
  diff.y = -((nsize.y/2) - ul_corner.y);

  return diff;
}
