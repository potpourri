// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The plugin loader sauce

#include <stdexcept>
#include <sys/types.h>
#include <dirent.h>
#include <errno.h>

#include "../../include/core/PluginLoader.h"
#include "../../include/core/common.h"

using namespace fragrant;

PluginLoader::PluginLoader(std::string folder)
{
  DIR* dp = opendir(folder.c_str());
  std::vector<std::string> results;
  std::vector<std::string> filenames;

  if (!dp)
    {
      throw std::runtime_error(
        "PluginLoader::PluginLoader(): failed opening folder: " + folder);
    }

  dirent* dirp = readdir(dp);

  while (dirp)
    {
      results.push_back(folder + path_separator + std::string(dirp->d_name));
      filenames.push_back(std::string(dirp->d_name));
      dirp = readdir(dp);
    }

  closedir(dp);

  cookie = magic_open(MAGIC_MIME);
  magic_load(cookie, 0);

  if (magic_error(cookie))
    throw std::runtime_error("");

  int cur;
  for (cur = 0; cur < results.size(); cur++)
    {
      std::string file_data;
      try
        {
          file_data = std::string(
            magic_file(cookie, results.at(cur).c_str()));
        }
      catch (std::logic_error& error)
        {
          file_data = "";
        }

      if (file_data.substr(0, SOFILE.size()) == SOFILE)
        {
          Plugin* nplugin = 0;
          try
            {
              nplugin = new Plugin(results.at(cur));
            }
          catch (std::runtime_error& d)
            {}

          if (nplugin)
            {
              plugins[filenames.at(cur)] = nplugin;
              std::cout << "PluginLoader::PluginLoader(): Loaded plugin:"
                        << std::endl << "\tFilename: " << filenames.at(cur)
                        << std::endl << nplugin->getDataString() << std::endl;
            }
        }
    }
}

PluginLoader::~PluginLoader()
{
  std::map<std::string, Plugin*>::iterator iter;

  for (iter = plugins.begin(); iter != plugins.end(); iter++)
    delete iter->second;

  magic_close(cookie);
}

Plugin* PluginLoader::getPlugin(std::string name)
{
  if (plugins.find(name) == plugins.end())
    return 0;

  Plugin* result = plugins[name];
  PluginData data = result->getData();

  int cur;
  for (cur = 0; cur < data.requirements.size(); cur++)
    if (!getPlugin(data.requirements.at(cur)))
      return 0;

  return result;
}

std::vector<Plugin*> PluginLoader::getPluginFromType(PluginType type)
{
  std::vector<Plugin*> results;
  std::map<std::string, Plugin*>::iterator iter;

  for (iter = plugins.begin(); iter != plugins.end(); iter++)
    if (iter->second->getData().type == type)
      results.push_back(iter->second);

  return results;
}
