// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// the sauce

#include "../../include/core/PhysicsObject.h"
#include "../../include/core/common.h"

#include <stdexcept>

using namespace fragrant;

bool isCloseEnough(float num1, float num2, float tolerance);

PhysicsObject::PhysicsObject(ActorPhysics data)
{
  parent = 0;
  fixed = data.fixed;

  if (data.fixed)
    {
      body = cpBodyNew(static_cast<float>(INFINITY),
                       static_cast<float>(INFINITY));
    }

  else
    {
      float moment;

      if (data.generate_rotational_inertia)
        moment = getMoment(data.shapes, data.mass);

      else if (isCloseEnough(data.rotational_inertia, -1, .05))
        moment = static_cast<float>(INFINITY);

      else
        moment = data.rotational_inertia;

      body = cpBodyNew(data.mass, moment);
    }

  int cur;
  for (cur = 0; cur < data.shapes.size(); cur++)
    {
      PhysicsShape cur_shape = data.shapes.at(cur);
      cpShape* nshape = 0;

      switch (cur_shape.shape)
        {
          case (S_Line):
            {
              cur_shape.data.at(0).x += data.base_position.x;
              cur_shape.data.at(0).y += data.base_position.y;

              cur_shape.data.at(1).x += data.base_position.x;
              cur_shape.data.at(1).y += data.base_position.y;

              nshape = cpSegmentShapeNew(body,
                cpv(cur_shape.data.at(0).x, cur_shape.data.at(0).y), 
                cpv(cur_shape.data.at(1).x, cur_shape.data.at(1).y), 
                cur_shape.data.at(2).x);
              break;
            }

          case (S_Circle):
            {
              nshape = cpCircleShapeNew(body,
                cur_shape.data.at(0).x,
                cpv(cur_shape.offset.x, cur_shape.offset.y));
              break;
            }

          case (S_Poly):
            {
              cpVect vect_data[cur_shape.data.size()];

              int cur_vect;
              for (cur_vect = 0; cur_vect < cur_shape.data.size(); cur_vect++)
                vect_data[cur_vect] = cpv(
                  cur_shape.data.at(cur_vect).x,
                  cur_shape.data.at(cur_vect).y);

              nshape = cpPolyShapeNew(body, cur_shape.data.size(),
                vect_data, cpv(cur_shape.offset.x, cur_shape.offset.y));
              break;
            }
        }

      nshape->e = cur_shape.elasticity;
      nshape->u = cur_shape.friction;
      nshape->collision_type = COLLISION_TYPE;
      nshape->data = (void*) data.the_actor;

      shapes.push_back(nshape);
    }
}

PhysicsObject::~PhysicsObject()
{
  if (parent)
    parent->removePhysicsObject(this);

  if (body)
    cpBodyFree(body);

  int cur;
  for (cur = 0; cur < shapes.size(); cur++)
    if (shapes.at(cur))
      cpShapeFree(shapes.at(cur));
}

void PhysicsObject::setPosition(ActorPair npos)
{
  if (!body)
    throw std::runtime_error("PhysicsObject::setPosition(): body is zero");

  body->p.x = npos.x;
  body->p.y = npos.y;

  if (fixed && parent)
    cpSpaceRehashStatic(parent->space);

  return;
}

ActorPair PhysicsObject::getPosition()
{
  if (!body)
    throw std::runtime_error("PhysicsObject::getPosition(): body is zero");

  ActorPair results;

  results.x = body->p.x;
  results.y = body->p.y;

  return results;
}

void PhysicsObject::setAngle(float nangle) // i hate radians
{
  cpBodySetAngle(body, toRadians(nangle));

  if (fixed && parent)
    cpSpaceRehashStatic(parent->space);

  return;
}

float PhysicsObject::getAngle()
{
  if (!fixed)
    return fromRadians(body->a);

  return 0;
}

ActorPair PhysicsObject::getCenter()
{
  cpVect world = cpBodyLocal2World(body, cpv(0,0));
  ActorPair results;

  results.x = world.x;
  results.y = world.y;

  return results;
}

ActorPair PhysicsObject::Object2World(ActorPair input)
{
  cpVect world = cpBodyLocal2World(body, cpv(input.x, input.y));
  return makePair(world.x, world.y);
}

void PhysicsObject::applyImpulse(ActorPair force, ActorPair offset)
{
  cpBodyApplyImpulse(body, cpv(force.x, force.y), cpv(offset.x, offset.y));
  return;
}

ActorPair PhysicsObject::getVelocity()
{
  return makePair(body->v.x, body->v.y);
}

void PhysicsObject::setVelocity(ActorPair nvelocity)
{
  body->v = cpv(nvelocity.x, nvelocity.y);

  return;
}

float PhysicsObject::getAngularVelocity()
{
  return fromRadians(body->w);
}

void PhysicsObject::setAngularVelocity(float nv)
{
  body->w = toRadians(nv);
  return;
}

void PhysicsObject::addToSpace(cpSpace* space)
{
  if (!fixed)
    cpSpaceAddBody(space, body);

  int cur;
  for (cur = 0; cur < shapes.size(); cur++)
    {
      if (fixed)
        cpSpaceAddStaticShape(space, shapes.at(cur));
      else
        cpSpaceAddShape(space, shapes.at(cur));
    }

  return;
}

void PhysicsObject::removeFromSpace(cpSpace* space)
{
  if (!fixed)
    cpSpaceRemoveBody(space, body);

  int cur;
  for (cur = 0; cur < shapes.size(); cur++)
    {
      if (fixed)
        cpSpaceRemoveStaticShape(space, shapes.at(cur));
      else
        cpSpaceRemoveShape(space, shapes.at(cur));
    }

  return;
}

float getMomentForPoly(float mass, PhysicsShape shape)
{
  if (shape.shape != S_Poly)
    return 0.0;

  int cur;

  for (cur = 0; cur < shape.data.size(); cur++)
    {
      shape.data.at(cur).x += shape.offset.x;
      shape.data.at(cur).y += shape.offset.y;
    }

  float sum1 = 0.0;
  float sum2 = 0.0;

  for (cur = 0; cur < shape.data.size(); cur++)
    {
      ActorPair actorpair_1 = shape.data.at(cur);
      ActorPair actorpair_2 = shape.data.at((cur + 1) % shape.data.size());

      cpVect v1 = cpv(actorpair_1.x, actorpair_1.y);
      cpVect v2 = cpv(actorpair_2.x, actorpair_2.y);

      float a = cpvcross(v2, v1);
      float b = cpvdot(v1, v1) + cpvdot(v1, v2) + cpvdot(v2, v2);

      sum1 += a * b;
      sum2 += a;
    }

  return (mass * sum1) / (6.0 * sum2);
}

float PhysicsObject::getMoment(std::vector<PhysicsShape> shapes, float mass)
{
  float results;
  float total;
  int count = 0;

  int cur;
  for (cur = 0; cur < shapes.size(); cur++)
    {
      PhysicsShape cur_shape = shapes.at(cur);

      if (shapes.at(cur).shape == S_Poly)
        {
          total += getMomentForPoly(mass, cur_shape);
          count++;
        }

      if (shapes.at(cur).shape == S_Circle)
        {
          total = cpMomentForCircle(mass, cur_shape.data.at(0).x,
            (cur_shape.data.at(0).x/2), cpv(cur_shape.offset.x,
            cur_shape.offset.y));
          count++;
        }
    }

  return (total / count);
}

bool isCloseEnough(float num1, float num2, float tolerance)
{
  if ((num1 + tolerance) > num2 && (num1 - tolerance) < num2)
    return true;

  return false;
}
