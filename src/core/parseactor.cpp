// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// Poorly implemented xml parsing

#include "../../include/core/parseactor.h"
#include "../../include/core/XmlExtra.h"
#include "../../dtd/actor.h"

#include <stdexcept>
#include <iostream>

namespace XMLParser{
namespace parseactor{

std::string parsename(xmlpp::Node* node)
{
  std::string results;

  results = getContents(node);

  return results;
}
}}

std::string XMLParser::parseactor::parsesource(xmlpp::Node* node)
{
  std::string results;

  results = getContents(node);

  return results;
}

std::string XMLParser::parseactor::parsepair(xmlpp::Node* node)
{
  std::string results;

  results = getContents(node);

  return results;
}

XMLParser::parseactor::pos XMLParser::parseactor::parsepos(xmlpp::Node* node)
{
  XMLParser::parseactor::pos results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "pair")
        {
          std::string temp = parsepair(*iter);
          results.m_pair = temp;
        }

    }

  return results;
}

float XMLParser::parseactor::parseangle(xmlpp::Node* node)
{
  float results;

  results = fromString<float>(getContents(node));

  return results;
}

XMLParser::parseactor::graphics
XMLParser::parseactor::parsegraphics(xmlpp::Node* node)
{
  XMLParser::parseactor::graphics results;
  results.m_angle = 0.0;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "source")
        {
          std::string temp = parsesource(*iter);
          results.m_source = temp;
        }

      if ((*iter)->get_name() == "pos")
        {
          XMLParser::parseactor::pos temp = parsepos(*iter);
          results.m_pos = temp;
        }

      if ((*iter)->get_name() == "angle")
        {
          float temp = parseangle(*iter);
          results.m_angle = temp;
        }

      if ((*iter)->get_name() == "param")
        {
          param temp = parseparam(*iter);
          results.m_params.push_back(temp);
        }
    }

  return results;
}

float XMLParser::parseactor::parsemass(xmlpp::Node* node)
{
  float results;

  results = fromString<float>(getContents(node));

  return results;
}

float XMLParser::parseactor::parseinertia(xmlpp::Node* node)
{
  float results;

  results = fromString<float>(getContents(node));

  return results;
}

bool XMLParser::parseactor::parsefixed(xmlpp::Node*){ return true; }

XMLParser::parseactor::body
XMLParser::parseactor::parsebody(xmlpp::Node* node)
{
  XMLParser::parseactor::body results;
  results.m_fixed = false;
  results.generate_rotational_inertia = true;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "mass")
        {
          float temp = parsemass(*iter);
          results.m_mass = temp;
        }

      if ((*iter)->get_name() == "inertia")
        {
          float temp = parseinertia(*iter);
          results.m_inertia = temp;
        }

      if ((*iter)->get_name() == "rotational_inertia")
        {
          results.generate_rotational_inertia = false;
          float temp = fromString<float>(getContents(*iter));
          results.m_rotational_inertia = temp;
        }

      if ((*iter)->get_name() == "fixed")
        {
          bool temp = parsefixed(*iter);
          results.m_fixed = temp;
        }

    }

  return results;
}

XMLParser::parseactor::data
XMLParser::parseactor::parsedata(xmlpp::Node* node)
{
  XMLParser::parseactor::data results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "pair")
        {
          std::string temp = parsepair(*iter);
          results.m_pair.push_back(temp);
        }

    }

  return results;
}

XMLParser::parseactor::poly
XMLParser::parseactor::parsepoly(xmlpp::Node* node)
{
  XMLParser::parseactor::poly results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "data")
        {
          XMLParser::parseactor::data temp = parsedata(*iter);
          results.m_data = temp;
        }

      if ((*iter)->get_name() == "offset")
        {
          XMLParser::parseactor::offset temp = parseoffset(*iter);
          results.m_offset = temp;
        }

      if ((*iter)->get_name() == "elasticity")
        {
          float temp = parseelasticity(*iter);
          results.m_elasticity = temp;
        }

      if ((*iter)->get_name() == "friction")
        {
          float temp = parsefriction(*iter);
          results.m_friction = temp;
        }

    }

  return results;
}

XMLParser::parseactor::offset
XMLParser::parseactor::parseoffset(xmlpp::Node* node)
{
  XMLParser::parseactor::offset results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "pair")
        {
          std::string temp = parsepair(*iter);
          results.m_pair = temp;
        }

    }

  return results;
}

XMLParser::parseactor::circle
XMLParser::parseactor::parsecircle(xmlpp::Node* node)
{
  XMLParser::parseactor::circle results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "offset")
        {
          offset temp = parseoffset(*iter);
          results.m_offset = temp;
        }

      if ((*iter)->get_name() == "radius")
        {
          float temp = parseradius(*iter);
          results.m_radius = temp;
        }

      if ((*iter)->get_name() == "elasticity")
        {
          float temp = parseelasticity(*iter);
          results.m_elasticity = temp;
        }

      if ((*iter)->get_name() == "friction")
        {
          float temp = parsefriction(*iter);
          results.m_friction = temp;
        }
    }

  return results;
}

float XMLParser::parseactor::parseradius(xmlpp::Node* node)
{
  float results;

  results = fromString<float>(getContents(node));

  return results;
}

XMLParser::parseactor::line
XMLParser::parseactor::parseline(xmlpp::Node* node)
{
  XMLParser::parseactor::line results;
  bool found_first = false;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "pair")
        {
          std::string temp = parsepair(*iter);

          if (!found_first)
            {
              results.m_a = temp;
              found_first = true;
            }

          else
            results.m_b = temp;
        }

      if ((*iter)->get_name() == "radius")
        {
          float temp = parseradius(*iter);
          results.m_radius = temp;
        }

      if ((*iter)->get_name() == "elasticity")
        {
          float temp = parseelasticity(*iter);
          results.m_elasticity = temp;
        }

      if ((*iter)->get_name() == "friction")
        {
          float temp = parsefriction(*iter);
          results.m_friction = temp;
        }
    }

  return results;
}

XMLParser::parseactor::physics
XMLParser::parseactor::parsephysics(xmlpp::Node* node)
{
  XMLParser::parseactor::physics results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "body")
        {
          body temp = parsebody(*iter);
          results.m_body = temp;
        }

      if ((*iter)->get_name() == "poly")
        {
          poly temp = parsepoly(*iter);
          results.m_poly.push_back(temp);
        }

      if ((*iter)->get_name() == "circle")
        {
          circle temp = parsecircle(*iter);
          results.m_circle.push_back(temp);
        }

      if ((*iter)->get_name() == "line")
        {
          line temp = parseline(*iter);
          results.m_line.push_back(temp);
        }

    }

  return results;
}
std::string XMLParser::parseactor::parseevent(xmlpp::Node* node)
{
  std::string results;

  results = getContents(node);

  return results;
}

std::string XMLParser::parseactor::parsescript(xmlpp::Node* node)
{
  std::string results;

  results = getContents(node);

  return results;
}

namespace XMLParser{
namespace parseactor{
reaction parsereaction(xmlpp::Node* node)
{
  reaction results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "event")
        {
          std::string temp = parseevent(*iter);
          results.m_event = temp;
        }

      if ((*iter)->get_name() == "script")
        {
          std::string temp = parsescript(*iter);
          results.m_script = temp;
        }

    }

  return results;
}
}}

XMLParser::parseactor::actor
XMLParser::parseactor::parseactor(xmlpp::Node* node)
{
  XMLParser::parseactor::actor results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {

      if ((*iter)->get_name() == "name")
        {
          std::string temp = parsename(*iter);
          results.m_name = temp;
        }

      if ((*iter)->get_name() == "graphics")
        {
          graphics temp = parsegraphics(*iter);
          results.m_graphics.push_back(temp);
        }

      if ((*iter)->get_name() == "physics")
        {
          physics temp = parsephysics(*iter);
          results.m_physics = temp;
        }

      if ((*iter)->get_name() == "reaction")
        {
          reaction temp = parsereaction(*iter);
          results.m_reaction.push_back(temp);
        }

    }

  return results;
}

namespace XMLParser{
namespace parseactor{
actor parseactorFromBuffer(std::string buffer)
{
  actor results;

  xmlpp::Node* base = 0;

  try
    {
      xmlpp::DomParser parser;
      parser.set_substitute_entities();

      parser.parse_memory(buffer);

      if (parser)
        base = parser.get_document()->get_root_node();
      else
        throw std::runtime_error("some weird error");

      if (!validXML(actor_str, "actor", &parser))
        throw std::runtime_error("invalid");

      results = XMLParser::parseactor::parseactor(base);
    }

  catch (const std::exception& e)
    {
      throw std::runtime_error(std::string(
        "XMLParser::parseactor::parseactorFromBuffer(): xmlpp error: ") +
        e.what());
    }

  return results;
}
}}

XMLParser::parseactor::param
XMLParser::parseactor::parseparam(xmlpp::Node* node)
{
  XMLParser::parseactor::param results;

  xmlpp::Node::NodeList list = node->get_children();

  xmlpp::Node::NodeList::iterator iter;
  for (iter = list.begin(); iter != list.end(); iter++)
    {
      xmlpp::Node* cur_node = *iter;

      if (cur_node->get_name() == "name")
        results.name = getContents(cur_node);

      if (cur_node->get_name() == "type")
        results.type = getContents(cur_node);

      if (cur_node->get_name() == "value")
        results.value = getContents(cur_node);
    }

  return results;
}

float XMLParser::parseactor::parseelasticity(xmlpp::Node* node)
{
  float results;

  results = fromString<float>(getContents(node));

  return results;
}

float XMLParser::parseactor::parsefriction(xmlpp::Node* node)
{
  float results;

  results = fromString<float>(getContents(node));

  return results;
}
