// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The main function, of course

#include "../../include/core/Engine.h"

#include <iostream>
#include <stdexcept>
#include <cstdlib>

fragrant::EngineParams parseParams(int argv, char* argc[]);

int main(int argv, char* argc[])
{
  fragrant::EngineParams params;

  try
    {
      params = parseParams(argv, argc);
    }

  catch  (std::runtime_error& e)
    {
      std::cout << "potpourri: Wrong params\n\t" << e.what() << std::endl;
      return EXIT_FAILURE;
    }

  fragrant::Engine engine;
  fragrant::EngineResults results = engine.run(params);

  return results == fragrant::ER_Good ? EXIT_SUCCESS : EXIT_FAILURE;
}

fragrant::EngineParams parseParams(int argc, char* argv[])
{
  fragrant::EngineParams results;

  int cur;
  for (cur = 1; cur < argc; cur++)
    {
      if (std::string(argv[cur]) == "-p")
        {
          results.params[fragrant::PLUGINFOLDER] = argv[cur + 1];
          cur++;
          continue;
        }

      if (results.params.find(fragrant::GAME) == results.params.end())
        {
          results.params[fragrant::GAME] = argv[cur];
          continue;
        }

      if (results.params.find(fragrant::PRESENTATION) ==
          results.params.end())
        {
          results.params[fragrant::PRESENTATION] = argv[cur];
          continue;
        }
    }

  return results;
}
