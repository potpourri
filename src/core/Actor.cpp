// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The source for the actor class.

#include <iostream>
#include <string>
#include <stdexcept>

#include <chipmunk.h> // will this work?

#include "../../include/core/Actor.h"
#include "../../include/core/parseactor.h"
#include "../../include/core/XmlExtra.h"

using namespace fragrant;

ActorPair fragrant::makePair(float x, float y)
{
  ActorPair results;
  results.x = x;
  results.y = y;

  return results;
}

void clearStringToVariantMap(std::map<std::string, Variant*> params)
{
  std::map<std::string, Variant*>::iterator iter;

  for (iter = params.begin(); iter != params.end(); iter++)
    delete iter->second;

  return;
}

std::map<std::string, Variant*>
  stringToVariantMap(std::map<std::string, Pair<std::string> > params)
{
  std::map<std::string, Variant*> results;
  std::map<std::string, Pair<std::string> >::iterator iter;

  for (iter = params.begin(); iter != params.end(); iter++)
    {
      results[iter->first] = new Variant;

      if (iter->second.x == "std::string")
        {
          *(results[iter->first]) = iter->second.y;
          continue;
        }

      if (iter->second.x == "bool")
        {
          *(results[iter->first]) = fromString<bool>(iter->second.y);
          continue;
        }

      if (iter->second.x == "int")
        {
          *(results[iter->first]) = fromString<int>(iter->second.y);
          continue;
        }

      if (iter->second.x == "float")
        {
          *(results[iter->first]) = fromString<float>(iter->second.y);
          continue;
        }

      delete results[iter->first];
      results.erase(results.find(iter->first));

      std::cerr << "stringToVariantMap(): failed "
                   "to identify type, \""
                << iter->second.x << "\"" << std::endl;
    }

  return results;
}

Actor::Actor(ActorData data)
{
  persistent = false; // debug
  initial_params = data;

  int cur;

  for (cur = 0; cur < data.graphics.size(); cur++)
    {
      ActorGraphics c_graphics = data.graphics.at(cur);

      ActorGraphic ngraphic;
      ngraphic.position.x = static_cast<int>(c_graphics.pos.x);
      ngraphic.position.y = static_cast<int>(c_graphics.pos.y);
      ngraphic.angle = c_graphics.angle;
      ngraphic.name = c_graphics.sauce;

      std::map<std::string, Variant*> params =
        stringToVariantMap(c_graphics.params);
      ngraphic.graphic = data.game->getGraphics(c_graphics.sauce, params);
      clearStringToVariantMap(params);

      sprites.push_back(ngraphic);
    }

  data.physics.base_position.x = data.position.x;
  data.physics.base_position.y = INV_Y - data.position.y;
  data.physics.the_actor = this;

  physics_object = new PhysicsObject(data.physics);
  physics_object->setPosition(makePair(
    data.position.x, INV_Y - data.position.y));
  data.simulation->addPhysicsObject(physics_object);
}

Actor::~Actor()
{
  delete physics_object;
}

void Actor::draw(Target* target, LevelPair offset)
{
  int cur;
  for (cur = 0; cur < sprites.size(); cur++)
    {
      ActorGraphic cur_graphic = sprites.at(cur);

      ActorPair physics_pos = physics_object->Object2World(
        makePair(cur_graphic.position.x, -cur_graphic.position.y));

      GraphicsRect src;
      src.xy.x = physics_pos.x - offset.x;
      src.xy.y = (INV_Y - physics_pos.y) - offset.y;

      cur_graphic.graphic->draw(dynamic_cast<Target*>(target), &src,
        static_cast<GraphicsPair*>(0), cur_graphic.angle +
        physics_object->getAngle());
    }

  return;
}

void Actor::setAngle(float nangle)
{
  physics_object->setAngle(nangle);
  return;
}

float Actor::getAngle()
{
  return physics_object->getAngle();
}

std::string Actor::getName()
{
  return initial_params.name;
}

int Actor::getIndex()
{
  return initial_params.level->findIndex(this);
}

void Actor::raiseEvent(Event event)
{
  std::map<std::string, std::string>::iterator iter;

  for (iter = initial_params.scripts.begin();
       iter != initial_params.scripts.end();
       iter++)
      if (matchEvent(iter->first, event))
        {
          std::string script_data;

          try
            {
              script_data = initial_params.game->getFile(iter->second);
            }

          catch (std::runtime_error& e)
            {
              std::cerr << "Actor::raiseEvent(): error with getting "
                           "script:\n\t" << e.what() << std::endl;
              continue;
            }

          ScriptVM::caller = this;
          cur_event = event;
          initial_params.script_vm->execString(script_data);
        }

  return;
}

func Actor::getConstructor()
{
  return 0; // not going to implement yet
}

void Actor::destroy()
{
  return; // probably not going to implement in the first place
}

std::string Actor::getClassName() { return "Actor"; }

std::vector<std::string> Actor::getClassFunctions()
{
  std::vector<std::string> results;

  results.push_back("getIndex");

  results.push_back("getPosition");
  results.push_back("setPosition");

  results.push_back("getAngle");
  results.push_back("setAngle");

  results.push_back("getVariable");
  results.push_back("setVariable");

  results.push_back("applyImpulse");

  results.push_back("getCurrentEvent");

  results.push_back("getVelocity");
  results.push_back("setVelocity");

  results.push_back("getAngularVelocity");
  results.push_back("setAngularVelocity");

  results.push_back("removeGraphic"); // actor.removeGraphic(idx)
  results.push_back("addGraphic"); // actor.addGraphic(name, pos, angle)

  results.push_back("getGraphicIndex");
  results.push_back("getGraphicListSize");

  results.push_back("getGraphicScriptObject");

  results.push_back("setGraphicPosition");
  results.push_back("getGraphicPosition");

  results.push_back("setGraphicAngle");
  results.push_back("getGraphicAngle");

  return results;
}

Variant* Actor::callFunction(std::string funcname,
  std::vector<Variant*> params, ScriptVM* script_vm)
{
  Variant* results = new Variant;

  if (funcname == "getIndex")
    *results = getIndex();

  if (funcname == "getPosition")
    {
      std::vector<Variant*> pos;
      Variant* var;

      var = new Variant;
      *var = physics_object->getPosition().x;
      pos.push_back(var);

      var = new Variant;
      *var = physics_object->getPosition().y;
      pos.push_back(var);

      *results = pos;
    }

  if (funcname == "setPosition")
    {
      ActorPair npos;

      if (params.size() < 2)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setPosition\"): wrong params"
            << std::endl;
          return results;
        }

      try
        {
          npos.x = (params.at(0)->verifyType<int>() ?
                    params.at(0)->get<int>() :
                    params.at(0)->get<float>());
          npos.y = (params.at(1)->verifyType<int>() ?
                    params.at(1)->get<int>() :
                    params.at(1)->get<float>());
        }

      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setPosition\"): wrong params"
            << std::endl;
          return results;
        }

      physics_object->setPosition(npos);
    }

  if (funcname == "getAngle")
    *results = physics_object->getAngle();

  if (funcname == "setAngle")
    {
      if (params.size() < 1)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setAngle\"): wrong params"
            << std::endl;
          return results;
        }

      try
        {
          float data = (params.at(0)->verifyType<int>() ?
                        static_cast<float>(params.at(0)->get<int>()) :
                        params.at(0)->get<float>());
          physics_object->setAngle(data);
        }

      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setAngle\"): wrong params"
            << std::endl;
          return results;
        }
    }

  if (funcname == "getVariable")
    {
      if (params.size() < 1)
        {
          std::cout
            << "Actor::callFunction(funcname=\"getVariable\"): wrong params"
            << std::endl;
          return results;
        }

      std::string key;

      try
        {
          key = params.at(0)->get<std::string>();
        }

      catch(...)
        {	 
          std::cout
            << "Actor::callFunction(funcname=\"getVariable\"): wrong params"
            << std::endl;
          return results;
        }

      if (!key.size() || ldata.find(key) == ldata.end())
        {
          *results = std::string("");
          return results;
        }

      *results = ldata[key];
    }

  if (funcname == "setVariable")
    {
      if (params.size() < 2)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setVariable\"): wrong params"
            << std::endl;
          return results;
        }

      std::string key;
      std::string value;

      try
        {
          key = params.at(0)->get<std::string>();
          value = params.at(1)->get<std::string>();
        }

      catch(...)
        {	 
          std::cout
            << "Actor::callFunction(funcname=\"setVariable\"): wrong params"
            << std::endl;
          return results;
        }

      if (!key.size())
        {
          std::cout
            << "Actor::callFunction(funcname=\"setVariable\"): wrong params"
            << std::endl;
          return results;
        }

      ldata[key] = value;
    }

  if (funcname == "applyImpulse")
    {
      ActorPair impulse;
      ActorPair offset = makePair(0,0);

      if (params.size() < 2)
        {
          std::cout
            << "Actor::callFunction(funcname=\"applyImpulse\"):"
            << " wrong params" << std::endl;
          return results;
        }

      try
        {
          impulse.x = (params.at(0)->verifyType<int>() ?
                       params.at(0)->get<int>() :
                       params.at(0)->get<float>());
          impulse.y = (params.at(1)->verifyType<int>() ?
                       params.at(1)->get<int>() :
                       params.at(1)->get<float>());
        }
      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"applyImpulse\"): "
            << "wrong params" << std::endl;
          return results;
        }

      if (params.size() > 3)
        {
          try
            {
              offset.x = (params.at(2)->verifyType<int>() ?
                          params.at(2)->get<int>() :
                          params.at(2)->get<float>());
              offset.y = (params.at(3)->verifyType<int>() ?
                          params.at(3)->get<int>() :
                          params.at(3)->get<float>());
            }
          catch (...)
            {
              std::cout
                << "Actor::callFunction(funcname=\"applyImpulse\"): "
                << "wrong params" << std::endl;
              return results;
            }
        }

      physics_object->applyImpulse(impulse, offset);
    }

  if (funcname == "getCurrentEvent")
    {
      std::vector<Variant*> event;
      event.push_back(new Variant);

      *(event.at(0)) = cur_event.name;

      std::map<std::string, Variant*>::iterator iter;
      for (iter = cur_event.data.begin();
           iter != cur_event.data.end();
           iter++)
        {
          Variant* nv = duplicateVariant(iter->second);

          if (nv)
            {
              event.push_back(new Variant);
              event.push_back(nv);

              *(event.at(event.size() - 2)) = iter->first;
            }
        }

      *results = event;
    }

  if (funcname == "getVelocity")
    {
      std::vector<Variant*> velocity;
      Variant* var;

      var = new Variant;
      *var = physics_object->getVelocity().x;
      velocity.push_back(var);

      var = new Variant;
      *var = physics_object->getVelocity().y;
      velocity.push_back(var);

      *results = velocity;
    }

  if (funcname == "setVelocity")
    {
      ActorPair nvelocity;

      if (params.size() < 2)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setVelocity\"): wrong params"
            << std::endl;
          return results;
        }

      try
        {
          nvelocity.x = (params.at(0)->verifyType<int>() ?
                         params.at(0)->get<int>() :
                         params.at(0)->get<float>());
          nvelocity.y = (params.at(1)->verifyType<int>() ?
                         params.at(1)->get<int>() :
                         params.at(1)->get<float>());
        }
      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setVelocity\"): wrong params"
            << std::endl;
          return results;
        }

      physics_object->setVelocity(nvelocity);
    }

  if (funcname == "getAngularVelocity")
    *results = physics_object->getVelocity();

  if (funcname == "setAngularVelocity")
    {
      float nvelocity;

      if (params.size() < 1)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setAngularVelocity\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      try
        {
          nvelocity = (params.at(0)->verifyType<int>() ?
                       params.at(0)->get<int>() :
                       params.at(0)->get<float>());
        }
      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setAngularVelocity\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      physics_object->setAngularVelocity(nvelocity);
    }

  if (funcname == "removeGraphic")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"removeGraphic\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= sprites.size())
        {
          std::cout
            << "Actor::callFunction(funcname=\"removeGraphic\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      sprites.erase(sprites.begin() + idx);
    }

  if (funcname == "addGraphic")
    {
      if (params.size() < 1 ||
          !params.at(0)->verifyType<std::string>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"addGraphic\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      std::string name = params.at(0)->get<std::string>();
      ActorPair pos = makePair(0,0);
      float angle = 0;

      try
        {
          if (params.size() > 2)
            {
              pos.x = (params.at(1)->verifyType<float>()) ?
                      (params.at(1)->get<float>()) :
                      (params.at(1)->get<int>());
              pos.y = (params.at(2)->verifyType<float>()) ?
                      (params.at(2)->get<float>()) :
                      (params.at(2)->get<int>());
            }
        }

      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"addGraphic\"): wrong params"
            << std::endl;
          return results;
        }

      if (params.size() > 3)
        {
          try
            {
              angle = (params.at(3)->verifyType<float>()) ?
                      (params.at(3)->get<float>()) :
                      (params.at(3)->get<int>());
            }

          catch (...)
            {
              std::cout
                << "Actor::callFunction(funcname=\"addGraphic\")"
                   ": wrong params"
                << std::endl;
              return results;
            }
        }

      std::map<std::string, Variant*> nparams;

      if (params.size() > 5)
        {
          int cur;
          int cap = (params.size() % 2) ? -1 : -2;
          for (cur = 4; cur < params.size()+cap; cur+=2)
            {
              if (!params.at(cur)->verifyType<std::string>())
                {
                  std::cout
                    << "Actor::callFunction(funcname=\"addGraphic\")"
                       ": wrong params"
                    << std::endl;
                  return results;
                }

              nparams[params.at(cur)->get<std::string>()] =
                params.at(cur + 1);
            }
        }

      ActorGraphic ngraphic;
      ngraphic.name = name;
      ngraphic.position.x = pos.x;
      ngraphic.position.y = pos.y;
      ngraphic.angle = angle;

      ngraphic.graphic = initial_params.game->
        getGraphics(ngraphic.name, nparams);

      if (ngraphic.graphic)
        {
          sprites.push_back(ngraphic);
          *results = sprites.size() - 1;
        }

      else
        {
          std::cout
            << "Actor::callFunction(funcname=\"addGraphic\")"
               ": failed to make graphic"
            << std::endl;
          return results;
        }
    }

  if (funcname == "getGraphicIndex")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<std::string>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicIndex\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      std::string name = params.at(0)->get<std::string>();
      int cur;

      for (cur = 0; cur < sprites.size(); cur++)
        if (sprites.at(cur).name == name)
          break;

      if (cur >= sprites.size())
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicIndex\")"
               ": no such sprite"
            << std::endl;
          cur = -1;
        }

      *results = cur;
    }

  if (funcname == "getGraphicListSize")
    *results = sprites.size();

  if (funcname == "getGraphicScriptObject")
    {
      if (params.size() < 1 ||
          !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicScriptObject\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= sprites.size() || idx < 0)
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicScriptObject\")"
               ": invalid index"
            << std::endl;
          return results;
        }

      ScriptedClass* item = sprites.at(idx).graphic->getScriptObject();

      if (item)
        *results = item;
      else
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicScriptObject\")"
               ": no script object supported"
            << std::endl;
          return results;
        }
    }

  if (funcname == "setGraphicPosition")
    {
      if (params.size() < 3 ||
          !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"setGraphicPosition\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();
      ActorPair pos;

      if (idx >= sprites.size() || idx < 0)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setGraphicPosition\")"
               ": bad index"
            << std::endl;
          return results;
        }

      try
        {
          pos.x = (params.at(1)->verifyType<float>()) ?
                  (params.at(1)->get<float>()) :
                  (params.at(1)->get<int>());
          pos.y = (params.at(2)->verifyType<float>()) ?
                  (params.at(2)->get<float>()) :
                  (params.at(2)->get<int>());
        }

      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setGraphicPosition\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      sprites.at(idx).position.x = pos.x;
      sprites.at(idx).position.y = pos.y;
    }

  if (funcname == "getGraphicPosition")
    {
      if (params.size() < 1 ||
          !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicPosition\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= sprites.size() || idx < 0)
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicPosition\")"
               ": bad index"
            << std::endl;
          return results;
        }

      std::vector<Variant*> dresults;

      dresults.push_back(new Variant);
      dresults.push_back(new Variant);

      *(dresults.at(0)) = sprites.at(idx).position.x;
      *(dresults.at(1)) = sprites.at(idx).position.y;

      *results = dresults;
    }

  if (funcname == "setGraphicAngle")
    {
      if (params.size() < 2 ||
          !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"setGraphicAngle\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();
      float angle;

      if (idx >= sprites.size() || idx < 0)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setGraphicAngle\")"
               ": bad index"
            << std::endl;
          return results;
        }

      try
        {
          angle = (params.at(1)->verifyType<float>()) ?
                  (params.at(1)->get<float>()) :
                  (params.at(1)->get<int>());
        }

      catch (...)
        {
          std::cout
            << "Actor::callFunction(funcname=\"setGraphicAngle\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      sprites.at(idx).angle = angle;
    }

  if (funcname == "getGraphicAngle")
    {
      if (params.size() < 1 ||
          !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicAngle\")"
               ": wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= sprites.size() || idx < 0)
        {
          std::cout
            << "Actor::callFunction(funcname=\"getGraphicAngle\")"
               ": bad index"
            << std::endl;
          return results;
        }

      *results = sprites.at(idx).angle;
    }

  return results;
}

ActorData Actor::parseXML(std::string source, MediaLoader& media_loader)
{
  std::string temp = source; // debug

  std::string buffer = media_loader.loadMedia(temp);

  XMLParser::parseactor::actor actor =
    XMLParser::parseactor::parseactorFromBuffer(buffer);

  ActorData results;
  results.media_loader = &media_loader;

  results.name = actor.m_name;

  int cur;
  for (cur = 0; cur < actor.m_graphics.size(); cur++)
    {
      ActorGraphics graphics;
      XMLParser::parseactor::graphics cur_graphics = actor.m_graphics.at(cur);

      graphics.sauce = cur_graphics.m_source;
      graphics.pos = parsePair(cur_graphics.m_pos.m_pair);
      graphics.angle = cur_graphics.m_angle; // fix'd

      int tcur;
      for (tcur = 0; tcur < cur_graphics.m_params.size(); tcur++)
        {
          std::string param_name = cur_graphics.m_params.at(tcur).name;
          std::string param_value = cur_graphics.m_params.at(tcur).value;
          std::string param_type = cur_graphics.m_params.at(tcur).type;

          graphics.params[param_name] = Pair<std::string>();

          graphics.params[param_name].x = param_type;
          graphics.params[param_name].y = param_value;
        }

      results.graphics.push_back(graphics);
    }

  results.physics.mass = actor.m_physics.m_body.m_mass;
  results.physics.inertia = actor.m_physics.m_body.m_inertia;
  results.physics.fixed = actor.m_physics.m_body.m_fixed;
  results.physics.rotational_inertia =
    actor.m_physics.m_body.m_rotational_inertia;
  results.physics.generate_rotational_inertia =
    actor.m_physics.m_body.generate_rotational_inertia;

  for (cur = 0; cur < actor.m_physics.m_poly.size(); cur++)
    {
      XMLParser::parseactor::poly cur_poly = actor.m_physics.m_poly.at(cur);

      PhysicsShape nshape;
      nshape.offset = parsePair(cur_poly.m_offset.m_pair);
      nshape.shape = S_Poly;

      nshape.elasticity = cur_poly.m_elasticity;
      nshape.friction = cur_poly.m_friction;

      int subcur;
      for (subcur = 0; subcur < cur_poly.m_data.m_pair.size(); subcur++)
        {
          std::string cur_item = cur_poly.m_data.m_pair.at(subcur);
          nshape.data.push_back(parsePair(cur_item));
        }

      results.physics.shapes.push_back(nshape);
    }

  for (cur = 0; cur < actor.m_physics.m_circle.size(); cur++)
    {
      XMLParser::parseactor::circle cur_circle =
        actor.m_physics.m_circle.at(cur);

      PhysicsShape nshape;
      nshape.offset = parsePair(cur_circle.m_offset.m_pair);
      nshape.shape = S_Circle;

      nshape.elasticity = cur_circle.m_elasticity;
      nshape.friction = cur_circle.m_friction;

      ActorPair pair;  // bender should grow this
      pair.x = cur_circle.m_radius;

      nshape.data.push_back(pair);

      results.physics.shapes.push_back(nshape);
    }

  for (cur = 0; cur < actor.m_physics.m_line.size(); cur++)
    {
      XMLParser::parseactor::line cur_line =
        actor.m_physics.m_line.at(cur);

      PhysicsShape nshape;
      nshape.shape = S_Line;

      nshape.elasticity = cur_line.m_elasticity;
      nshape.friction = cur_line.m_friction;

      nshape.data.push_back(parsePair(cur_line.m_a));
      nshape.data.push_back(parsePair(cur_line.m_b));

      ActorPair pair;
      pair.x = cur_line.m_radius;

      nshape.data.push_back(pair);

      results.physics.shapes.push_back(nshape);
    }

  for (cur = 0; cur < actor.m_reaction.size(); cur++)
    {
      std::string event = actor.m_reaction.at(cur).m_event;
      std::string script = actor.m_reaction.at(cur).m_script;

      results.scripts[event] = script;
    }

  return results;
}
