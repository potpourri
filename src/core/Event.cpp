// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// simply for the matchEvent() function

#include "../../include/Global.h"
#include "../../include/core/Event.h"
#include "../../include/core/XmlExtra.h"

#include <iostream>
#include <vector>
#include <stdexcept>

using namespace fragrant;

std::map<std::string, MouseButton> fragrant::makeMouseStringMap()
{
  std::map<std::string, MouseButton> results;

  results["MB_LEFT"] = MB_LEFT;
  results["MB_MIDDLE"] = MB_MIDDLE;
  results["MB_RIGHT"] = MB_RIGHT;
  results["MB_WHEEL_UP"] = MB_WHEEL_UP;
  results["MB_WHEEL_DOWN"] = MB_WHEEL_DOWN;

  return results;
}

bool fragrant::matchEvent(std::string to_match, Event type)
{
  std::string name;
  std::map<std::string, Pair<std::string> > data;

  std::vector<std::string> initial_split = splitString(to_match,
    EVENT_DELIM_1);

  if (!initial_split.size()) // lolwut
    throw std::runtime_error("matchEvent(): whoops");

  name = initial_split.at(0);

  if (type.name != name)
    return false;

  if (initial_split.size() == 1)
    return true;

  if (initial_split.size() != 2)
    throw std::runtime_error("matchEvent(): Extra colon");

  std::vector<std::string> secondary_split =
    splitString(initial_split.at(1), EVENT_DELIM_2);

  int cur;
  for (cur = 0; cur < secondary_split.size(); cur++)
    {
      std::string cur_param = secondary_split.at(cur);
      std::vector<std::string> portions =
        splitString(cur_param, EVENT_DELIM_3);

      if (portions.size() != 2)
        throw std::runtime_error("matchEvent(): Wrong something or other");

      std::vector<std::string> pair =
        splitString(portions.at(1), EVENT_DELIM_4);

      if (pair.size() != 2)
        throw std::runtime_error("matchEvent(): rabblerabblerabble");

      Pair<std::string> string_pair;
      string_pair.x = pair.at(0);
      string_pair.y = pair.at(1);

      data[portions.at(0)] = string_pair;
    }

  std::map<std::string, Pair<std::string> >::iterator iter;
  for (iter = data.begin(); iter != data.end(); iter++)
    {
      if (type.data.find(iter->first) == type.data.end())
        return false;

      if (iter->second.x == BOOL)
        {
          bool xdata;
          try
            {
              xdata = type.data[iter->first]->get<bool>();
            }

          catch (...)
            {
              return false;
            }

          bool ydata = fromString<bool>(iter->second.y);

          if (xdata != ydata)
            return false;
        }

      if (iter->second.x == CHAR)
        {
          char xdata;
          try
            {
              xdata = type.data[iter->first]->get<char>();
            }

          catch (...)
            {
              return false;
            }

          char ydata = iter->second.y.at(0);

          if (xdata != ydata)
            return false;
        }

      if (iter->second.x == INT)
        {
          int xdata;
          try
            {
              xdata = type.data[iter->first]->get<int>();
            }

          catch (...)
            {
              return false;
            }

          int ydata = fromString<int>(iter->second.y);

          if (xdata != ydata)
            return false;
        }

      if (iter->second.x == KEY)
        {
          Key xdata;
          try
            {
              xdata = type.data[iter->first]->get<Key>();
            }

          catch (...)
            {
              return false;
            }

          Key ydata = static_cast<Key>(-1);

          int cur;
          for (cur = 0;
               cur < sizeof(KeyStringArray)/sizeof(std::string);
               cur++)
            {
              if (iter->second.y == KeyStringArray[cur])
                ydata = static_cast<Key>(cur);
            }

          if (xdata != ydata)
            return false;
        }

      if (iter->second.x == MOUSEBUTTON)
        {
          MouseButton xdata;
          try
            {
              xdata = type.data[iter->first]->get<MouseButton>();
            }

          catch (...)
            {
              return false;
            }

          MouseButton ydata = static_cast<MouseButton>(-1);

          if (MouseStringMap.find(iter->second.y) != MouseStringMap.end())
            ydata = MouseStringMap.find(iter->second.y)->second;

          if (xdata != ydata)
            return false;
        }
    }

  return true;
}
