// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// sauce

#include "../../include/core/PhysicsSimulation.h"
#include "../../include/core/common.h"
#include <stdexcept>

using namespace fragrant;

int chipmunk_collision_callback(
  cpShape* a, cpShape* b, cpContact* contacts, int numContacts,
  cpFloat normal_coef, void* data);

PhysicsSimulation::PhysicsSimulation()
{
  if (!initted)
    {
      cpInitChipmunk();
      initted = true;
    }

  space = cpSpaceNew();
  if (!space)
    throw std::runtime_error(
      "PhysicsSimulation::PhysicsSimulation(): couldn't make space");

  space->gravity = cpv(GRAVITY.x, GRAVITY.y);
}

PhysicsSimulation::~PhysicsSimulation()
{
  int cur;
  for (cur = 0; cur < objects.size(); cur++)
    removePhysicsObject(objects.at(cur));

  if (space)
    cpSpaceFree(space);
}

void PhysicsSimulation::setInputManager(InputManager* manager)
{
  input_manager = manager;
  cpSpaceSetDefaultCollisionPairFunc(space,
    chipmunk_collision_callback, (void*) this);

  return;
}

InputManager* PhysicsSimulation::getInputManager()
{
  return input_manager;
}

void PhysicsSimulation::process(float max, float increment)
{
  float tcur;

  for (tcur = 0.0; tcur < max; tcur += increment)
    {
      int cur;
      for (cur = 0; cur < objects.size(); cur++)
        cpBodyResetForces(objects.at(cur)->body);

      cpSpaceStep(space, increment);
    }

  return;
}

void PhysicsSimulation::addPhysicsObject(PhysicsObject* object)
{
  objects.push_back(object);

  cpResetShapeIdCounter();

  object->parent = this;
  object->addToSpace(space);
  object->setPosition(object->getPosition());
}

void PhysicsSimulation::removePhysicsObject(PhysicsObject* object)
{
  std::vector<PhysicsObject*>::iterator iter;
  std::vector<std::vector<PhysicsObject*>::iterator> to_del;
  for (iter = objects.begin(); iter != objects.end(); iter++)
    if (*iter == object)
      to_del.push_back(iter);

  int cur;
  for (cur = (to_del.size() - 1); cur >= 0; cur--)
    objects.erase(to_del.at(cur));

  object->parent = 0;
  object->removeFromSpace(space);

  return;
}

long unsigned int hashPointers(Actor* a, Actor* b)
{
  Actor* temp;

  if (a > b)
    {
      temp = a;
      a = b;
      b = a;
    }

  long unsigned int na = (long unsigned int) a;
  long unsigned int nb = (long unsigned int) b;

  na <<= sizeof(na);

  return (na + nb);
}

bool PhysicsSimulation::isNewEvent(Actor* a, Actor* b)
{
  long unsigned int hash = hashPointers(a, b);
  int cur_ticks = getticks();

  if (collision_times.find(hash) == collision_times.end())
    {
      collision_times[hash] = cur_ticks;
      return true;
    }

  int old_ticks = collision_times[hash];
  collision_times[hash] = cur_ticks;
  int diff = cur_ticks - old_ticks;

  if (diff >= DIFF)
    return true;

  return false;
}

bool PhysicsSimulation::initted = false;

int chipmunk_collision_callback(
  cpShape* a, cpShape* b, cpContact* contacts, int numContacts,
  cpFloat normal_coef, void* data)
{
  PhysicsSimulation* sim = static_cast<PhysicsSimulation*>(data);
  InputManager* manager = sim->getInputManager();

  if (!sim->isNewEvent(
      static_cast<Actor*>(a->data), static_cast<Actor*>(b->data)))
    return 1;

  Event nevent;
  nevent.name = COLLISION_EVENT_NAME;

  nevent.data["object_a"] = new Variant;
  *(nevent.data["object_a"]) = static_cast<Actor*>(a->data)->getIndex();

  nevent.data["object_b"] = new Variant;
  *(nevent.data["object_b"]) = static_cast<Actor*>(b->data)->getIndex();

  manager->appendEvent(nevent);

  return 1;
}
