// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The implementation for Variant

#include "../../include/core/Variant.h"

using namespace fragrant;

void Generic::null()
{
  // nothing
}

Generic::~Generic()
{
  // nothing
}

Variant::Variant()
{
  data = 0;
}

Variant::Variant(Variant const &rhs)
{
  if (&rhs != this)
    {
      Specific<Variant*>* pointer = new Specific<Variant*>();
      *pointer = const_cast<Variant*>(&rhs);
      this->data = static_cast<Generic*>(pointer);
    }
}

Variant::~Variant()
{
  delete data;
}

Variant& Variant::operator=(Variant const &rhs)
{
  if (&rhs != this)
    {
      Specific<Generic*>* pointer = new Specific<Generic*>();
      *pointer = const_cast<Generic*>(rhs.data);
      this->data = static_cast<Generic*>(pointer);
    }

  return *this;
}

Variant* fragrant::duplicateVariant(Variant* nv)
{
  if (!nv)
    return 0;

  Variant* results = new Variant;

  if (nv->verifyType<std::string>())
    {
      *results = nv->get<std::string>();
      return results;
    }

  if (nv->verifyType<bool>())
    {
      *results = nv->get<bool>();
      return results;
    }

  if (nv->verifyType<int>())
    {
      *results = nv->get<int>();
      return results;
    }
  if (nv->verifyType<float>())
    {
      *results = nv->get<float>();
      return results;
    }
  if (nv->verifyType<char>())
    {
      *results = std::string("") + nv->get<char>();
      return results;
    }

  delete results;
  return 0;
}
