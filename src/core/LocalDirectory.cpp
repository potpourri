// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The sauce

#include "../../include/core/LocalDirectory.h"
#include "../../include/core/common.h"
#include "../../include/core/Magic.h"

#include <fstream>
#include <iostream>
#include <stdexcept>

#include <cstring>

using namespace fragrant;

LocalDirectory::LocalDirectory(std::string base,
  std::map<std::string, archive_function> funcs)
{
  this->base = base;
  this->functions = funcs;
}

LocalDirectory::~LocalDirectory()
{}

std::string LocalDirectory::getFile(std::string filename)
{
  std::string temp = filename; // debug
  std::string tpath = (base + path_separator + filename);
  std::vector<std::string> portions =
    splitPath(tpath);

  if (splitPath(simplifyPath(joinPath(splitPath(filename)))).size() == 1 ||
      base == "")
    return fetchData(filename);

  Directory* last_directory = getDirectory(
    std::vector<std::string>(portions.begin(), portions.end() - 1));

  std::string data = last_directory->getFile(
    portions.at(portions.size() - 1));

  delete last_directory;

  return data;
}

std::vector<std::string> LocalDirectory::getListing(std::string folder)
{
  throw std::runtime_error(
    "LocalDirectory::getListing(): Not implemented yet");
}

Directory* LocalDirectory::getDirectory(std::vector<std::string> path)
{
  if (!path.size())
    {
      return new LocalDirectory(base, functions);
    }

  std::string base_plus = base + path_separator + path.at(0);
  std::string data;

  try
    {
      data = fetchData(path.at(0));
    }

  catch (std::runtime_error& e){}

  Magic mime_magic = Magic(true);
  Magic usual_magic = Magic(false);
  std::string mime_type = mime_magic.data(data);
  bool dir;
  try
    {
      dir = usual_magic.file(base_plus) == directory;
    }
  catch (std::runtime_error& e){}

  Directory* result = 0;

  std::vector<std::string> rest =
    std::vector<std::string>(path.begin() + 1, path.end());

  if (dir)
    {
      Directory* temp = new
        LocalDirectory(base_plus, functions);

      result = temp->getDirectory(rest);
      delete temp;
    }

  else if (functions.find(mime_type) != functions.end())
    {
      archive_function function = functions[mime_type];
      Archive* arc = function(data);
      arc->setArchiveFuncs(functions);
      result = arc->getDirectory(rest);
      delete arc;
    }

  else
    {
      throw std::runtime_error(
        "LocalDirectory::getDirectory(): filetype not supported, \"" +
        mime_type + "\"");
    }

  return result;
}

std::string LocalDirectory::fetchData(std::string path)
{
  std::fstream the_file;
  the_file.open((base + path_separator + path).c_str());

  if (!the_file.is_open())
    throw std::runtime_error(
      "LocalDirectory::fetchData(): Error opening file, \"" + path + "\"");

  the_file.seekg(0, std::ios_base::end);
  int size = the_file.tellg();
  the_file.seekg(0);

  char* contents = new char[size];
  memset(contents, '\0', size);

  the_file.read(contents, size);

  std::string the_contents = std::string(contents, size);
  delete [] contents;

  return the_contents;
}
