// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.

#include "../../include/core/XmlExtra.h"

#include <libxml/tree.h>
#include <libxml/parser.h>

#include <cstring>
#include <stdexcept>

std::string getContents(xmlpp::Node* node)
{
  std::string results;

  if (!node->get_children().size())
    return results;

  xmlpp::Node* nnode = *(node->get_children().begin());
  xmlpp::TextNode* text_node = 0;

  while (nnode)
    {
      text_node = dynamic_cast<xmlpp::TextNode*>(nnode);
      nnode = nnode->get_next_sibling();

      if (text_node)
        results += text_node->get_content();
    }

  return results;
}

Pair<float> parsePair(std::string input)
{
  Pair<float> results;
  std::vector<std::string> parts = splitString(input, pair_delim);

  if (parts.size() < 2)
    throw std::runtime_error("parsePair(): Not enough parts");

  results.x = fromString<float>(parts.at(0));
  results.y = fromString<float>(parts.at(1));

  return results;
}

std::string callback_dtd = "";

int match_callback(char const* data)
{
  if (std::string(data) == match_string)
    return 1;

  return 0;
}

void* open_callback(char const* filename)
{
  if (std::string(filename) == match_string)
    return new std::string(callback_dtd);

  return 0;
}

int read_callback(void*str, char* buffer, int len)
{
  std::string dstr = *(std::string*)str;
  strncpy(buffer, dstr.c_str(), len);

  return len;
}

int close_callback(void* context)
{
  delete (std::string*)context;
  return 0;
}

bool validXML(std::string dtd, std::string name, xmlpp::DomParser* parser)
{
  bool results = false;
  xmlRegisterInputCallbacks(match_callback, open_callback,
    read_callback, close_callback);

  callback_dtd = dtd;

  xmlDtdPtr dtd_ptr = xmlParseDTD((xmlChar*) name.c_str(),
    (xmlChar*)match_string.c_str());

  xmlValidCtxtPtr valid_ctxt = xmlNewValidCtxt();
  results = xmlValidateDtd(valid_ctxt,
    parser->get_document()->cobj(), dtd_ptr);

  xmlFreeValidCtxt(valid_ctxt);
  xmlFreeDtd(dtd_ptr);

  return results;
}
