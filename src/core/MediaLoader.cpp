// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The source for the medialoader class.

#include <iostream>
#include <string>
#include <stdexcept>

#include "../../include/core/MediaLoader.h"

#include "../../include/core/common.h"
#include "../../include/core/cURLDirectory.h"
#include "../../include/core/LocalDirectory.h"

using namespace fragrant;

MediaLoader::MediaLoader()
{
}

MediaLoader::~MediaLoader()
{}

std::string MediaLoader::loadMedia(std::string source)
{
  bool relative;
  bool local;

  std::string base;

  try
    {
      ParsedPath path = parsePath(source);
      base = path.protocol + protocol_host_separator +
        path.host + path_separator;
    }

  catch (std::runtime_error& e)
    {
      local = true;

      relative = splitPath(source).size() &&
        splitPath(source).at(0) != (std::string("") + path_separator);

      if (!relative)
        base = path_separator;
      else
        base = current_dir;
    }

  std::string results;

  if (local)
    {
      LocalDirectory dir = LocalDirectory(base, plugin_map);
      results = dir.getFile(source);
    }

  else
    {
      cURLDirectory dir = cURLDirectory(base, plugin_map);
      results = dir.getFile(source);
    }

  return results;
}

std::map<std::string, archive_function>& MediaLoader::plugins()
{
  return plugin_map;
}

Game* MediaLoader::getGame()
{
  return game;
}

void MediaLoader::setGame(Game* ngame)
{
  game = ngame;
  return;
}
