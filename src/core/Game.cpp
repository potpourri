// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// The source for the Game class

#include <iostream>
#include <stdexcept>

#include "../../include/core/Game.h"
#include "../../include/core/common.h"
#include "../../include/core/parsegame.h"

using namespace fragrant;

void GameData::printData()
{
  std::cout << "GameData::printData(): Parsed game xml file:" << std::endl
            << "\tName: " << name << std::endl
            << "\tVersion: " << version << std::endl
            << "\tIcon: " << icon << std::endl;
  int cur;
  for (cur = 0; cur < authors.size(); cur++)
    std::cout << "\tAuthor: " << authors.at(cur) << std::endl;

  for (cur = 0; cur < presentations.size(); cur++)
    {
      GamePresentation cur_p = presentations.at(cur);

      std::cout << "\tPresentation: " << cur_p.name
                << "(" << cur_p.size.x << ", " << cur_p.size.y << ") "
                << (cur_p.net ? "Net" : "") << std::endl;
    }

  std::cout << "\tGfx Plugin:\t" << graphics_plugin << std::endl
            << "\tAudio Plugin:\t" << audio_plugin << std::endl
            << "\tInput Plugin:\t" << input_plugin << std::endl
            << "\tScript Plugin:\t" << scripting_plugin << std::endl;

  for (cur = 0; cur < extra_plugins.size(); cur++)
    std::cout << "\tOther Plugin:\t" << extra_plugins.at(cur) << std::endl;

  for (cur = 0; cur < media.size(); cur++)
    std::cout << "\tMedia: " << media.at(cur).name << " ("
              << media.at(cur).contents << ")"
              << (media.at(cur).plugin_name.size() ? ", handled by " +
                  media.at(cur).plugin_name : "") << std::endl;

  return;
}

Game::Game(GameData gdata, std::string dpresentation,
  MediaLoader& nmedia_loader, PluginLoader& nplugin_loader) :
  media_loader(nmedia_loader),
  plugin_loader(nplugin_loader)
{
  gdata.printData();

  current_game = this;
  media_loader.setGame(this);

  std::string root_path = gdata.base;

  presentation = dpresentation;
  bool no_presentation = true;

  int cur;
  for (cur = 0; cur < gdata.presentations.size(); cur++)
    if (gdata.presentations.at(cur).name == presentation)
      no_presentation = false;

  if (no_presentation || !presentation.size())
    {
      presentation = gdata.presentations.at(0).name;
      std::cout << "Game::Game(): "
                <<  (!dpresentation.size() ?
                    "No presentation specified" :
                    "no such presentation, \"" + dpresentation + "\"")
                << std::endl;
    }

  data = gdata;
  level = 0;

  for (cur = 0; cur < gdata.levels.size(); cur++)
    {
      LevelData tdata;
      try
        {
          tdata = Level::parseXML(root_path + path_separator +
            gdata.levels.at(cur), media_loader);

          bool works = true;

          try
            {
              tdata.getSlice(presentation);
            }

          catch (...)
            {
              works = false;
            }

          if (works)
            level_defs[tdata.name] = tdata;
          std::cout << "Game::Game(): parsed level xml file: "
                    << gdata.levels.at(cur) << std::endl
                    << tdata.toString() << std::endl;
        }

      catch (std::runtime_error& e)
        {
          std::cerr << "Game::Game(): Error parsing level XML file: "
                    << gdata.levels.at(cur) << "\n\t" << e.what()
                    << std::endl;
          tdata.getSlice(presentation).startup = false;
        }

      if (tdata.getSlice(presentation).startup)
        {
          if (level) // bad, someone ignored my warning
            delete level;

          try
            {
              level = new Level(tdata.getSlice(presentation), *this);
            }

          catch (std::runtime_error& e)
            {
              std::cerr << "Game::Game(): failed to make level" << std::endl;
              delete level;
              level = 0;
            }
        }
    }

  for (cur = 0; cur < gdata.actors.size(); cur++)
    {
      try
        {
          ActorData data = Actor::parseXML(root_path + path_separator +
            gdata.actors.at(cur), media_loader);
          actor_defs.push_back(data);
          std::cout << "Game::Game(): parsed actor xml file: "
                    << gdata.actors.at(cur) << std::endl;
        }

      catch (std::runtime_error& e)
        {
          std::cerr << "Game::Game(): Error parsing actor xml file: "
                    << gdata.actors.at(cur) << "\n\t" << e.what()
                    << std::endl;
        }
    }

  display = 0;

  if (plugin_loader.getPlugin(gdata.input_plugin))
    input_manager = plugin_loader.getPlugin(gdata.input_plugin)->
      getPayload()->get<InputManager*>();
  else
    throw std::runtime_error(
      "Game::Game(): couldn't load input plugin: " +
      gdata.input_plugin);

  if (plugin_loader.getPlugin(gdata.graphics_plugin))
    {
      graphics = plugin_loader.getPlugin(gdata.graphics_plugin)->
        getPayload()->get<Graphics>();

      display = graphics.makeDisplay();
    }

  else
    {
      throw std::runtime_error(
        "Game::Game(): couldn't load graphics plugin: " +
        gdata.graphics_plugin);
    }

  if (plugin_loader.getPlugin(gdata.audio_plugin))
    {
      audio = plugin_loader.getPlugin(gdata.audio_plugin)->
        getPayload()->get<Audio>();
      audio_device = audio.device_function();
    }

  else
    {
      throw std::runtime_error(
        "Game::Game(): couldn't load audio plugin: " +
        gdata.audio_plugin);
    }

  if (plugin_loader.getPlugin(gdata.scripting_plugin))
    script_vm = plugin_loader.getPlugin(gdata.scripting_plugin)->
      getPayload()->get<ScriptVM*>();

  else
    throw std::runtime_error(
      "Game::Game(): couldn't load scripting plugin: " +
      gdata.scripting_plugin);

  std::vector<ActorData> ndata;
  for (cur = 0; cur < actor_defs.size(); cur++)
    {
      ActorData data = actor_defs.at(cur);

      data.audio = audio;
      data.script_vm = script_vm;
      data.input_manager = input_manager;
      data.game = this;

      ndata.push_back(data);
    }

  actor_defs = ndata;

  for (cur = 0; cur < gdata.media.size(); cur++)
    {
      GameMedia cur_media = gdata.media.at(cur);

      Plugin* the_plugin = plugin_loader.getPlugin(cur_media.plugin_name);

      if (!the_plugin)
        {
          std::cerr << "Game::Game(): When loading media, plugin was "
                    << "missing, \"" << cur_media.plugin_name << "\""
                    << std::endl;
          continue;
        }

      if (!isURL(cur_media.contents) && isRelative(cur_media.contents))
        cur_media.contents = simplifyPath(gdata.base + path_separator +
          cur_media.contents);

      if (the_plugin->getData().type == PT_Audio)
        {
          AudioSample* result = audio.sample_function(
            cur_media.contents, media_loader);

          if (result)
            audio_samples[cur_media.name] = result;
          else
            std::cerr << "Game::Game(): Error loading audio sample"
                      << std::endl;

          continue;
        }

      if (the_plugin->getData().type == PT_Graphics)
        continue;

      std::cerr << "Game::Game(): requested plugin is not audio nor "
                << "graphics, \"" << cur_media.plugin_name << "\""
                << std::endl;
    }
}

Game::~Game()
{
  delete level;

  current_game = 0;

  std::map<std::string, Drawable*>::iterator draw_iter;
  for (draw_iter = graphic_samples.begin(); draw_iter !=
    graphic_samples.end(); draw_iter++)
    draw_iter->second->destroy();

  display->close();
  input_manager->close();
  script_vm->closeVM();
  audio_device->close();

  std::map<std::string, AudioSample*>::iterator a_iter;
 
  for (a_iter = audio_samples.begin(); a_iter != audio_samples.end(); a_iter++)
    a_iter->second->destroy();
}

void Game::init(GameInit params)
{
  display->init(params.size, params.bpp);
  audio_device->init(params.frequency, params.channels, params.chunksize);
  script_vm->initVM();
  script_vm->wrapFunction("getCurrentGame", &getCurrentGame);
  script_vm->wrapFunction("getScriptCaller", &getScriptCaller);

  return;
}

void Game::run()
{
  EventPipeline pipeline;
  pipeline.getTargetVector().push_back(
    dynamic_cast<EventReceiver*>(this));
  pipeline.getAlias() = data.controls;
  input_manager->init(&pipeline);

  GameInit params;
  GamePresentation cur_presentation;

  if (presentation.size())
    {
      int cur;
      for (cur = 0; cur < data.presentations.size(); cur++)
        if (data.presentations.at(cur).name == presentation)
          cur_presentation = data.presentations.at(cur);
    }

  else
    cur_presentation = data.presentations.at(0);

  params.bpp = 32;
  params.size = cur_presentation.size;
  params.frequency = 44100;
  params.channels = 3;
  params.chunksize = 1024;

  init(params);

  if (!level)  // oh, somethings fucked up
    {
      std::cerr << "Game::run(): No startup level" << std::endl;
      return;
    }

  level->init(actor_defs, &pipeline, audio, script_vm);

  while (level)
    {
      level->cycle(display);
      input_manager->pump();
      display->flip();
    }

  return;
}

void Game::raiseEvent(Event event)
{
  if (event.name == EventStringArray[ET_SwitchLevel])
    {
      if (!event.data["dest"]->verifyType<int>() &&
          !event.data["dest"]->verifyType<std::string>())
        std::cerr << "Game::raiseEvent(): ET_SwitchLevel event param, "
                  << "``dest'' is proper type" << std::endl;
      else
        {
          if (event.data["dest"]->verifyType<int>())
            {
              int value = event.data["dest"]->get<int>();
              if (value)
                std::cerr << "Game::raiseEvent(): wrong int value for "
                          << "ET_SwitchLevel event" << std::endl;
              else
                {
                  delete level;
                  level = 0;
                }
            }

          else
            {
              std::string nlevel = event.data["dest"]->get<std::string>();
              if (level_defs.find(nlevel) == level_defs.end())
                std::cerr << "Game::raiseEvent(): no such level: "
                          << nlevel << std::endl;
              else
                {
                  Level* new_level = new Level(
                    level_defs[nlevel].getSlice(presentation), *level, *this);
                  delete level;
                  level = new_level;
                }
            }
        }
    }

  if (event.name == EventStringArray[ET_Quit])
    {
      std::cout << "Event::raiseEvent(): received ET_Quit event" << std::endl;

      delete level;
      level = 0;
    }

  return;
}

Drawable* Game::getGraphics(std::string name,
  std::map<std::string, Variant*> params = std::map<std::string, Variant*>())
{
  int cur;
  for (cur = 0; cur < data.media.size(); cur++)
    if (data.media.at(cur).name == name)
      {
        GameMedia cur_media = data.media.at(cur);

        Plugin* the_plugin = plugin_loader.getPlugin(cur_media.plugin_name);

        if (!the_plugin)
          throw std::runtime_error(
            ("Game::getGraphics(): error loading graphic, \"" +
             name + "\""));

        if (!isURL(cur_media.contents) && isRelative(cur_media.contents))
          cur_media.contents = simplifyPath(data.base + path_separator +
            cur_media.contents);

        if (the_plugin->getData().type == PT_Graphics)
          {
            Graphics ngraphics = the_plugin->getPayload()->get<Graphics>();

            Drawable* result = ngraphics.makeImage(
              media_loader, cur_media.contents, params);

            if (result)
              {
                while (graphic_samples.find(cur_media.name) !=
                       graphic_samples.end())
                  cur_media.name += "_";

                graphic_samples[cur_media.name] = result;
                return result;
              }

            else
             throw std::runtime_error(
               ("Game::getGraphics(): error loading graphic, \"" +
                name + "\""));
          }

        throw std::runtime_error(
          ("Game::getGraphics(): error loading graphic, \"" +
           name + "\""));
      }
}

AudioSample* Game::getAudio(std::string name)
{
  if (audio_samples.find(name) == audio_samples.end())
    throw std::runtime_error(
      ("Game::getAudio(): no such audio sample, \"" + name + "\"").c_str());

  return audio_samples[name];
}

std::string Game::getFile(std::string name)
{
  if (!isURL(name) && isRelative(name))
    name = simplifyPath(data.base + path_separator + name);

  return media_loader.loadMedia(name);
}

func Game::getConstructor()
{
  return 0;  // nope
}

void Game::destroy()
{
  return; // again, not going to do it
}

std::string Game::getClassName()
{
  return "Game";
}

std::vector<std::string> Game::getClassFunctions()
{
  std::vector<std::string> results;

  results.push_back("getName");
  results.push_back("getVersion");
  results.push_back("getAuthors");
  results.push_back("getRootPath");

  results.push_back("getCurrentLevel");

  return results;
}

Variant* Game::callFunction(std::string funcname,
  std::vector<Variant*> params, ScriptVM* script_vm)
{
  Variant* return_value = new Variant;

  if (funcname == "getName")
    {
      *return_value = data.name;
    }

  if (funcname == "getVersion")
    {
      *return_value = data.version;
    }

  if (funcname == "getAuthors")
    {
      std::vector<Variant*> authors;

      int cur;
      for (cur = 0; cur < data.authors.size(); cur++)
        {
          Variant* temp = new Variant;
          *temp = data.authors.at(cur);
          authors.push_back(temp);
        }

      *return_value = authors;
    }

  if (funcname == "getRootPath")
    *return_value = data.base;

  if (funcname == "getCurrentLevel")
    *return_value = dynamic_cast<ScriptedClass*>(level);

  return return_value;
}

Variant* Game::getCurrentGame(std::vector<Variant*> params)
{
  Variant* result = new Variant;
  *result = dynamic_cast<ScriptedClass*>(current_game);

  return result;
}

GameData Game::parseXML(std::string path, MediaLoader& media_loader)
{
  std::string game_file_path = path + path_separator + GAMEFILE;

  std::string buffer = media_loader.loadMedia(game_file_path);

  XMLParser::parsegame::game xml_game =
    XMLParser::parsegame::parsegameFromBuffer(buffer);

  // put into more convenient struct defined in Game.h

  GameData results;

  results.name = xml_game.m_meta.m_title;
  results.version = xml_game.m_meta.m_version;
  results.authors = xml_game.m_meta.m_author;
  results.icon = xml_game.m_meta.m_icon;
  results.base = path;

  int cur;
  for (cur = 0; cur < xml_game.m_presentation.size(); cur++)
    {
      XMLParser::parsegame::presentation cur_presentation =
        xml_game.m_presentation.at(cur);

      GamePresentation temp_presentation;

      temp_presentation.name = cur_presentation.m_name;
      temp_presentation.net = cur_presentation.m_net;

      temp_presentation.size.x = cur_presentation.m_graphics.m_width;
      temp_presentation.size.y = cur_presentation.m_graphics.m_height;

      results.presentations.push_back(temp_presentation);
    }

  for (cur = 0; cur < xml_game.m_controls.m_event.size(); cur++)
    {
      XMLParser::parsegame::event event = xml_game.m_controls.m_event.at(cur);
      results.controls[event.m_name] = event.m_default;
    }

  results.graphics_plugin = xml_game.m_plugins.m_graphics;
  results.input_plugin = xml_game.m_plugins.m_input;
  results.audio_plugin = xml_game.m_plugins.m_audio;
  results.scripting_plugin = xml_game.m_plugins.m_script;
  results.extra_plugins = xml_game.m_plugins.m_extra;

  for (cur = 0; cur < xml_game.m_media.m_graphics.size(); cur++)
    {
      XMLParser::parsegame::dgraphics x_graphics =
        xml_game.m_media.m_graphics.at(cur);
      GameMedia media;

      media.name = x_graphics.m_name;
      media.plugin_name = x_graphics.m_plugin;
      media.contents = x_graphics.m_filename;

      results.media.push_back(media);
    }

  for (cur = 0; cur < xml_game.m_media.m_audio.size(); cur++)
    {
      XMLParser::parsegame::audio x_audio =
        xml_game.m_media.m_audio.at(cur);
      GameMedia media;

      media.name = x_audio.m_name;
      media.contents = x_audio.m_filename;

      results.media.push_back(media);
    }

  results.actors = xml_game.m_actor;
  results.levels = xml_game.m_level;

  return results;
}

InputManager* Game::getInputManager()
{
  return input_manager;
}

Game* Game::current_game = 0;
