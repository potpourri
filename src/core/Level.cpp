// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The source for the level class.

#include <iostream>
#include <string>
#include <stdexcept>
#include <algorithm>

#include "../../include/core/Level.h"
#include "../../include/core/parselevel.h"
#include "../../include/core/common.h"

using namespace fragrant;

PhysicsObject* makeDrumstick(PhysicsSimulation* sim);
PhysicsObject* makeFloor(PhysicsSimulation* sim);

bool isSimulationWonky(PhysicsSimulation* simulation)
{
  PhysicsObject* floor = makeFloor(simulation);
  PhysicsObject* drumstick = makeDrumstick(simulation);

  int d = 100;

  float original_angle = drumstick->getAngle();

  while (--d)
    {
      simulation->process(20, 0.1);
    }

  float drumstick_angle = drumstick->getAngle();

  delete floor;
  delete drumstick;

  return (drumstick_angle > (original_angle - 5)) &&
         (drumstick_angle < (original_angle + 5));
}

LevelSlice LevelData::getSlice(std::string presentation)
{
  LevelSlice results;

  if (!presentation.size() ||
      (presentations.size() == 1 && !presentations.at(0).size()))
    results = slices[presentations.at(0)];

  else
    {
      if (slices.find(presentation) == slices.end())
        throw std::runtime_error(
          ("LevelData::getSlice(): Level does not support "
           "presentation, \"" + presentation + "\"").c_str());
      results = slices[presentation];
    }

  results.presentation = presentation;
  return results;
}

std::string LevelData::toString()
{
  std::string results;

  results += "\tName: " + name + "\n";
  results += "\tPresentations: ";

  int cur;
  for (cur = 0; cur < presentations.size(); cur++)
    if (presentations.at(cur).size())
      results += "\"" + presentations.at(cur) +
        std::string((cur < presentations.size() - 1) ? ("\", ") : ("\""));

  if (presentations.size() == 1)
    results += "All";

  results += "\n";

  std::map<std::string, LevelSlice>::iterator iter;
  for (iter = slices.begin(); iter != slices.end(); iter++)
    {
      results += std::string("\tLevelSlice for \"") + iter->first +
        std::string("\"\n");
      results += std::string("\t* Startup: ") +
        std::string(iter->second.startup ? "True" : "False") +
        "\n";
      results += "\t* Rest of data truncated...\n";
    }

  return results;
}

Level::Level(LevelSlice data, Game& dgame)
  : level_data(data), presentation(data.presentation), game(dgame)
{
  simulation = new PhysicsSimulation;

//  while (isSimulationWonky(simulation))
//    {
//      std::cout << "Simulation failed to make properly, recreating..."
//                << std::endl;
//      delete simulation;
//      simulation = new PhysicsSimulation;
//    }
}

Level::Level(LevelSlice data, Level& old_level, Game& dgame) : game(dgame)
{
  throw std::runtime_error(std::string("Level::Level(LevelData, Level&): ")
                           + std::string("Not implemented yet"));
}

Level::~Level()
{
  int cur;
  for (cur = 0; cur < actors.size(); cur++)
    {
      delete actors.at(cur).actor;

      std::vector<EventReceiver*>& target_vector =
        pipeline->getTargetVector();
      target_vector.erase(find(target_vector.begin(),
        target_vector.end(), actors.at(cur).actor));
    }

  delete simulation;

  actors.clear();
  static_graphics.clear();
}

void clearStringToVariantMap(std::map<std::string, Variant*> params);
std::map<std::string, Variant*>
  stringToVariantMap(std::map<std::string, Pair<std::string> > params);

std::map<std::string, Variant*>
  stringToVariantMap(std::vector<LevelGraphicParam> params)
{
  std::map<std::string, Pair<std::string> > temp;

  int cur;
  for (cur = 0; cur < params.size(); cur++)
    {
      std::string name = params.at(cur).name;
      temp[name] = Pair<std::string>();

      temp[name].x = params.at(cur).type;
      temp[name].y = params.at(cur).value;
    }

  return stringToVariantMap(temp);
}

void Level::init(std::vector<ActorData> nactor_defs,
  EventPipeline* npipeline, Audio& naudio, ScriptVM* nscript_vm)
{
  this->actor_defs = nactor_defs;
  this->pipeline = npipeline;
  this->audio = naudio;
  this->script_vm = nscript_vm;
  simulation->setInputManager(game.getInputManager());

  viewport = level_data.viewport;

  int cur;
  std::vector<LevelActor> actor_pos = level_data.actors;
  for (cur = 0; cur < actor_pos.size(); cur++)
    {
      int ncur;
      LevelActor temp_level_actor = actor_pos.at(cur);
      for (ncur = 0; ncur < actor_defs.size(); ncur++)
        if (actor_defs.at(ncur).name == temp_level_actor.source)
          {
            ActorData cur_data = actor_defs.at(ncur);
            cur_data.position = temp_level_actor.position;
            cur_data.simulation = simulation;
            cur_data.level = this;

            LActor ngraphic;
            ngraphic.zindex = temp_level_actor.zindex;
            ngraphic.actor = new Actor(cur_data);
            ngraphic.actor->setAngle(temp_level_actor.angle);
            npipeline->getTargetVector().push_back(
              dynamic_cast<EventReceiver*>(ngraphic.actor));

            actors.push_back(ngraphic);
          }
    }

  std::vector<LevelGraphic> level_graphics =
    level_data.static_graphics;

  for (cur = 0; cur < level_graphics.size(); cur++)
    {
      std::map<std::string, Variant*> params =
        stringToVariantMap(level_graphics.at(cur).params);
      Drawable* drawable = game.getGraphics(
        level_graphics.at(cur).source, params);
      clearStringToVariantMap(params);

      StaticGraphic graphic;
      graphic.name = level_graphics.at(cur).source;
      graphic.position = level_graphics.at(cur).position;
      graphic.drawable = drawable;
      graphic.zindex = level_graphics.at(cur).zindex;
      graphic.angle = level_graphics.at(cur).angle;

      static_graphics.push_back(graphic);
    }

  return;
}

struct drawable_item  // throwaway struct for drawing
{
  LActor actor;
  StaticGraphic graphic;
  float depth;

  bool is_actor;
};

void Level::cycle(Display* dest)
{
  // do physics stuff

  int cur = getticks();
  simulation->process(PROCESS_TIME, PROCESS_PIECE);
  int tickcount = PROCESS_TIME - (getticks() - cur);
  dosleep(tickcount > 0 ? tickcount : 0);

  // do drawing stuff

  std::multimap<float, drawable_item> drawable_items;

  for (cur = 0; cur < static_graphics.size(); cur++)
    {
      drawable_item item;
      item.graphic = static_graphics.at(cur);
      item.is_actor = false;
      item.depth = item.graphic.zindex;

      drawable_items.insert(
        std::pair<float, drawable_item>(item.depth, item));
    }

  for (cur = 0; cur < actors.size(); cur++)
    {
      drawable_item item;
      item.actor = actors.at(cur);
      item.is_actor = true;
      item.depth = actors.at(cur).zindex;

      drawable_items.insert(
        std::pair<float, drawable_item>(item.depth, item));
    }

  std::multimap<float, drawable_item>::iterator iter;
  for (iter = drawable_items.begin(); iter != drawable_items.end(); iter++)
    {
      if (iter->second.is_actor)
        iter->second.actor.actor->draw(dest, viewport);

      else
        {
          GraphicsRect src;
          src.xy.x = iter->second.graphic.position.x - viewport.x;
          src.xy.y = iter->second.graphic.position.y - viewport.y;

          iter->second.graphic.drawable->draw(dest, &src,
            static_cast<GraphicsPair*>(0), iter->second.graphic.angle);
        }
    }

  return;
}

int Level::findIndex(Actor* actor)
{
  int cur;
  for (cur = 0; cur < actors.size(); cur++)
    if (actors.at(cur).actor == actor)
      return cur;

  return -1;
}

func Level::getConstructor() { return 0; }
void Level::destroy() { return; }

std::string Level::getClassName() { return "Level"; }

std::vector<std::string> Level::getClassFunctions()
{
  std::vector<std::string> results;

  results.push_back("getViewport");
  results.push_back("setViewport");

  results.push_back("getActorIndicesByName");
  results.push_back("getActor");

  results.push_back("getActorZIndex");
  results.push_back("setActorZIndex");

  results.push_back("deleteActor");
  results.push_back("makeActor");

  results.push_back("removeStaticGraphic");
  results.push_back("addStaticGraphic");

  results.push_back("getStaticGraphicIndex");
  results.push_back("getStaticGraphicListSize");

  results.push_back("setStaticGraphicPosition");
  results.push_back("getStaticGraphicPosition");

  results.push_back("setStaticGraphicAngle");
  results.push_back("getStaticGraphicAngle");

  results.push_back("setStaticGraphicZIndex");
  results.push_back("getStaticGraphicZIndex");

  return results;
}

template<class TYPE>
struct find_actordata : public std::unary_function<TYPE, void>
{
  void operator() (TYPE& x)
    {
      if (x.name == to_find)
        *data = x;
    }

  std::string to_find;
  ActorData* data;
  bool found;
};

Variant* Level::callFunction(std::string funcname,
  std::vector<Variant*> params, ScriptVM* script_vm)
{
  Variant* results = new Variant;

  if (funcname == "getViewport")
    {
      std::vector<Variant*> viewport_data;

      Variant* value = new Variant;
      *value = viewport.x;
      viewport_data.push_back(value);

      value = new Variant;
      *value = viewport.x;
      viewport_data.push_back(value);

      *results = viewport_data;
    }

  if (funcname == "setViewport")
    {
      LevelPair nviewport = viewport;

      try
        {
          nviewport.x = (params.at(0)->verifyType<float>() ?
                         params.at(0)->get<float>() :
                         params.at(0)->get<int>());
          nviewport.y = (params.at(1)->verifyType<float>() ?
                         params.at(1)->get<float>() :
                         params.at(1)->get<int>());
        }

      catch (...)
        {
          std::cout
            << "Level::callFunction(funcname=\"setViewport\"): wrong params"
            << std::endl;
          return results;
        }

      viewport = nviewport;
    }

  if (funcname == "getActorIndicesByName")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<std::string>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getActorIndicesByName\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      std::vector<Variant*> nresults;
      std::string name = params.at(0)->get<std::string>();

      int cur;
      for (cur = 0; cur < actors.size(); cur++)
        if (actors.at(cur).actor->getName() == name)
          {
            nresults.push_back(new Variant);
            *(nresults.at(nresults.size() - 1)) = cur;
          }

      *results = nresults;
    }

  if (funcname == "getActor")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getActor\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx < 0 || idx >= actors.size())
        {
          std::cout
            << "Level::callFunction(funcname=\"getActor\"):"
               " invalid index"
            << std::endl;
          return results;
        }

      *results = dynamic_cast<ScriptedClass*>(actors.at(idx).actor);
    }

  if (funcname == "getActorZIndex")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getActorZIndex\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx < 0 || idx >= actors.size())
        {
          std::cout
            << "Level::callFunction(funcname=\"getActorZIndex\"):"
               " invalid index"
            << std::endl;
          return results;
        }

      *results = actors.at(idx).zindex;
    }

  if (funcname == "setActorZIndex")
    {
      if (params.size() < 2 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"setActorZIndex\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();
      float nzindex;

      try
        {
          nzindex = params.at(1)->verifyType<int>() ?
                    params.at(1)->get<int>():
                    params.at(1)->get<float>();
        }

      catch(...)
        {
          std::cout
            << "Level::callFunction(funcname=\"setActorZIndex\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      if (idx < 0 || idx >= actors.size())
        {
          std::cout
            << "Level::callFunction(funcname=\"setActorZIndex\"):"
               " invalid index"
            << std::endl;
          return results;
        }

      actors.at(idx).zindex = nzindex;
    }

  if (funcname == "deleteActor")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"deleteActor\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx < 0 || idx >= actors.size())
        {
          std::cout
            << "Level::callFunction(funcname=\"deleteActor\"):"
               " invalid index"
            << std::endl;
          return results;
        }

      delete actors.at(idx).actor;
      actors.erase(actors.begin() + idx);
    }

  if (funcname == "makeActor")
    {
      if (params.size() < 2 ||
          !params.at(0)->verifyType<std::string>() ||
          !params.at(1)->verifyType<std::string>())
        {
          std::cout
            << "Level::callFunction(funcname=\"makeActor\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      std::string name = params.at(0)->get<std::string>();
      std::string source = params.at(0)->get<std::string>();

      ActorData data;

      find_actordata<ActorData> search;
      search.data = &data;
      search.to_find = source;
      search.found = false;

      std::for_each(actor_defs.begin(), actor_defs.end(), search);

      if (!search.found)
        {
          std::cout
            << "Level::callFunction(funcname=\"makeActor\"):"
               " failed to find source"
            << std::endl;
          *results = -1;
          return results;
        }

      Actor* nactor = new Actor(data);

      LActor ngraphic;
      ngraphic.actor = nactor;
      ngraphic.zindex = 0.0;

      pipeline->getTargetVector().push_back(
        dynamic_cast<EventReceiver*>(ngraphic.actor));
      actors.push_back(ngraphic);

      *results = actors.size() - 1;
    }

  if (funcname == "removeStaticGraphic")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"removeStaticGraphic\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"removeStaticGraphic\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      static_graphics.erase(static_graphics.begin() + idx);
    }

  if (funcname == "addStaticGraphic")
    {
      if (params.size() < 1 ||
          !params.at(0)->verifyType<std::string>())
        {
          std::cout
            << "Level::callFunction(funcname=\"addStaticGraphic\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      std::string name = params.at(0)->get<std::string>();

      LevelPair pos;
      pos.x = 0;
      pos.y = 0;

      float angle = 0;

      try
        {
          if (params.size() > 2)
            {
              pos.x = (params.at(1)->verifyType<float>()) ?
                      (params.at(1)->get<float>()) :
                      (params.at(1)->get<int>());
              pos.y = (params.at(2)->verifyType<float>()) ?
                      (params.at(2)->get<float>()) :
                      (params.at(2)->get<int>());
            }
        }

      catch (...)
        {
          std::cout
            << "Level::callFunction(funcname=\"addStaticGraphic\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      if (params.size() > 3)
        {
          try
            {
              angle = (params.at(3)->verifyType<float>()) ?
                      (params.at(3)->get<float>()) :
                      (params.at(3)->get<int>());
            }

          catch (...)
            {
              std::cout
                << "Level::callFunction(funcname=\"addStaticGraphic\"):"
                   " wrong params"
                << std::endl;
              return results;
            }
        }

      std::map<std::string, Variant*> nparams;

      if (params.size() > 5)
        {
          int cur;
          int cap = (params.size() % 2) ? -1 : -2;
          for (cur = 4; cur < params.size()+cap; cur+=2)
            {
              if (!params.at(cur)->verifyType<std::string>())
                {
                  std::cout
                    << "Level::callFunction(funcname=\"addStaticGraphic\"):"
                       " wrong params"
                    << std::endl;
                  return results;
                }

              nparams[params.at(cur)->get<std::string>()] =
                params.at(cur + 1);
            }
        }

      StaticGraphic ngraphic;

      ngraphic.position = pos;
      ngraphic.name = name;
      ngraphic.angle = angle;
      ngraphic.zindex = 0.0;
      ngraphic.drawable = game.getGraphics(ngraphic.name, nparams);

      if (ngraphic.drawable)
        {
          static_graphics.push_back(ngraphic);
          *results = static_graphics.size() - 1;
        }

      else
        {
          std::cout
            << "Level::callFunction(funcname=\"addStaticGraphic\"):"
               " wrong params"
            << std::endl;
          return results;
        }
    }

  if (funcname == "getStaticGraphicIndex")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<std::string>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicIndex\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      std::string name = params.at(0)->get<std::string>();
      int cur;

      for (cur = 0; cur < static_graphics.size(); cur++)
        if (static_graphics.at(cur).name == name)
          break;

      if (cur >= static_graphics.size())
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicIndex\"):"
               " no such static graphic"
            << std::endl;
          cur = -1;
        }

      if (!static_graphics.size())
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicIndex\"):"
               " no static graphics"
            << std::endl;
          cur = -1;
        }

      *results = cur;
    }

  if (funcname == "getStaticGraphicListSize")
    *results = static_graphics.size();

  if (funcname == "setStaticGraphicPosition")
    {
      if (params.size() < 3 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicPosition\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicPosition\"):"
               " bad index"
            << std::endl;
          return results;
        }

      LevelPair pos;

      try
        {
          pos.x = (params.at(1)->verifyType<float>()) ?
                  (params.at(1)->get<float>()) :
                  (params.at(1)->get<int>());
          pos.y = (params.at(2)->verifyType<float>()) ?
                  (params.at(2)->get<float>()) :
                  (params.at(2)->get<int>());
        }

      catch (...)
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicPosition\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      static_graphics.at(idx).position = pos;
    }

  if (funcname == "getStaticGraphicPosition")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicPosition\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicPosition\"):"
               " bad index"
            << std::endl;
          return results;
        }

      std::vector<Variant*> nresults;

      nresults.push_back(new Variant);
      nresults.push_back(new Variant);

      *(nresults.at(0)) = static_graphics.at(idx).position.x;
      *(nresults.at(1)) = static_graphics.at(idx).position.y;

      *results = nresults;
    }

  if (funcname == "setStaticGraphicAngle")
    {
      if (params.size() < 2 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicAngle\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();
      float nangle;

      try
        {
          nangle = params.at(1)->verifyType<int>() ?
                   params.at(1)->get<int>() :
                   params.at(1)->get<float>();
        }

      catch (...)
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicAngle\"):"
               " wrong param"
            << std::endl;
          return results;
        }

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicAngle\"):"
               " bad index"
            << std::endl;
          return results;
        }

      static_graphics.at(idx).angle = nangle;
    }

  if (funcname == "getStaticGraphicAngle")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicAngle\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicAngle\"):"
               " bad index"
            << std::endl;
          return results;
        }

      *results = static_graphics.at(idx).angle;
    }

  if (funcname == "setStaticGraphicZIndex")
    {
      if (params.size() < 2 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicZIndex\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();
      float nindex;

      try
        {
          nindex = params.at(1)->verifyType<int>() ?
                   params.at(1)->get<int>() :
                   params.at(1)->get<float>();
        }

      catch (...)
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicZIndex\"):"
               " wrong param"
            << std::endl;
          return results;
        }

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"setStaticGraphicZIndex\"):"
               " bad index"
            << std::endl;
          return results;
        }

      static_graphics.at(idx).zindex = nindex;
    }

  if (funcname == "getStaticGraphicZIndex")
    {
      if (params.size() < 1 || !params.at(0)->verifyType<int>())
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicZIndex\"):"
               " wrong params"
            << std::endl;
          return results;
        }

      int idx = params.at(0)->get<int>();

      if (idx >= static_graphics.size() || idx < 0)
        {
          std::cout
            << "Level::callFunction(funcname=\"getStaticGraphicZIndex\"):"
               " bad index"
            << std::endl;
          return results;
        }

      *results = static_graphics.at(idx).zindex;
    }

  return results;
}

LevelData Level::parseXML(std::string source, MediaLoader& media_loader)
{
  return XMLParser::parselevel::parselevelFromBuffer(
    media_loader.loadMedia(source));

  // lolwut
}

PhysicsObject* makeDrumstick(PhysicsSimulation* sim)
{
  ActorPhysics data;
  data.fixed = 0;
  data.mass = 0.3;
  data.inertia = 0.3;

  fragrant::PhysicsShape round;

  round.shape = fragrant::S_Circle;
  round.elasticity = 0.1;
  round.friction = 0.2;

  round.offset = fragrant::makePair(0.0, 0.0);

  fragrant::ActorPair radius;
  radius.x = 19;
  round.data.push_back(radius);

  fragrant::PhysicsShape stem;

  stem.shape = fragrant::S_Poly;
  stem.elasticity = 0.1;
  stem.friction = 0.2;

  stem.offset = round.offset;

  stem.data.push_back(fragrant::makePair(5.0, -25.0));
  stem.data.push_back(fragrant::makePair(5.0, -55.0));
  stem.data.push_back(fragrant::makePair(-5.0, -55.0));
  stem.data.push_back(fragrant::makePair(-5.0, -25.0));

  PhysicsShape base;
  base.shape = S_Poly;

  base.elasticity = 0.1;
  base.friction = 0.2;

  base.offset = round.offset;

  base.data.push_back(fragrant::makePair(20.0, -55.0));
  base.data.push_back(fragrant::makePair(20.0, -60.0));
  base.data.push_back(fragrant::makePair(-20.0, -60.0));
  base.data.push_back(fragrant::makePair(-20.0, -55.0));

  data.shapes.push_back(base);
  data.shapes.push_back(stem);
  data.shapes.push_back(round);

  fragrant::PhysicsObject* results = new fragrant::PhysicsObject(data);

  sim->addPhysicsObject(results);

  results->setPosition(fragrant::makePair(100, 0));
  results->setAngle(30);

  return results;
}

PhysicsObject* makeFloor(PhysicsSimulation* sim)
{
  ActorPhysics data;
  data.base_position.x = 0;
  data.base_position.y = 0;
  data.fixed = true;

  PhysicsShape floor;

  floor.shape = fragrant::S_Line;
  floor.elasticity = 0.3;
  floor.friction = 0.1;

  ActorPair p1;
  p1.x = -1000;
  p1.y = -300;

  ActorPair p2;
  p2.x = 1000;
  p2.y = -300;

  fragrant::ActorPair p3;
  p3.x = 1;

  floor.data.push_back(p1);
  floor.data.push_back(p2);
  floor.data.push_back(p3);

  floor.offset = makePair(0, 0);

  data.shapes.push_back(floor);

  PhysicsObject* results = new PhysicsObject(data);
  sim->addPhysicsObject(results);

  return results;
}
