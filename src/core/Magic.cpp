// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// My magic sauce

#include "../../include/core/Magic.h"

#include <stdexcept>

using namespace fragrant;

Magic::Magic(bool mime)
{
  cookie = magic_open(mime ? MAGIC_MIME : MAGIC_NONE);
  if (!cookie)
    throw std::runtime_error("Magic::Magic(): Error opening magic cookie");

  magic_load(cookie, 0);
}

Magic::~Magic()
{
  magic_close(cookie);
}

std::string Magic::file(std::string filename)
{
  if (magic_file(cookie, filename.c_str()))
    return magic_file(cookie, filename.c_str());

  throw std::runtime_error("Magic::file(): Error with file:\n\t" + filename);
}

std::string Magic::data(std::string data)
{
  if (magic_buffer(cookie, data.c_str(), data.size()))
    return magic_buffer(cookie, data.c_str(), data.size());

  throw std::runtime_error("Magic::data(): Error with data");
}
