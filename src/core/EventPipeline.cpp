// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The source for the event pipeline

#include "../../include/core/EventPipeline.h"
#include <stdexcept>

using namespace fragrant;

void EventPipeline::raiseEvent(Event newevent)
{
  if (newevent.data.find(NO_RECURSE) == newevent.data.end())
    {
      std::map<std::string, std::string>::iterator alias_iter;
      for (alias_iter = event_alias.begin();
           alias_iter != event_alias.end();
           alias_iter++)
        {
          bool matches;

          try
            {
              matches = matchEvent(alias_iter->second, newevent);
            }

            catch (...){ matches = false; }

          if (matches)
            {
              Event nevent;
              nevent.name = alias_iter->first;
              nevent.data[NO_RECURSE] = 0;

              raiseEvent(nevent);
            }
        }
    }

  std::vector<EventReceiver*>::iterator iter;
  for (iter = targets.begin(); iter != targets.end(); iter++)
    (*iter)->raiseEvent(newevent);

  std::map<std::string, fragrant::Variant*> data = newevent.data;
  std::map<std::string, fragrant::Variant*>::iterator niter;
  for (niter = data.begin(); niter != data.end(); niter++)
    delete niter->second;

  return;
}

std::vector<EventReceiver*>& EventPipeline::getTargetVector()
{
  return targets;
}

std::map<std::string, std::string>& EventPipeline::getAlias()
{
  return event_alias;
}

func EventPipeline::getConstructor()
{
  return 0; // not implemented yet
}

void EventPipeline::destroy()
{
  return; // shoudn't be called
}

std::string EventPipeline::getClassName()
{
  return "EventPipeline";
}

std::vector<std::string> EventPipeline::getClassFunctions()
{
  std::vector<std::string> functions;
  functions.push_back("raiseEvent");
  return functions;
}

Variant* EventPipeline::callFunction(std::string funcname,
  std::vector<Variant*> params, ScriptVM* script_vm)
{
  if (funcname != getClassFunctions().at(0))
    {
      std::cerr << "EventPipeline::callFunction(): No such function: \""
                << funcname << "\"" << std::endl;
      return 0;
    }

  if (params.size() < 2)
    {
      std::cerr << "EventPipeline::callFunction(): "
                << "too few arguments" << std::endl;
      return 0;
    }

  if (!params.at(0)->verifyType<std::string>() ||
      !params.at(1)->verifyType<std::vector<Variant*> >())
    {
      std::cerr << "EventPipeline::callFunction(): Wrong "
                << "types of arguments, expected (std::string, "
                << "std::vector<Variant*>) " << std::endl;
      return 0;
    }

  Event event;
  event.name = params.at(0)->get<std::string>();

  int cur;
  std::vector<Variant*> data = params.at(1)->get<std::vector<Variant*> >();
  for (cur = 0; cur < data.size(); cur++)
    {
      if (!data.at(cur)->verifyType<std::vector<Variant*> >() ||
          !data.at(cur)->get<std::vector<Variant*> >().size() < 2 ||
          !data.at(cur)->get<std::vector<Variant*> >().at(0)->
            verifyType<std::string>())
        {
          throw std::runtime_error("EventPipeline::callFunction(): "
            "moar errors");
          return 0;
        }

      else
        {
          std::vector<Variant*> current_item =
            data.at(cur)->get<std::vector<Variant*> >();
          std::string data_name = current_item.at(0)->get<std::string>();
          Variant* data_item = current_item.at(1);
          current_item.at(1) = 0;

          *(data.at(cur)) = current_item;
        }
    }

  *(params.at(1)) = data;

  raiseEvent(event);

  return 0;
}
