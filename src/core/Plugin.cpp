// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The plugin sauce

#include <iostream>
#include <stdexcept>

#include "../../include/core/Plugin.h"

using namespace fragrant;

Plugin::Plugin(std::string sofile)
{
  payload = 0;
  lib = dlopen(sofile.c_str(), RTLD_GLOBAL|RTLD_LAZY);
  if (lib)
    {
      void* func = dlsym(lib, PLUGIN_FUNC_NAME.c_str());
      if (!func)
        {
          std::cerr << "Plugin::Plugin(): Error getting function, "
                    << PLUGIN_FUNC_NAME << "\n\t" << dlerror() << std::endl;
          dlclose(lib);
          lib = 0;
        }

      else
        payload = (plugin_payload)func;

      payload_v = 0;

      void* infofunc = dlsym(lib, PLUGIN_INFO_NAME.c_str());
      if (!infofunc)
        std::cerr << "Plugin::Plugin(): Error getting function, "
                  << PLUGIN_INFO_NAME << dlerror() << std::endl;
      else
        {
          plugin_query function = (plugin_query)infofunc;
          data = function();
        }

    }
  else
    std::cerr << "Plugin::Plugin(): Error opening sofile, "
              << sofile << std::endl << "\tError: "
              << dlerror() << std::endl;

  if (!lib)
    throw std::runtime_error("");
}

Plugin::~Plugin()
{
  if (payload_v)
    delete payload_v;

  if (lib)
    dlclose(lib);
}

PluginData Plugin::getData()
{
  return data;
}

std::string Plugin::getDataString()
{
  std::string result;

  result = "\tName: " + data.name + "\n\tType: ";

  switch (data.type)
    {
      case (PT_Audio):
        {
          result += "PT_Audio";
          break;
        }
      case (PT_Graphics):
        {
          result += "PT_Graphics";
          break;
        }
      case (PT_Input):
        {
          result += "PT_Input";
          break;
        }
      case (PT_Media):
        {
          result += "PT_Media";
          break;
        }
      case (PT_Script):
        {
          result += "PT_Script";
          break;
        }
      case (PT_UI):
        {
          result += "PT_UI";
          break;
        }
    }


  if (data.authors.size())
    result +=
      ((data.authors.size() - 1) ? ("Authors: ") : ("\n\tAuthor: ")) +
      data.authors.at(0);

  int cur;
  for (cur = 1; cur < data.authors.size(); cur++)
    result += ", " + data.authors.at(cur);

  if (data.requirements.size())
    result += "\n\tRequirements: " + data.requirements.at(0);

  for (cur = 1; cur < data.requirements.size(); cur++)
    result += ", " + data.requirements.at(cur);

  return result;
}

fragrant::Variant* Plugin::getPayload()
{
  if (!payload_v)
    payload_v = payload();

  return payload_v;
}
