// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// moar sauce

#include "../../include/core/cURLDirectory.h"
#include "../../include/core/Magic.h"
#include "../../include/core/common.h"

#include <curlpp/cURLpp.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

#include <stdexcept>

using namespace fragrant;

URL::URL(std::string url)
{
  raw_url = url;

  ParsedPath pp = parsePath(url);

  protocol = pp.protocol;
  host = pp.host;
  path = pp.path;
  split_path = pp.split_path;
}

URL::URL(const URL& right)
{
  ParsedPath pp = parsePath(const_cast<URL&>(right).getURL());

  this->protocol = pp.protocol;
  this->host = pp.host;
  this->path = pp.path;
  this->split_path = pp.split_path;
}

std::string URL::getURL()
{
  return protocol + protocol_host_separator + host + path;
}

cURLDirectory::cURLDirectory(std::string poorl,
  std::map<std::string, archive_function> archive_funcs)
  : url(poorl)
{
  data_received = "";
  this->archive_funcs = archive_funcs;
}

cURLDirectory::~cURLDirectory()
{
  // nothing
}

std::string cURLDirectory::getFile(std::string filename)
{
  std::vector<std::string> portions =
    splitPath(simplifyPath(url.path + filename));

  if (portions.size() == 1)
    return fetchData(filename);

  Directory* last_directory = getDirectory(
    std::vector<std::string>(portions.begin(), portions.end() - 1));

  std::string data = last_directory->getFile(portions.at(portions.size() - 1));

  delete last_directory;

  return data;
}

std::vector<std::string> cURLDirectory::getListing(std::string folder)
{
  Magic mime_magic = Magic(true);
  throw std::runtime_error(
    "cURLDirectory::getListing(): not implemented yet");
}

Directory* cURLDirectory::getDirectory(std::vector<std::string> paths)
{
  if (!paths.size())
    {
      cURLDirectory* result = new cURLDirectory(url.getURL(), archive_funcs);
      return result;
    }

  std::string data;
  try
    {
      data = fetchData(paths.at(0));
    }
  catch (cURLpp::LogicError& e){}
  catch (cURLpp::RuntimeError& e){}

  Magic mime_magic = Magic(true);
  std::string mime_type = mime_magic.data(data);

  Directory* result = 0;

  std::vector<std::string> rest =
    std::vector<std::string>(paths.begin() + 1, paths.end());

  if (archive_funcs.find(mime_type) != archive_funcs.end())
    {
      archive_function function = archive_funcs[mime_type];
      Archive* arc = function(data);
      arc->setArchiveFuncs(archive_funcs);
      result = arc->getDirectory(rest);
      delete arc;
    }
  else
    {
      cURLDirectory* ndir = new cURLDirectory(
        url.getURL() + path_separator + paths.at(0), archive_funcs);
      result = ndir->getDirectory(rest);
      delete ndir;
    }

  return result;
}

int cURLDirectory::callback(cURLpp::Easy* ptr, char* data, int size, int nmemb)
{
  data_received += std::string(data, size * nmemb);

  return size * nmemb;
}

std::string cURLDirectory::fetchData(std::string filename)
{
  URL nurl = URL(url);
  nurl.path += path_separator + filename;
  nurl = URL(nurl.getURL());

  cURLpp::Easy request;

  cURLpp::Types::WriteFunctionFunctor
    functor(utilspp::BindFirst(utilspp::make_functor(this, 
    &cURLDirectory::callback), &request));
  cURLpp::Options::WriteFunction* write_function =
    new cURLpp::Options::WriteFunction(functor);

  request.setOpt(cURLpp::Options::Url(nurl.getURL()));
  request.setOpt(write_function);

  request.perform();

  std::string data = data_received;
  data_received = "";
  return data;
}

cURLpp::Cleanup cURLDirectory::cleanup_class;
