// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The Engine sauce

#include "../../include/core/Engine.h"

#include "../../include/core/Game.h"
#include "../../include/core/parsegame.h"
#include "../../include/core/SettingsDB.h"
#include "../../include/core/MediaLoader.h"
#include "../../include/core/common.h"

#include "../../include/plugins/ui/UI.h"

#include <dirent.h>
#include <sys/types.h>

#include <iostream> // debug
#include <stdexcept>

using namespace fragrant;

Engine::Engine()
{
  d_settings = 0;
  d_plugin_loader = 0;
}

Engine::~Engine()
{
  if (d_settings)
    delete d_settings;

  if (d_plugin_loader)
    delete d_plugin_loader;
}

EngineResults Engine::run(EngineParams params)
{
  initSettingsDB(params);

  if (!d_settings)
    return ER_Bad;

  SettingsDB& settings = *d_settings;

  initPluginLoader(params);

  if (!d_plugin_loader)
    {
      std::cerr << "Engine::run(): Couldn't make PluginLoader" << std::endl;
      return ER_Bad;
    }

  PluginLoader& plugin_loader = *d_plugin_loader;

  bool exit_fully = (params.params.find(GAME) != params.params.end());

  MediaLoader media_loader;
  media_loader.plugins() = getMediaPluginMap();

  if (exit_fully)
    {
      bool game_exists = false;
      fragrant::GameData gdata;
      std::string game_path = params.params[GAME];

      std::string presentation = "";

      while (!game_exists)
        {
          try
            {
              gdata = Game::parseXML(game_path, media_loader);
              game_exists = true;
            }

          catch (std::runtime_error& e)
            {
              std::cout << "Engine::run(): error or something\n\t"
                        << e.what() << std::endl;
            }

          if (game_exists)
            {
              try
                {
                  std::string presentation =
                    (params.params.find(fragrant::PRESENTATION) ==
                     params.params.end()) ?
                    settings.getPresentationName(gdata.name).
                      last_presentation :
                    params.params[fragrant::PRESENTATION];
                  std::string control_info =
                    settings.getPresentationName(gdata.name).controls;
                  if (control_info.size())
                    {
                      std::map<std::string, std::string> controls;

                      try
                        {
                          controls = XMLParser::parsegame::controlsTomap(
                            XMLParser::parsegame::
                            parsecontrolsFromBuffer(control_info));
                        }

                      catch (...)
                        {
                          controls.clear();
                        }

                      std::map<std::string, std::string>::iterator iter;
                      for (iter = controls.begin();
                           iter != controls.end(); iter++)
                        if (gdata.controls.find(iter->first) !=
                            gdata.controls.end())
                          gdata.controls[iter->first] = iter->second;
                    }

                  runGame(gdata, presentation, media_loader);
                }

              catch(std::runtime_error& e)
                {
                  std::cout << "Engine::run(): Error running game\n\t"
                            << e.what() << std::endl;
                  return ER_Bad;
                }

              return ER_Good;
            }

          UI* ui = getUI(params.params[UIPLUGIN]);

          if (!ui)
            {
              std::cerr << "Engine::run(): A lack of an UI plugin makes me "
                        << "a SAAAAD panda." << std::endl;
              return ER_Bad;
            }

          EngineSub results = ui->run(plugin_loader, settings);
          if (results.type == ESR_Quit)
            return ER_Good;

          delete ui;

          if (results.type == ESR_UI)
            std::cout << "What the hell??" << std::endl;

          game_path = results.game;
        }
    }

  else
    {
      UI* ui = getUI(params.params[UIPLUGIN]);

      if (!ui)
        {
          std::cerr << "Engine::run(): A lack of an UI plugin makes me "
                    << "a SAAAAD panda." << std::endl;
          return ER_Bad;
        }

      EngineSub results;
      results.type = ESR_UI;

      while(results.type == ESR_UI)
        results = ui->run(plugin_loader, settings);

      if (results.type == ESR_Quit)
        return ER_Good;

      bool game_exists = false;
      fragrant::GameData gdata;
      std::string game_path = results.game;

      while (!game_exists)
        {
          try
            {
              gdata = Game::parseXML(game_path, media_loader);
              game_exists = true;
            }

          catch (std::runtime_error& e)
            {
              std::cout << "Engine::run(): error or something\n\t"
                        << e.what() << std::endl;
            }

          if (game_exists)
            {
              try
                {
                  runGame(gdata, settings.getPresentationName(
                    gdata.name).last_presentation, media_loader);
                }

              catch(std::runtime_error& e)
                {
                  std::cout << "Engine::run(): Error running game\n\t"
                            << e.what() << std::endl;
                }
            }

          results.type = ESR_UI;
          while (results.type == ESR_UI)
            results = ui->run(plugin_loader, settings);

          if (results.type == ESR_Quit)
            return ER_Good;

          game_path = results.game;
          game_exists = false;
        }

      delete ui;
    }

  return ER_Good;
}

void Engine::initSettingsDB(EngineParams params)
{
  try
    {
      d_settings = new SettingsDB(
        params.params.find(DBLOC) == params.params.end() ? 
        (SettingsDB(DBLOC)) : SettingsDB(params.params[DBLOC]));
    }

  catch (std::runtime_error& error)
    {
      std::cerr << error.what() << std::endl;
      d_settings = 0;
    }
}

void Engine::initPluginLoader(EngineParams params)
{
  SettingsDB& settings = *d_settings;

  try
    {
      if (params.params.find(PLUGINFOLDER) == params.params.end())
        {
          DIR* p;
          if ((p = opendir(settings.pluginFolder().c_str())) != 0)
            {
              closedir(p);
              d_plugin_loader = new PluginLoader(settings.pluginFolder());
            }
          else
            {
              if ((p = opendir(PLUGINFOLDER.c_str())) != 0)
                {
                  d_plugin_loader = new PluginLoader(PLUGINFOLDER);
                  closedir(p);
                }
              else
                {
                  std::cerr << "Engine::initPluginLoader(): No "
                            << "plugin folder specified, using current "
                            << "directory" << std::endl;
                  d_plugin_loader = new PluginLoader(std::string("."));
                }
            }
        }
      else
        d_plugin_loader = new PluginLoader(params.params[PLUGINFOLDER]);
    }

  catch (std::runtime_error& error)
    {
      std::cerr << error.what() << std::endl;
      d_plugin_loader = 0;
    }
}

std::map<std::string, archive_function> Engine::getMediaPluginMap()
{
  std::map<std::string, archive_function> results;
  std::vector<Plugin*> media_plugins = d_plugin_loader->
    getPluginFromType(PT_Media);
  int cur;
  for (cur = 0; cur < media_plugins.size(); cur++)
    {
      Media item = media_plugins.at(cur)->getPayload()->get<Media>();
      results[item.mime_type] = item.func;
    }

  return results;
}

Plugin* Engine::getUIPlugin(std::string preferred)
{
  PluginLoader& plugin_loader = *d_plugin_loader;

  Plugin* ui_plugin = 0;

  if (preferred.size() && (ui_plugin = plugin_loader.getPlugin(preferred)))
    return ui_plugin;

  if (plugin_loader.getPluginFromType(PT_UI).size())
    return plugin_loader.getPluginFromType(PT_UI).at(0);

  std::cout << "Engine::getUIPlugin(): no UI plugin found" << std::endl;

  return 0;
}

UI* Engine::getUI(std::string preferred)
{
  Plugin* ui_plugin = getUIPlugin(preferred);
  if (!ui_plugin)
    return 0;

  return ui_plugin->getPayload()->get<UI*>();
}

// oh shit! we support fucking urls now. URLS!! Thanks to the magic that
// is libcurl, curlpp, pcre, and pcrepp

void Engine::runGame(GameData data, std::string presentation,
  MediaLoader loader)
{
  Game* the_game;
  try
    {
      the_game = new Game(data, presentation, loader, *d_plugin_loader);
    }

  catch (std::runtime_error& e)
    {
      std::cerr << "Engine::runGame(): Error making game\n\t"
                << e.what() << std::endl;
      the_game = 0;
    }

  if (!the_game)
    throw std::runtime_error("You lost the game");

  the_game->run();
  delete the_game;

  return;
}
