// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The source for the event pipeline

#include <iostream>
#include <stdexcept>
#include <pcrecpp.h>

#include "../../include/core/SettingsDB.h"

using namespace fragrant;

GameInfo fragrant::makeGameInfo(std::string name,
  std::string url, std::string last, std::string controls)
{
  GameInfo results;
  results.name = name;
  results.url = url;
  results.last_presentation = last;
  results.controls = controls;

  return results;
}

SettingsDB::SettingsDB(std::string dblocation)
{
  int result = sqlite3_open(dblocation.c_str(), &db);

  if (result)
    {
      std::cerr << "SettingsDB::SettingsDB(): Error opening db, "
                << sqlite3_errmsg(db) << std::endl;
      sqlite3_close(db);
      throw std::runtime_error("");
    }

  std::string query = "select * from sqlite_master;";
  table_exists = false;
  to_search = "general";
  sqlite3_exec(db, query.c_str(), mainquery, this, 0);

  if (!table_exists)
    {
      std::string query = "create table general(key varchar[10], "
                          "value varchar[20]);";
      sqlite3_exec(db, query.c_str(), mainquery, this, 0);
    }

  table_exists = false;
  to_search = "game_info";

  sqlite3_exec(db, query.c_str(), mainquery, this, 0);

  if (!table_exists)
    {
      std::string query = "create table game_info(gamename varchar[10],"
                          " lastmode varchar[20], url varchar[40], cont"
                          "rols varchar[100]);";
      sqlite3_exec(db, query.c_str(), mainquery, this, 0);
    }

  plugin_folder = getValueFromGeneral("plugin_folder");
  game_presentations = getGamePresentations();
}

SettingsDB::~SettingsDB()
{
  setValueFromGeneral("plugin_folder", plugin_folder);
  setGamePresentations(game_presentations);

  sqlite3_close(db);
}

std::string& SettingsDB::pluginFolder()
{
  return plugin_folder;
}

std::vector<GameInfo> SettingsDB::getGameList()
{
  std::vector<GameInfo> results;

  std::map<std::string, GameInfo>::iterator iter;
  for (iter = game_presentations.begin();
       iter != game_presentations.end();
       iter++)
    results.push_back(iter->second);

  return results;
}

GameInfo& SettingsDB::getPresentationName(std::string game)
{
  if (game_presentations.find(game) == game_presentations.end())
    game_presentations[game] = makeGameInfo("", "", "");

  return game_presentations[game];
}

std::string SettingsDB::getValueFromGeneral(std::string value)
{
  std::string query = "select * from general where key='" +
    sanitizeString(value) + "';";
  general_callback_called = false;
  sqlite3_exec(db, query.c_str(), generalcallback, this, 0);
  if (general_callback_called)
    return general_callback_result;
  else
    setValueFromGeneral(value, "");

  return "";
}

void SettingsDB::setValueFromGeneral(std::string key, std::string newvalue)
{
  std::string query = "select * from general where key='" +
    sanitizeString(key) + "';";
  general_callback_called = false;
  sqlite3_exec(db, query.c_str(), generalcallback, this, 0);

  if (general_callback_called)
    {
      std::string query = "update general set value='" +
        sanitizeString(newvalue) + "' where key='" + sanitizeString(key) + "';";
      general_callback_called = false;
      sqlite3_exec(db, query.c_str(), generalcallback, this, 0);
    }
  else
    {
      std::string query = "insert into general values('" +
        sanitizeString(key) + "', '" + sanitizeString(newvalue) +"');";
      general_callback_called = false;
      sqlite3_exec(db, query.c_str(), generalcallback, this, 0);
    }

  return;
}

std::map<std::string, GameInfo> SettingsDB::getGamePresentations()
{
  contents.clear();
  std::string query = "select * from game_info;";

  sqlite3_exec(db, query.c_str(), content_callback, this, 0);

  return contents;
}

void SettingsDB::setGamePresentations(
  std::map<std::string, GameInfo> presentations)
{
  std::map<std::string, GameInfo> current_contents = getGamePresentations();

  std::map<std::string, GameInfo>::iterator iter;
  for (iter = presentations.begin(); iter != presentations.end(); iter++)
    {
      std::string query;
      if (current_contents.find(iter->first) == current_contents.end())
        query = "insert into game_info values('" +
          sanitizeString(iter->first) + "', '" +
          sanitizeString(iter->second.last_presentation) + "', '" +
          sanitizeString(iter->second.url) + "', '" +
          sanitizeString(iter->second.controls) + "');";

      else
        query = "update game_info set lastmode='" +
          sanitizeString(iter->second.last_presentation) +"', url='" +
          sanitizeString(iter->second.url) + "', controls='" +
          sanitizeString(iter->second.controls) + "' where gamename='" +
          sanitizeString(iter->first) + "';";

      sqlite3_exec(db, query.c_str(), generalcallback, this, 0);
    }

  return;
}

std::string SettingsDB::sanitizeString(std::string input)
{
  pcrecpp::RE replace_stuff("'");
  replace_stuff.GlobalReplace("''", &input);

  // hot damn

  // my original code was buggy and obnoxious and longer than the
  // above solution

  // note to self: don't try and reinvent the wheel

  // unless the existing wheel is shitty

  return input;
}

int SettingsDB::mainquery(void* sdb, int argc, char** argv,
                          char** colname)
{
  SettingsDB* db = static_cast<SettingsDB*>(sdb);

  if (std::string(argv[1]) == db->to_search)
    db->table_exists = true;

  return 0;
}

int SettingsDB::generalcallback(void* sdb, int argc, char** argv,
                                char** colname)
{
  SettingsDB* db = static_cast<SettingsDB*>(sdb);

  db->general_callback_called = true;
  db->general_callback_result = argv[1];

  return 0;
}

int SettingsDB::content_callback(void* sdb, int argc, char** argv,
                                char** colname)
{
  SettingsDB* db = static_cast<SettingsDB*>(sdb);

  db->contents[argv[0]] = makeGameInfo(argv[0], argv[2], argv[1]);

  return 0;
}
