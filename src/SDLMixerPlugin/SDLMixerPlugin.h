// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// ?

#ifndef __SDLMIXERPLUGIN_H
#define __SDLMIXERPLUGIN_H

#include "../../include/plugins/audio/Audio.h"
#include "SDLMixerSample.h"
#include "SDLMixerDevice.h"

extern "C"
{
  fragrant::AudioSample* makeAudioSample(std::string path,
    fragrant::MediaLoader& loader);
  fragrant::AudioDevice* makeDevice();
}

#endif
