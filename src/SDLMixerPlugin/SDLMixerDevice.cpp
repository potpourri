// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// sauce

#include "SDLMixerDevice.h"
#include "../SDL/SDLManager.h"
#include <SDL/SDL_getenv.h>

#include <stdexcept>

using namespace fragrant;

SDLMixerDevice::SDLMixerDevice() {}
SDLMixerDevice::~SDLMixerDevice() {}

bool SDLMixerDevice::init(int frequency, int channels, int chunksize)
{
  localInit();
  return
    (Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, channels, chunksize) != -1);
}

void SDLMixerDevice::close()
{
  Mix_CloseAudio();
  localClose();
  delete this;
}

void SDLMixerDevice::playAudioSample(AudioSample* sample,
  int channel, int loops)
{
  SDLMixerSample* my_sample = dynamic_cast<SDLMixerSample*>(sample);

  if (!my_sample)
    throw std::runtime_error(
      "SDLMixerDevice::playAudioSample(): Coudln't cast sample"
      " to SDLMixerSample");

  Mix_PlayChannel(channel, my_sample->chunk, loops);

  return;
}

void SDLMixerDevice::pause()
{
  Mix_Pause(-1);

  return;
}

void SDLMixerDevice::resume()
{
  Mix_Resume(-1);

  return;
}

void SDLMixerDevice::setVolume(int volume, int channel)
{
  Mix_Volume(channel, volume);

  return;
}

void SDLMixerDevice::localClose()
{
//  SDL_putenv("SDL_AUDIODRIVER=dummy"); // debug

  SDLManager manager;
  manager.increment(SS_Audio);
}

void SDLMixerDevice::localInit()
{
  SDLManager manager;
  manager.decrement(SS_Audio);
}
