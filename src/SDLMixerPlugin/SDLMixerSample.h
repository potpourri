// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A SDL_mixer implementation of AudioSample

#ifndef __SDLMIXERSAMPLE_H
#define __SDLMIXERSAMPLE_H

#include <SDL/SDL_mixer.h>
#include "../../include/plugins/audio/AudioSample.h"

#include "SDLMixerDevice.h"

namespace fragrant
{
  class SDLMixerSample : public AudioSample
    {
      public:
        SDLMixerSample(Mix_Chunk* nchunk);
        ~SDLMixerSample();

        void destroy();

      private:

        friend class SDLMixerDevice;
        Mix_Chunk* chunk;
    };
}

#endif
