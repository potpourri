// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// machinery..blahblahblah

#include "SDLMixerPlugin.h"
#include "../../include/plugins/audio/AudioSample.h"
#include <stdexcept>

using namespace fragrant;

AudioSample* makeAudioSample(std::string path, MediaLoader& loader)
{
  std::string data;

  try
    {
      data = loader.loadMedia(path);
    }

  catch (std::runtime_error& e)
    {
      std::cerr << "make_image(): Error loading image\n\t"
                << e.what() << std::endl;
      return 0;
    }

  SDL_RWops* const_mem_sauce = SDL_RWFromConstMem(
    (const void*)data.c_str(), data.size());
  Mix_Chunk* chunk = Mix_LoadWAV_RW(const_mem_sauce, 1);

  SDLMixerSample* sample = new SDLMixerSample(chunk);

  return dynamic_cast<AudioSample*>(sample);
}

AudioDevice* makeDevice()
{
  return dynamic_cast<AudioDevice*>(new SDLMixerDevice);
}

PluginData queryPlugin()
{
  PluginData data;

  data.name = "SDL_mixer Audio Plugin";
  data.type = PT_Audio;
  data.version = "1.0";
  data.authors.push_back("Brian Caine");

  return data;
}

Variant* loadPluginPayload()
{
  Variant* payload = new Variant;

  Audio results;
  results.device_function = makeDevice;
  results.sample_function = makeAudioSample;

  *payload = results;

  return payload;
}
