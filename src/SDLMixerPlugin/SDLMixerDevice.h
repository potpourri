// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A fragrant::AudioDevice implemented with SDL_mixer

#ifndef __SDLMIXERDEVICE_H
#define __SDLMIXERDEVICE_H

#include "../../include/plugins/audio/AudioDevice.h"
#include "SDLMixerSample.h"

namespace fragrant
{
  class SDLMixerDevice : public AudioDevice
    {
      public:
        SDLMixerDevice();
        ~SDLMixerDevice();

        bool init(int frequency, int channels, int chunksize);
        void close();

        void playAudioSample(AudioSample* sample, int channel,
          int loops);

        void pause();
        void resume();

        void setVolume(int volume, int channel = -1);

      private:

        void localInit();
        void localClose();
    };
}

#endif
