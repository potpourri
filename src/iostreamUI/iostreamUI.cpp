// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// A simple ui that is just command line

#include <iostream>

#include "iostreamUI.h"

using namespace fragrant;

EngineSub iostreamUI::run(PluginLoader& plugin_loader, SettingsDB& settingsdb)
{
  EngineSub results;
  results.type = ESR_Quit;

  std::cout << "iostreamUI::run(): stub..." << std::endl;

  return results;
}
