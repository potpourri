/*% cc -o # %
 * ncsl -- count non-comment source lines
 * Deletes comments, white space and braces then counts non-empty lines.
 * Flag -t causes { and } not to be ignored.
 * Flag -b causes C++ comments //...\n to be ignored
 */
#include <stdio.h>
int tflag;	/* don't ignore Trivial lines */
int bflag;	/* ignore Bjarne-style comments */
int getnc(FILE *f){	/* get non-comment */
	int c;

	for(;;){
		c=getc(f);
		if(c!='/') return c;
		c=getc(f);
		if(bflag && c=='/'){
			do{
				c=getc(f);
				if(c==EOF) return EOF;
			}while(c!='\n');
			ungetc(c, f);
		}
		else{
			if(c!='*'){
				ungetc(c, f);
				return '/';
			}
			for(;;){
				do{
					c=getc(f);
					if(c==EOF) return EOF;
				}while(c!='*');
				c=getc(f);
				if(c=='/') break;
				ungetc(c, f);
			}
		}
	}
}	
int getnwnc(FILE *f){	/* get non-whitespace, non-comment */
	int c;

	do
		c=getnc(f);
	while(c==' ' || c=='\t' || (!tflag && (c=='{' || c=='}')));
	return c;
}
int count(FILE *f){
	int c, lastc, nline;

	nline=0;
	lastc=getnwnc(f);
	while((c=getnwnc(f))!=EOF){
		if(c=='\n' && lastc!='\n') nline++;
		lastc=c;
	}
	return nline;
}
main(int argc, char *argv[]){
	int total, i, n, error;
	char *c;
	FILE *f;
	char *cmd;

	error=0;
	cmd=argv[0];
	while(argc>1 && argv[1][0]=='-'){
		for(c=argv[1]+1;*c;c++) switch(*c){
		case 'b': bflag++; break;
		case 't': tflag++; break;
		default:
			fprintf(stderr, "Usage: %s -[bt] [file ...]\n", cmd);
			exit(1);
		}
		--argc;
		argv++;
	}
	switch(argc){
	case 1:
		printf("%d\n", count(stdin));
		break;
	case 2:
		f=fopen(argv[1], "r");
		if(f==0){
			perror(argv[1]);
			error=1;
		}
		else{
			printf("%d\n", count(f));
			fclose(f);
		}
		break;
	default:
		total=0;
		for(i=1;i!=argc;i++){
			f=fopen(argv[i], "r");
			if(f==0){
				perror(argv[i]);
				error=1;
			}
			else{
				n=count(f);
				fclose(f);
				printf("%s: %d\n", argv[i], n);
				total+=n;
			}
		}
		printf("total %d\n", total);
		break;
	}
	exit(error);
}
