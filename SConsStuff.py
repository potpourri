import os
from types import *

def pkg_config(package, prefix=""):

    if type(package) is StringType:

        command2 = ("""bash -c 'PKG_CONFIG_PATH=\"""" + prefix +
            """\" pkg-config --cflags """ + package + """' """)

        pkgpipe = os.popen(command2)
        flags = (pkgpipe.read())[:-1]
        pkgpipe.close()

        slash_en = '\\n'

        command1 = ("""bash -c 'PKG_CONFIG_PATH=\"""" +
              prefix + """\" pkg-config --libs """ + package + 
              """ | sed -e "s/-l/""" + slash_en +
              """LIB /g" | grep LIB | sed -e "s/LIB //g" | tr -d " "'""")

        pkgpipe = os.popen(command1)
        libs = (pkgpipe.read().split('\n'))[1:-1]
        pkgpipe.close()

        return [flags, libs]

    if type(package) is TupleType or type(package) is ListType:

        results = ['', []]

        for input in package:
            result = pkg_config(input, prefix)

            if len(result):
                results[0] += result[0]
                results[1] += result[1]

        return results

def other_command(command):

   command_pipe = os.popen(command)
   results = (command_pipe.read())[:-1]
   command_pipe.close()

   return results
