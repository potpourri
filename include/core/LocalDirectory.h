// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A Directory on the local computer. cURLDirectory might be able to do
// more, but this is better suited

#ifndef __LOCALDIRECTORY_H
#define __LOCALDIRECTORY_H

#include "Directory.h"
#include "../plugins/media/Archive.h"

#include "common.h"

namespace fragrant
{
  const std::string directory = "directory";

  class LocalDirectory : public Directory
    {
      public:
        LocalDirectory(std::string base,
          std::map<std::string, archive_function> funcs);
        ~LocalDirectory();

        std::string getFile(std::string filename);
        std::vector<std::string> getListing(std::string folder);

        Directory* getDirectory(std::vector<std::string> path);

      private:

        std::string base;

        std::string fetchData(std::string path);

        std::map<std::string, archive_function> functions;
    };
}

#endif
