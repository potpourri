// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// This is the SettingsDB class
// So, the db has the following tables:
// General Settings, with the format of string key, string value, named general

#ifndef __SETTINGSDB_H
#define __SETTINGSDB_H

#include <sqlite3.h>
#include <string>
#include <vector>
#include <map>

namespace fragrant
{
  struct GameInfo
    {
      std::string name;
      std::string url;
      std::string last_presentation;
      std::string controls;
    };

  GameInfo makeGameInfo(std::string, std::string, std::string,
    std::string controls = "");

  class SettingsDB
    {
      public:

        SettingsDB(std::string dblocation);
        ~SettingsDB();

        std::string& pluginFolder();
        std::vector<GameInfo> getGameList();
        GameInfo& getPresentationName(std::string);

      private:
        std::string plugin_folder;
        std::map<std::string, GameInfo> game_presentations;

        sqlite3* db;

        std::string getValueFromGeneral(std::string value);
        void setValueFromGeneral(std::string key, std::string newvalue);

        std::map<std::string, GameInfo> getGamePresentations();
        void setGamePresentations(std::map<std::string, GameInfo>);

        static std::string sanitizeString(std::string the_string);

        static int mainquery(void* sdb, int argc, char** argv, char** colname);
        bool table_exists;
        std::string to_search;

        static int generalcallback(void* sdb, int argc, char** argv,
                                   char** colname);
        std::string general_callback_result;
        bool general_callback_called;

        std::map<std::string, GameInfo> contents;
        static int content_callback(void* sdb, int argc, char** argv,
                                   char** colname);
    };
}

#endif
