// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by 
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of 
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License 
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// This is the Engine class

#ifndef __ENGINE_H
#define __ENGINE_H

#include <map>
#include <string>

#include "PluginLoader.h"
#include "SettingsDB.h"
#include "Game.h"
#include "MediaLoader.h"

#include "../plugins/ui/UI.h"
#include "../plugins/media/Media.h"

namespace fragrant
{
  const std::string DBLOC = ".settings.db";
  const std::string PLUGINFOLDER = "./plugins";
  const std::string GAME = "GAME";
  const std::string PRESENTATION = "hehehe";
  const std::string UIPLUGIN = "iostreamUI";

  enum EngineResults
    {
      ER_Good,
      ER_Bad
    };

  enum EngineSubResults  // lulz, esr
    {
      ESR_UI,
      ESR_Game,
      ESR_Quit
    };

  struct EngineSub
    {
      EngineSubResults type;
      std::string game;
      std::string presentation;
    };

  struct EngineParams
    {
      std::map<std::string, std::string> params;
    };

  class UI;
  class Engine
    {
      public:

        Engine();
        ~Engine();

        EngineResults run(EngineParams params);

      private:
        SettingsDB* d_settings;
        PluginLoader* d_plugin_loader;

        void initSettingsDB(EngineParams params);
        void initPluginLoader(EngineParams params);

        std::map<std::string, archive_function> getMediaPluginMap();

        Plugin* getUIPlugin(std::string preferred = "");
        UI* getUI(std::string preferred = "d");

        void runGame(GameData, std::string, MediaLoader);
    };
}

#endif
