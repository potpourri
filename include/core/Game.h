// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// A struct to hold an entire game definition

#ifndef __GAME_H
#define __GAME_H

#include "Level.h"
#include "Actor.h"

#include "../plugins/graphics/Graphics.h"
#include "../plugins/script/ScriptVM.h"
#include "../plugins/audio/Audio.h"
#include "../plugins/input/InputManager.h"

#include "EventPipeline.h"

#include <string>
#include <vector>
#include <map>

namespace fragrant
{
  const std::string GAMEFILE = "game.xml";

  struct GameMedia
    {
      std::string plugin_name;
      std::string name;
      std::string contents;
    };

  struct GamePresentation
    {
      std::string name;
      Pair<int> size;
      bool net;
    };

  struct GameData
    {
      // meta
      std::string name;
      std::string version;
      std::vector<std::string> authors;
      std::string icon;  // why the hell did i provide for an icon?

      std::string base;

      std::vector<GamePresentation> presentations;
      std::map<std::string, std::string> controls;

      std::string graphics_plugin;
      std::string scripting_plugin;
      std::string audio_plugin;
      std::string input_plugin;

      std::vector<std::string> extra_plugins;

      std::vector<GameMedia> media;

      std::vector<std::string> actors;
      std::vector<std::string> levels;

      void printData();
    };

  struct GameInit
    {
      GraphicsPair size;
      int bpp;

      int frequency;
      int format;
      int channels;
      int chunksize;
    };

  class Level;
  struct ActorData;
  struct LevelData;
  class MediaLoader;
  class Game : public EventReceiver, public ScriptedClass
    {
      public:

        Game(GameData data, std::string presentation,
          MediaLoader& nmedia_loader, PluginLoader& nplugin_loader);
        ~Game();

        void init(GameInit params);
        void run();

        void raiseEvent(Event event);

        Drawable* getGraphics(std::string name,
          std::map<std::string, Variant*> params);
        AudioSample* getAudio(std::string name);
        std::string getFile(std::string name);

        // script stuff

        func getConstructor();
        void destroy();

        std::string getClassName();
        std::vector<std::string> getClassFunctions();

        Variant* callFunction(std::string funcname,
          std::vector<Variant*> params, ScriptVM* script_vm);

        // other stuff

        static Variant* getCurrentGame(std::vector<Variant*>);
        static GameData parseXML(std::string path, MediaLoader&);

        InputManager* getInputManager();

      private:
        MediaLoader& media_loader;
        PluginLoader& plugin_loader;

        Display* display;
        AudioDevice* audio_device;

        Graphics graphics;
        Audio audio;
        ScriptVM* script_vm;
        InputManager* input_manager;

        Level* level;

        std::string presentation;
        GameData data;

        std::map<std::string, LevelData> level_defs;
        std::vector<ActorData> actor_defs;

        std::map<std::string, Drawable*> graphic_samples;
        std::map<std::string, AudioSample*> audio_samples;

        static Game* current_game;
    };
}

#endif
