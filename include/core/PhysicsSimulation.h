// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


//NOTES:
// A oop wrapper for chipmunk

#ifndef __PHYSICSSIMULATION_H
#define __PHYSICSSIMULATION_H

#include <chipmunk.h>

#include "Actor.h"
#include "PhysicsObject.h"
#include "../plugins/input/InputManager.h"

namespace fragrant
{
  const int ITER = 20;
  const int DIFF = 70;
    // collision events must be DIFF milliseconds apart to fire an event
  const std::string COLLISION_EVENT_NAME = "ET_Collision";

  typedef Pair<float> ActorPair;
  ActorPair makePair(float, float);
  const fragrant::ActorPair GRAVITY = fragrant::makePair(0.0, -.002);

  class Actor;
  class PhysicsObject;
  class PhysicsSimulation
    {
      public:
        PhysicsSimulation();
        ~PhysicsSimulation();

        void setInputManager(InputManager* manager);
        InputManager* getInputManager();

        void process(float time, float increment);

        void addPhysicsObject(PhysicsObject* object);
        void removePhysicsObject(PhysicsObject* object);

        bool isNewEvent(Actor*, Actor*);

      private:
        friend class PhysicsObject;

        cpSpace* space;

        InputManager* input_manager;
        std::vector<PhysicsObject*> objects;
        std::map<long unsigned int, int> collision_times;

        static bool initted;
    };
}

#endif
