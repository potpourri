// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// A level is, well, the current level.

#ifndef __LEVEL_H
#define __LEVEL_H

#include <vector>
#include <string>
#include <map>

#include "Actor.h"
#include "MediaLoader.h"
#include "PluginLoader.h"
#include "PhysicsSimulation.h"
#include "EventPipeline.h"
#include "parselevel.h"

#include "../plugins/graphics/Graphics.h"
#include "../plugins/script/ScriptVM.h"
#include "../plugins/audio/Audio.h"
#include "../plugins/input/InputManager.h"

namespace fragrant
{
  typedef Pair<int> LevelPair;
  const int PROCESS_TIME = 20;
  const float PROCESS_PIECE = 0.05;

  struct LevelActor
    {
      std::string name;
      std::string source;
      float zindex;
      float angle;
      LevelPair position;
    };

  struct LevelGraphicParam
    {
      std::string name;
      std::string type;
      std::string value;
    };

  struct LevelGraphic
    {
      std::string source;
      float zindex;
      float angle;
      LevelPair position;
      std::vector<LevelGraphicParam> params;
    };

  class Actor;
  struct LActor // this bullshit is not cool
    {
      float zindex;
      Actor* actor;
    };

  struct StaticGraphic
    {
      LevelPair position;
      std::string name;
      float zindex;
      float angle;
      Drawable* drawable;
    };

  struct ActorData;

  struct LevelSlice
    {
      std::string name;
      std::string presentation;
      bool startup;
      LevelPair viewport;
      std::vector<LevelActor> actors;
      std::vector<LevelGraphic> static_graphics;
    };

  struct LevelData
    {
      std::string name;
      std::map<std::string, LevelSlice> slices;
      std::vector<std::string> presentations;

      LevelSlice getSlice(std::string);
      std::string toString();
    };

  class PhysicsSimulation;
  class Actor;
  class Game;
  class Display;
  class Level : public ScriptedClass
    {
      public:
        Level(LevelSlice, Game& game);
        Level(LevelSlice, Level&, Game&);
        ~Level();

        void init(std::vector<ActorData> actor_defs,
          EventPipeline* pipeline, Audio& audio, ScriptVM* script_vm);
        void cycle(Display*);

        int findIndex(Actor* actor);

        // scripting stuff

        func getConstructor();
        void destroy();

        std::string getClassName();
        std::vector<std::string> getClassFunctions();
        Variant* callFunction(std::string funcname,
          std::vector<Variant*> params, ScriptVM* script_vm);

        // parse level

        static LevelData parseXML(std::string source,
                                  MediaLoader& media_loader);
      private:
        std::string presentation;
        LevelSlice level_data;

        PhysicsSimulation* simulation;

        LevelPair viewport;

        Game& game;

        std::vector<ActorData> actor_defs;
        EventPipeline* pipeline;
        ScriptVM* script_vm;
        Audio audio;

        std::vector<LActor> actors;
        std::vector<StaticGraphic> static_graphics;
    };
};

#endif
