// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A Directory that gets shit via curl
// curl is a bitchin' library
// and curlpp is a bitchin' binding for a bitchin' library

#ifndef __CURLDIRECTORY_H
#define __CURLDIRECTORY_H

#include "../../include/core/Directory.h"
#include "../../include/plugins/media/Archive.h"

#include <map>
#include <vector>
#include <string>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>

namespace fragrant
{
  const std::string protocol_host_separator = "://";

  struct URL
    {
      std::string protocol;
      std::string host;
      std::string path;

      std::vector<std::string> split_path;
      std::string raw_url;

      std::string getURL();
      URL(std::string);
      URL(const fragrant::URL& right);
    };

  class cURLDirectory : public Directory
    {
      public:
        cURLDirectory(std::string durl,
          std::map<std::string, archive_function>);
        ~cURLDirectory();

        std::string getFile(std::string filename);
        std::vector<std::string> getListing(std::string folder);

        Directory* getDirectory(std::vector<std::string> paths);

      private:

        int callback(cURLpp::Easy* ptr, char* data, int size, int nmemb);

        std::string fetchData(std::string url);

        std::string data_received;
        URL url;
        std::map<std::string, archive_function> archive_funcs;

        static cURLpp::Cleanup cleanup_class;
    };
}

#endif
