// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// A plugin system

#ifndef __PLUGIN_H
#define __PLUGIN_H

#include <string>
#include <vector>
#include <dlfcn.h>

#include "Variant.h"

namespace fragrant
{
  const std::string PLUGIN_FUNC_NAME = "loadPluginPayload";
  const std::string PLUGIN_INFO_NAME = "queryPlugin";

  enum PluginType
    {
      PT_Audio,
      PT_Graphics,
      PT_Input,
      PT_Media,
      PT_Script,
      PT_UI
    };

  struct PluginData
    {
      std::string name;
      PluginType type;
      std::string version;
      std::vector<std::string> authors;
      std::vector<std::string> requirements;
    };

  typedef PluginData (*plugin_query)();
  typedef Variant* (*plugin_payload)();

  class Plugin
    {
      public:
        Plugin(std::string sofile);
        ~Plugin();

        PluginData getData();
        std::string getDataString();
        fragrant::Variant* getPayload();

      private:
        PluginData data;

        void* lib;
        plugin_payload payload;
        fragrant::Variant* payload_v;
    };
}

extern "C" 
{
  fragrant::PluginData queryPlugin();
  fragrant::Variant* loadPluginPayload();
}

#endif
