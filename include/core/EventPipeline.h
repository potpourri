// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// EventPipeline, well named, no?

#ifndef __EVENTPIPELINE_H
#define __EVENTPIPELINE_H

#include "EventReceiver.h"
#include "ScriptedClass.h"

#include <map>
#include <vector>

namespace fragrant
{
  class EventPipeline : public EventReceiver, public ScriptedClass
    {
      public:
        void raiseEvent(Event new_event);

        std::vector<EventReceiver*>& getTargetVector();
        std::map<std::string, std::string>& getAlias();

        // script stuff

        func getConstructor();
        void destroy();

        std::string getClassName();
        std::vector<std::string> getClassFunctions();

        Variant* callFunction(std::string funcname,
          std::vector<Variant*>, ScriptVM* script_vm);

      private:
        std::vector<EventReceiver*> targets;
        std::map<std::string, std::string> event_alias;
    };
}

#endif
