// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// A class that loads media from sauces

#ifndef __MEDIALOADER_H
#define __MEDIALOADER_H

#include <string>
#include <string>
#include <map>

#include "../Global.h"

#include "../../include/plugins/media/Media.h"
#include "PluginLoader.h"

namespace fragrant
{
  class Game;
  class MediaLoader
    {
      public:

        MediaLoader();
        ~MediaLoader();

        std::string loadMedia(std::string source);
        std::map<std::string, archive_function>& plugins();

        Game* getGame();
        void setGame(Game* ngame);

      private:
        std::map<std::string, archive_function> plugin_map;

        Game* game;
    };
}

#endif
