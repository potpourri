// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// This is the Actor class.

#ifndef __ACTOR_H
#define __ACTOR_H

#include "MediaLoader.h"
#include "ScriptedClass.h"
#include "EventReceiver.h"
#include "Level.h"

#include "Game.h"

#include "../Global.h"

#include "../plugins/graphics/Graphics.h"
#include "../plugins/audio/Audio.h"
#include "../plugins/input/InputManager.h"

#include <string>
#include <map>
#include <vector>

namespace fragrant
{
  typedef Pair<float> ActorPair;
  typedef Pair<int> LevelPair;

  ActorPair makePair(float, float);

  const int INV_Y = 0;

  struct ActorGraphics  // fixme
    {
      std::string sauce;
      ActorPair pos;
      float angle;
      std::map<std::string, Pair<std::string> > params;
      // params[param name].x = type
      // params[param name].y = value
    };

  // a line fills the first two pairs in PhysicsShape's data member with
  // the first and second point. The radius of the line is set as the x
  // member for a third

  // Circles have x of one member of data set to the radius

  enum Shape
    {
      S_Line,
      S_Circle,
      S_Poly
    };

  struct PhysicsShape
    {
      Shape shape;
      std::vector<ActorPair> data;
      ActorPair offset;

      float elasticity;
      float friction;
    };

  class Actor;
  struct ActorPhysics
    {
      float mass;
      float inertia;
      float rotational_inertia;

      bool generate_rotational_inertia;
      bool fixed;

      Actor* the_actor;
      LevelPair base_position; // it is really only relevant for lines

      std::vector<PhysicsShape> shapes;  // oh god, no!
    };

  class Drawable;

  struct ActorGraphic  // fixme: poor choice of naming here
    {
      LevelPair position;
      float angle;
      Drawable* graphic;
      std::string name;
    };

  class Game;
  class PhysicsObject;
  class PhysicsSimulation;
  class Target;
  class MediaLoader;
  class Level;
  struct ActorData
    {
      std::string name;

      std::vector<ActorGraphics> graphics; // fixme: per other fixmes
      ActorPhysics physics;
      std::map<std::string, std::string> scripts;
      LevelPair position;

      Audio audio;
      ScriptVM* script_vm;
      InputManager* input_manager;
      PhysicsSimulation* simulation;
      MediaLoader* media_loader;

      Level* level;
      Game* game;
    };

  class Actor : public ScriptedClass, public EventReceiver
    {
      public:
        Actor(ActorData data);
        ~Actor();

        void draw(Target* target, LevelPair offset);
        void setAngle(float);
        float getAngle();

        std::string getName();
        int getIndex();

        // event stuff
        void raiseEvent(Event event);

        // script stuff

        func getConstructor();
        void destroy();

        std::string getClassName();
        std::vector<std::string> getClassFunctions();
        Variant* callFunction(std::string funcname,
          std::vector<Variant*> params, ScriptVM* script_vm);

        // xml parsing stuff

        static ActorData parseXML(std::string source,
          MediaLoader& media_loader);

      private:
        std::vector<ActorGraphic> sprites;

        std::map<std::string, std::string> ldata;
        bool persistent;

        ActorData initial_params;

        Event cur_event;
        LevelPair position;
        PhysicsObject* physics_object;
    };
}

#endif
