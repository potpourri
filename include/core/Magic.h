// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// My magic class

#ifndef __MAGIC_H
#define __MAGIC_H

#include <string>
#include <magic.h>

namespace fragrant
{
  class Magic
    {
      public:
        Magic(bool mime = false);
        ~Magic();

        std::string file(std::string filename);
        std::string data(std::string data);

      private:
        magic_t cookie;
    };
}

#endif
