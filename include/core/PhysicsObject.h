// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// represents a physics object

#ifndef __PHYSICSOBJECT_H
#define __PHYSICSOBJECT_H

#include <vector>

#include "Actor.h"
#include "PhysicsSimulation.h"

namespace fragrant
{
  const int COLLISION_TYPE = 1;

  typedef Pair<float> ActorPair;

  struct PhysicsShape;
  struct ActorPhysics;
  class PhysicsSimulation;
  class PhysicsObject
    {
      public:
        PhysicsObject(ActorPhysics data);
        ~PhysicsObject();

        void setPosition(ActorPair npos);
        ActorPair getPosition();

        void setAngle(float);
        float getAngle();

        ActorPair getCenter();
        ActorPair Object2World(ActorPair input);

        void applyImpulse(ActorPair force, ActorPair offset);

        ActorPair getVelocity();
        void setVelocity(ActorPair nvelocity);

        float getAngularVelocity();
        void setAngularVelocity(float nvelocity);

      private:
        friend class PhysicsSimulation;

        void addToSpace(cpSpace*);
        void removeFromSpace(cpSpace*);

        static float getMoment(std::vector<PhysicsShape>, float);

        cpBody* body;
        std::vector<cpShape*> shapes; // again, gwar
        bool fixed;

        PhysicsSimulation* parent;
    };
}

#endif
