// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// Any directory or directory like object should implement this stuff

// However, nothing I've written that inherits from this does

#ifndef __DIRECTORY_H
#define __DIRECTORY_H

#include <string>
#include <vector>

namespace fragrant
{
  class Directory
    {
      public:
        virtual ~Directory();

        virtual std::string getFile(std::string source) = 0;
        virtual std::vector<std::string> getListing(std::string folder) = 0;
        virtual Directory* getDirectory(std::vector<std::string>) = 0;
    };
}

#endif
