// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This is a collection of odd portions of various functions
// shitty written, of course

#ifndef __COMMON_H
#define __COMMON_H

#include <vector>
#include <string>
#include "../Global.h"
#include <sstream>

const char path_separator = '/';

const std::string parent_dir = "..";
const std::string current_dir = ".";

const float PI = 3.14159;

const std::string url_regexp =
  "([a-zA-Z+-.]*)://([a-zA-Z.&:0-9]*)([~a-zA-Z./%0-9?=-]*)";

const std::string hostless_protocols[] = {"file"};

struct ParsedPath
{
  std::vector<std::string> split_path;
  std::string raw_path;

  std::string protocol;
  std::string path;
  std::string host;
};

enum DirectoryNesting
{
  DN_FormerInLatter,
  DN_LatterInFormer,
  DN_Unrelated,
  DN_Identical
};

template <typename T>
std::string toString(T input)
{
  std::stringstream d;
  d << input;
  std::string result;
  d >> result;
  return result;
}

std::string joinPath(std::vector<std::string> path);
std::vector<std::string> splitPath(std::string path);
std::string simplifyPath(std::string orig);
DirectoryNesting compareDirectories(std::string patha, std::string pathb);
std::vector<std::string> pathDifference(std::string patha, std::string pathb);
bool isRelative(std::string path);
ParsedPath parsePath(std::string path);

bool isURL(std::string);

std::vector<std::string> splitString(std::string, std::string=" ");

// fuck radians

float toRadians(float);
float fromRadians(float);

int getticks();
void dosleep(int);

Pair<float> getOffset(Pair<float>, float);

#endif
