// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.

// NOTES:

// This is probably my millionth attempt at a good variant.

// Probably not going to end very well

#ifndef __VARIANT_H
#define __VARIANT_H

#include <typeinfo>
#include <iostream>
#include <string>

namespace fragrant
{
  class Generic
    {
      public:
        virtual void null();
        virtual ~Generic();
    };

  template <class T>
  class Specific : public Generic
    {
      public:
        Specific<T>()
          {
          }

        ~Specific<T>()
          {
          }

        Specific<T>(Specific<T> const &rhs)
          {
            if (this != &rhs)
              this->data = rhs.data;
          }

        Specific<T>(T const &rhs)
          {
            data = rhs;
          }

        Specific<T>& operator=(T const &rhs)
          {
            data = rhs;
            return *this;
          }

        operator T() const
          {
            return data;
          }

      private:
        T data;
    };

  class Variant
    {
      public:
        Variant();
        Variant(Variant const &rhs);
        ~Variant();

        Variant& operator=(Variant const &rhs);

        template <class T>
        Variant& operator=(T const &rhs)
          {
            if (data == 0)
              {
                data = new Specific<T>();
                type = typeid(T).name();
              }

            Specific<T>* pointer = new Specific<T>();

            if (verifyType<T>())
              {
                *pointer = rhs;
                data = pointer;
              }

            else
              {
                delete data;
                data = pointer;
              }

            type = typeid(T).name();
            return *this;
          }

        template <class T>
        operator T() const
          {
            return get<T>();
          }

        template <class T>
        T force_get() // cheap hack, sue me
          {
            Specific<T>* pointer = (Specific<T>*)data;
            return static_cast<T>(*pointer);
          }

        template <class T>
        T get()
          {
            if (typeid(T).name() == type)
              return force_get<T>();

            throw(std::bad_typeid());
          }

        template <class T>
        bool verifyType()
          {
            if (typeid(T).name() == type)
              return true;
            return false;
          }

      private:
        Generic* data;
        std::string type; // gwar, /me is displeased for having to do this
    };

  Variant* duplicateVariant(Variant*);
}

#endif
