// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This dtd is too complicated to parse auto-magically, so this is
// actually hand coded

// won't be any better though

#ifndef __PARSELEVEL_H
#define __PARSELEVEL_H

#include "Level.h"

#include <string>
#include <libxml++/libxml++.h>

namespace fragrant
{
  struct LevelActor;
  struct LevelGraphic;
  struct LevelData;
  struct LevelSlice;
  typedef Pair<int> LevelPair;
}

namespace XMLParser
{
  namespace parselevel
    {
      std::string getPresentationName(xmlpp::Node*);
      xmlpp::Node* getPresentationData(xmlpp::Node*);

      fragrant::LevelActor getActor(xmlpp::Node*, std::string);
      fragrant::LevelGraphic getGraphic(xmlpp::Node*, std::string);
      fragrant::LevelPair getPair(xmlpp::Node*, std::string);

      std::vector<std::string> getPresentationNames(xmlpp::Node*);
      fragrant::LevelSlice getLevelSlice(xmlpp::Node*, std::string);

      fragrant::LevelData parselevel(xmlpp::Node*);

      fragrant::LevelData parselevelFromBuffer(std::string);
    }
}

#endif
