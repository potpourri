// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A class that inherits from this can be scripted

#ifndef __SCRIPTEDCLASS_H
#define __SCRIPTEDCLASS_H

#include "../plugins/script/ScriptVM.h"

typedef fragrant::Variant* (*func)(std::vector<fragrant::Variant*>);

namespace fragrant
{
  class ScriptVM;
  class ScriptedClass
    {
      public:
        virtual func getConstructor() = 0;
        virtual void destroy() = 0;

        virtual std::string getClassName() = 0;
        virtual std::vector<std::string> getClassFunctions() = 0;

        virtual Variant* callFunction(std::string funcname,
          std::vector<Variant*> params, ScriptVM* script_vm) = 0;

      private:
    };
}

#endif
