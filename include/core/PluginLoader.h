// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// It loads plugins

#ifndef __PLUGINLOADER_H
#define __PLUGINLOADER_H

#include <string>
#include <vector>
#include <map>
#include <magic.h>

#include "Plugin.h"

namespace fragrant
{
  const std::string SOFILE = "application/x-sharedlib";

  class PluginLoader
    {
      public:

        PluginLoader(std::string folder);
        ~PluginLoader();

        Plugin* getPlugin(std::string name);
        std::vector<Plugin*> getPluginFromType(PluginType type);

      private:
        std::map<std::string, Plugin*> plugins;
        magic_t cookie;
    };
}

#endif
