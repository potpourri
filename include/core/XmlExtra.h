// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// a few extra pieces of code

#ifndef __XMLEXTRA_H
#define __XMLEXTRA_H

#include <libxml++/libxml++.h>

#include <sstream>
#include <string>

#include "../Global.h"
#include "common.h"

std::string getContents(xmlpp::Node*);

const std::string pair_delim = ",";
const std::string match_string = "something_random";

template<typename T>
T fromString(std::string input)
{
  std::stringstream s;

  T output;

  s << input;
  s >> output;

  return output;
}

Pair<float> parsePair(std::string);

bool validXML(std::string dtd, std::string name, xmlpp::DomParser* parser);

#endif
