// global stuff

#ifndef __GLOBAL_H
#define __GLOBAL_H

#define NIY "not implemented yet"

template <class T>
struct Pair
{
  T x;
  T y;
};

template <class T>
struct Rect
{
  Pair<T> xy;
  Pair<T> size;
};

#endif
