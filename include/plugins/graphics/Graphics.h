// header for graphics subsystem

#ifndef __GRAPHICS_H
#define __GRAPHICS_H

#include <string>

#include "../../core/MediaLoader.h"

#include "../../Global.h"

typedef Pair<int> GraphicsPair;
typedef Rect<int> GraphicsRect;

#include "Display.h"
#include "Drawable.h"

namespace fragrant
{
  class Display;
  class Drawable;
  class MediaLoader;

  typedef Drawable* (*image_function)
    (MediaLoader&,std::string, std::map<std::string, Variant*>);
  typedef Display* (*display_function)();
  typedef void (*destroy_function)(Drawable*);

  struct Graphics
    {
      display_function makeDisplay;
      image_function makeImage;

      destroy_function deleteImage;
    };
}

#endif
