// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This is a Target that represents the screen

#ifndef __DISPLAY_H
#define __DISPLAY_H

#include "Graphics.h"
#include "Target.h"

namespace fragrant
{
  class Display : public Target
    {
      public:

        virtual bool init(GraphicsPair size, int bpp) = 0;
        virtual bool resize(GraphicsPair size, int bpp) = 0;
        virtual void close() = 0;

        virtual void flip() = 0;

      private:
    };
}

#endif
