// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This represents a class that can be drawn on a target
// Any specific implementation is up to the graphics plugin

#ifndef __DRAWABLE_H
#define __DRAWABLE_H

#include "Graphics.h"
#include "Target.h"

#include "../../core/ScriptedClass.h"

namespace fragrant
{
  class Target;
  class Drawable
    {
      public:
        virtual void draw(Target* target, GraphicsRect* dstrect, 
                          GraphicsPair* srcrect, float angle) = 0;
        virtual ScriptedClass* getScriptObject() = 0;
        virtual void destroy() = 0;

      private:
    };
}

#endif
