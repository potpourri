// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This represents something which can be drawn on
// Also, it's nothing so far, as it should be implementation dependent

#ifndef __TARGET_H
#define __TARGET_H

#include "Graphics.h"

namespace fragrant
{
  class Display;
  class Target
    {
      public:
      private:
        virtual void d();
    };
}

#endif
