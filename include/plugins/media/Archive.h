// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This describes an archive

#ifndef __ARCHIVE_H
#define __ARCHIVE_H

#include "../../core/Directory.h"

#include <string>
#include <map>

namespace fragrant
{
  class Archive;
  typedef Archive* (*archive_function)(std::string);

  class Archive : public Directory
    {
      public:
        virtual void setArchiveFuncs(
          std::map<std::string, archive_function>) = 0;
    };
}

#endif
