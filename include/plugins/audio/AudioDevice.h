// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// An audio device. What else?

#include "../../Global.h"

#include "AudioSample.h"

#ifndef __AUDIODEVICE_H
#define __AUDIODEVICE_H

namespace fragrant
{
  class AudioDevice
    {
      public:

        virtual bool init(int frequency, int channels, int chunksize) = 0;
        virtual void close() = 0;

        virtual void playAudioSample(AudioSample* sample,
          int channel = -1, int loops = 0) = 0;

        virtual void pause() = 0;
        virtual void resume() = 0;

        virtual void setVolume(int volume, int channel = -1) = 0;
      private:
    };
}

#endif
