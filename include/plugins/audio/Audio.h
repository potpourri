// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This is the top level audio header

#ifndef __AUDIO_H
#define __AUDIO_H

#include "AudioDevice.h"
#include "AudioSample.h"

#include "../../Global.h"
#include "../../core/MediaLoader.h"

#include <string>

namespace fragrant
{
  class MediaLoader;

  typedef AudioSample* (*audio_function)(std::string, MediaLoader&);
  typedef AudioDevice* (*make_device_function)();

  struct Audio
    {
      make_device_function device_function;
      audio_function sample_function;
    };
}

#endif
