// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// This reprensents some sound

#include "../../Global.h"

#ifndef __AUDIOSAMPLE_H
#define __AUDIOSAMPLE_H

namespace fragrant
{
  class AudioSample
    {
      public:
        virtual void destroy() = 0;

      private:
        virtual void d();
    };
}

#endif
