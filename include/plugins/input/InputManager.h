// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:
// A class that reads input events and stashes them in a queue somewhere

#ifndef __INPUTMANAGER_H
#define __INPUTMANAGER_H

#include "../../core/EventPipeline.h"
#include "../../Global.h"

namespace fragrant
{
  class InputManager
    {
      public:

        virtual void init(EventPipeline* pipeline) = 0;
        virtual void close() = 0;

        virtual void appendEvent(Event nevent) = 0;
        virtual void pump() = 0;

      private:
  };
}

#endif
