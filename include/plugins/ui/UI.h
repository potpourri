// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The interface for a UI component

#ifndef __UI_H
#define __UI_H

#include "../../core/Engine.h"
#include "../../core/PluginLoader.h"
#include "../../core/SettingsDB.h"

namespace fragrant
{
  struct EngineSub;
  class UI
    {
      public:
        virtual EngineSub run(PluginLoader& plugin_loader,
                              SettingsDB& settingsdb) = 0;

      private:
    };
}

#endif
