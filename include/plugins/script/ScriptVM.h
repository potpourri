// Copyright 2008 Brian Caine

// This file is part of Potpourri.

// Potpourri is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// Potpourri is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY of FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with Potpourri. If not, see <http://www.gnu.org/licenses/>.


// NOTES:

// The interface for a Scripting plugin

#ifndef __SCRIPTVM_H
#define __SCRIPTVM_H

#include <vector>

#include "../../core/Variant.h"
#include "../../core/ScriptedClass.h"

typedef fragrant::Variant* (*func)(std::vector<fragrant::Variant*>);

namespace fragrant
{
  class ScriptedClass;
  class ScriptVM
    {
      public:
        virtual bool initVM() = 0;
        virtual void closeVM() = 0;

        virtual void execString(std::string data) = 0;

        virtual void wrapFunction(std::string name, func thefunc) = 0;
        virtual void wrapClass(ScriptedClass* dclass) = 0;

        static ScriptedClass* caller;
    };

  Variant* getScriptCaller(std::vector<Variant*> params);
}

#endif
